# API

```javascript
import * as formul8 from 'formul8';

const { App } = formul8;

const app = new App({ data, store, history, target });

app.getData();
app.setData();

actions.setInput();

app.render('#target');

```

have a render function with the same signature as the react.render

appData: Pass a JSON string, URL, request object

```text

|-- components
| |-- inputs
| |-- forms
| |-- pages
| |-- random-component.tsx
| |-- <...>
|-- actions.ts
| |-

```