'use strict';

import * as foruml8 from 'formul8';

const { App } = formul8;

const app = new App();

class MyApp extends formul8.App {
  
  renderTarget = '#root';
  dataSource = '';
  
  onBeforeInitialRender() {
    
  }
  
  onAfterInitialRender() {
    
  }
  
}