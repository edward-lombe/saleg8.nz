# Default actions

---

## Inputs

- Set
- Reset
- Validate

## Forms

- Reset
- Validate
- Load / Set
- Save

## Pages

Do we need any default actions per page?
- Clear forms maybe?

# Server communication

As a form simply represents a view into the data
we must have some way of showing how the form
matches up to the data on the server.

## Unsynced

A form has been used to generate a set of values,
but this data has not yet been sent to the server.
Changes to the form while it is unsynced will only
be persisted locally.

## Synced

This means that the current data being displayed is
up to date on the client as well as the server.

## Modified

This means that a record has been created on the
server, but one ore more of the values for that
collection of values may have changed. For example
a form may have a vendor fill out a form and the
save, leaving the purchaser to modify the form at
a later date