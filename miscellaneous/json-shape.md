# JSON shape

## JSON primitives

- boolean
- string
- number
- integer (same as number but must be well, an integer)
- positive
- negative

## JSON abstract data types

- object
- array
- any

## Approach

define a primitive endpoint with just the string of the
primitive type.

For example a json string would just be represented with

`"string"`

Building upon this, an array with a single string element
would look like this

`["string"]`

A string and a number

`["string", "number"]`

An object with a key called `name` that is a string

`{ "name": "string" }`

With a number as well

`{ "name": "string", "age": "number" }`

Compounding now

```json
{
  "winners": ["string", "string", "string"]
}
```

```json
{
  "name": "string",
  "age": "number",
  "hobbies": []
}
```
