'use strict';

process.env.DEBUG = 'formul8*';

import { exec } from 'child_process';
import * as debug from 'debug';

const log = debug('formul8:process.ts');

export function execute(command: string) {

  log(`Running command '${command}'`);

  const process = exec(command, (error, stdout, stderr) => {

    if (error) {
      log(`${command} encountered error, ${error.toString()}`);
      throw error;
    }

    log(`${command}, stdout: ${stdout}`);
    log(`${command}, stderr: ${stderr}`);

  });

  return process;

}

