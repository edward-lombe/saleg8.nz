'use strict';

global.process.env.DEBUG = 'formul8*';

import { join } from 'path';
import * as fsep from 'fs-extra-promise';
import * as debug from 'debug';

const log = debug('formul8:copy-typings.ts');
const source = join(__dirname, '../typings');
const destination = join(__dirname, '../application/typings');

fsep.copyAsync(source, destination)
  .then(() => {
    log(`Copied typings folder`);
  })
  .catch(error => {
    log(`Error when copying typings, ${error}`);
  });
