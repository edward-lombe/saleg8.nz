'use strict';

process.env.DEBUG = 'formul8*';

import { join } from 'path';
import * as debug from 'debug';
import { readFileSync } from 'fs';

const log = debug('formul8:lint.ts');
const tslint = require('tslint');
const walk = require('walk');
const Linter = tslint;
const configuration = require('../tslint.json');
const walker = walk.walk(
  join(__dirname, '../source'), { followLinks: false }
);

walker.on('file', (...args) => {

  const [ root, fileStat, next] = args;
  const { name } = fileStat;
  if (!name.includes('.ts') || !name.includes('.tsx')) {
    next();
    return;
  }
  const fileName = join(root, name);
  const source = readFileSync(fileName, 'utf8');
  const formatter = 'prose';
  const options = { configuration, formatter };
  const linter = new Linter(fileName, source, options);
  const result = linter.lint();
  const { failureCount, output } = result;

  if (failureCount > 0) {
    log(output);
  }

  next();

});
