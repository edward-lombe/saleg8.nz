'use strict';

import * as chokidar from 'chokidar';
import { join } from 'path';
import * as webpack from 'webpack';
import { spawn } from 'child_process'

const nodemon = require('nodemon');
const livereload = require('livereload');
const webpackConfig: webpack.Configuration = require('../webpack.config');

const config: webpack.Configuration = {
  entry: ['babel-polyfill', './source/main.tsx'],
  output: {
    filename: 'main.js',
    path: join(__dirname, '../application/bundles')
  },
  devtool: 'source-map',
  devServer: {
    port: 8081,
    historyApiFallback: true
  },
  resolve: {
    // Add `.ts` and `.tsx` as a resolvable extension.
    extensions: ['', '.webpack.js', '.web.js', '.ts', '.tsx', '.js', 'json']
  },
  watch: true,
  module: {
    loaders: [
      {
        test: /\.tsx?$/,
        loaders: ['babel-loader', 'ts-loader']
      },
      {
        test: /\.json$/,
        loader: 'json-loader'
      }
    ]
  },
  plugins: [
    new webpack.ProvidePlugin({
      'fetch': 'imports?this=>global!exports?global.fetch!isomorphic-fetch'
    })
  ]
};

runWebpack();
runElectron();

export function runElmMake() {

  const makeFile = join(__dirname, '../source/elm/Main.elm');
  const outFile = join(__dirname, '../source/elm.js');

  const args = [makeFile, '--output', outFile];
  const process = spawn('elm-make', args, { stdio: 'inherit' });

  const watcher =
    chokidar.watch(join(__dirname, '../source/elm'));

  watcher.on('change', (pathname, stats) => {
    console.log('got change, making');
    const args = [makeFile, '--output', outFile];
    const process = spawn('elm-make', args, { stdio: 'inherit' });
  });

}

export function runWebpack() {

  const livereloadServer = livereload.createServer();
  const mapjd = a => a.map(b => join(__dirname, b));
  livereloadServer.watch(mapjd(['../application/bundles']));
  const webpackCompiler =
    webpack(config, (error, stats) => {

      if (error) {
        console.error(error);
        throw error;
      }

      console.log('webpack built successfully');

    });

}

export function runElectron() {

  const ext = '--ext ts,tsx,js,jsx,json';
  const watch = '--watch ./application';
  const ignore = '--ignore ./application/bundles ' +
    '--ignore ./application/resources';
  const exec = '--exec "npm run electron"';
  const delay = '--delay 1000ms';
  const verbose = '--verbose';

  nodemon(`${ext} ${watch} ${ignore} ${exec}`);

  nodemon.on('log', e => console.log(e.message));

}
