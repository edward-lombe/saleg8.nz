'use strict';

process.env.DEBUG = 'formul8*';

import * as debug from 'debug';

const log = debug('formul8:nodemon.ts');
const nodemon = require('nodemon');

const ext = '--ext ts,tsx,js,jsx,json,html';
const watch = '--watch ./source --watch ./application';
const ignore = '--ignore ./application/bundles ' +
  '--ignore ./application/resources';
const exec = '--exec "npm run webpack lint && npm run electron"';
const delay = '--delay 1000ms';
const verbose = '--verbose';

nodemon(`${ext} ${watch} ${ignore} ${exec}`);

nodemon.on('log', e => log(e.message));
