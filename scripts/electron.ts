'use strict'

import { join } from 'path'
import { spawn } from 'child_process'

const ELECTRON_PATH = require('electron-prebuilt')
const SOURCE_DIRECTORY = join(__dirname, '../')

const process = spawn(ELECTRON_PATH, [SOURCE_DIRECTORY], { stdio: 'inherit' })