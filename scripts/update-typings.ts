'use strict';

import { execute } from './process';

const typings = require('../typings.json');
const { dependencies, globalDependencies } = typings;

for (const moduleName in dependencies) {
  const process = execute(`typings install ${moduleName} --save`);
}

for (const moduleName in globalDependencies) {

  const value = globalDependencies[moduleName];

  if (value.includes('registry:dt')) {
    const process = execute(`typings install dt~${moduleName} --save --global`);
  }

}
