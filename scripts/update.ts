'use strict';

global.process.env.DEBUG = 'formul8*';

import * as debug from 'debug';
import { execute } from './process';

const log = debug('formul8:update.ts');

try {

  const outdated = require('../outdated.json');

  const outdatedModules = Object.keys(outdated);

  log(`Updating ${outdatedModules.join(', ')} to latest`);

  outdatedModules.forEach(moduleName => {

    const process = execute(`npm install --save ${moduleName}@latest`);

  });

} catch (error) {

  log(`Encountered ${error.toString()} while updating modules`);
  log(`Maybe run 'npm outdated --json > outdated.json' first?`);

}
