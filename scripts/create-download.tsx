'use strict';

/**
 * This file generates the download folder that
 * is then copied into a 'serve-able' directory
 * that can be used to download the app
 */

import { join } from 'path';
import * as React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';
import * as fsep from 'fs-extra-promise';
import * as debug from 'debug';
import
  { Grid
  , Row
  , Col
  , Button
  , ButtonToolbar
  , Jumbotron
  } from 'react-bootstrap';

debug.enable('*');

const dedent = require('dedent');
const log = debug('formul8:create-download');

Promise.resolve()
  .then(copyDependencies)
  .then(getCurrentVersion)
  .then(makeDownloadPage)
  .then(moveToPublicFolder);

async function copyDependencies() {

  await fsep.copyAsync
    ( join(__dirname, 'ms.js')
    , join(__dirname, '../distribution/ms.js')
    )
    .then(() => {
      log('Copied ms to distribution');
    })
    .catch(error => {
      log(`Error copying ms, ${error}`);
    });

  const node_modules = ['bootstrap'];

  return Promise.all(node_modules.map(moduleName => {

    const source = join(__dirname, '../node_modules', moduleName);
    const destination =
      join(__dirname, '../distribution/node_modules', moduleName);

    return fsep.copyAsync(source, destination)
      .then(() => {
        log(`Copied ${moduleName} to distribution`);
      })
      .catch(error => {
        log(`Error when copying ${moduleName}, ${error}`);
      });

  }));

}

async function getCurrentVersion(): Promise<string> {

  const source = join(__dirname, '../package.json');

  const json = await fsep.readJSONAsync(source)

  const JSONFilePath = join(__dirname, '../distribution/package.json');
  await fsep.writeJSONAsync(JSONFilePath, json);

  return json.version

  // return fsep.readJSONAsync(source)
  //   .then(packageJSON => {

  //     const JSONFilePath = join(__dirname, '../distribution/package.json');
  //     fsep.writeJSONAsync(JSONFilePath, packageJSON);

  //     const version = packageJSON['version'];
  //     return Promise.resolve(version);
  //   });

}

async function makeDownloadPage(version: string): Promise<void> {

  log(`Creating download page for version ${version}`);

  const script = dedent`
    var created = ${Date.now()};
    var timestamp = new Date(created).toString().slice(0, 21);
    var createdElement = document.getElementById('download-time');
    var timestampElement = document.getElementById('timestamp');
    var ago = ms(Date.now() - created);
    createdElement.textContent = ago;
    timestampElement.textContent = timestamp;
  `;

  const download = (
    <html>
      <head>
        <meta charSet='utf-8'/>
        <meta
          httpEquiv='X-UA-Compatible'
          content='IE=edge'/>
        <meta
          name='viewport'
          content='initial-scale=1'/>
        <title>Sale Note Download</title>
        <link
          rel='stylesheet'
          type='text/css'
          href='node_modules/bootstrap/dist/css/bootstrap.min.css'/>
        <script src='ms.js'></script>
      </head>
      <body>
        <Grid>
          <Row>
            <Col
              md={6}
              mdOffset={3}
              sm={8}
              smOffset={2}
              xs={12}>
              <br/>
              <Jumbotron>
                <h1>Download Sale Note</h1>
                <p>
                  Click the button below to download the sale note
                  application
                </p>
                <hr/>
                <p>
                  <small>
                    Current version is <code>{version}</code> created at{' '}
                    <strong>
                      <span id='download-time'>
                        an unknown amount of time
                      </span>
                    </strong>{' '}
                    ago on{' '}
                    <strong>
                      <span id='timestamp'>
                        an unknown date
                      </span>
                    </strong>
                  </small>
                </p>
                <ButtonToolbar>
                  <Button
                    href={`win/Sale Note Application Setup ${version}.exe`}
                    bsStyle='primary'>
                    Download for Windows
                  </Button>
                  <Button
                    href={`osx/Sale Note Application-${version}.dmg`}
                    bsStyle='primary'>
                    Download for OSX
                  </Button>
                </ButtonToolbar>
              </Jumbotron>
            </Col>
          </Row>
        </Grid>
        <script dangerouslySetInnerHTML={{ __html: script }}></script>
      </body>
    </html>
  );

  const downloadString = '<!DOCTYPE html>'  + renderToStaticMarkup(download);
  const destination = join(__dirname, '../distribution/index.html');

  await fsep.outputFileAsync(destination, downloadString)
    .then(() => {
      log('Created download page');
    })
    .catch(error => {
      log(`Error creating download file, ${error.toString()}`);
    });

}

function moveToPublicFolder() {

  const AWS_PUBLIC_FOLDER = 'C:/InterSystems/Cache/httpd/root/formul8';

  return fsep.existsAsync(AWS_PUBLIC_FOLDER)
    .then(exists => {
      if (exists) {
        return copyToPublicFolder();
      } else {
        log(`${AWS_PUBLIC_FOLDER} does not exist, distribution copy not done`);
      }
    })
    .catch(error => {
      log(`Exists error, ${error.toString()}`);
    });

  function copyToPublicFolder() {

    const source = join(__dirname, '../distribution');

    return fsep.copyAsync(source, AWS_PUBLIC_FOLDER)
      .then(() => {
        log('Successfully copied distribution to public');
      })
      .catch(error => {
        log(`Error copying files to public folder, ${error}`);
      });

  }

}
