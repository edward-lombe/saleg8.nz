'use strict';

process.env.DEBUG = 'formul8*';

import { join } from 'path';
import * as debug from 'debug';
import * as fsep from 'fs-extra-promise';

const log = debug('formul8:clean.ts');

const distributionDirectory = join(__dirname, '../distribution');

fsep.removeAsync(distributionDirectory)
  .then(() => {
    log('Distribution directory removed');
  })
  .catch(error => {
    const message =
      `Encountered error whilst trying to remove distribution folder, ${error}`;
    log(message);
  });
