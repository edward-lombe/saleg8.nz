'use strict';

/**
 * This file copies the version and the dependencies
 * from the package.json file found in the root folder
 * to the package.json found in the application folder
 */

import { join } from 'path';
import * as fsep from 'fs-extra-promise';
import * as debug from 'debug';
import * as semver from 'semver';

debug.enable('*');

const log = debug('formul8:update-application-package');
const { writeFileAsync } = fsep;
const applicationPath = join(__dirname, '../application/package.json');
const developmentPath = join(__dirname, '../package.json');
const developmentPackage = require('../package.json');
const applicationPackage = require('../application/package.json');
const { version } = developmentPackage;

const newVersion = semver.inc(version, 'patch');

log(`New version is ${newVersion}`);

developmentPackage.version = newVersion;
applicationPackage.version = newVersion;

const updatedDevelopment = JSON.stringify(developmentPackage, null, '  ');
const updatedApplication = JSON.stringify(applicationPackage, null, '  ');

writeFileAsync(developmentPath, updatedDevelopment)
  .then(() => {
    log('Successfuly updated package.json');
  })
  .catch(error => {
    log(`Error updating package.json, ${error.toString()}`);
  });

writeFileAsync(applicationPath, updatedApplication)
  .then(() => {
    log('Successfuly updated application/package.json');
  })
  .catch(error => {
    log(`Error updating application/package.json, ${error.toString()}`);
  });
