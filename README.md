# README

## Getting started

- ** Clone the repository **

Either `git clone https://<username>@bitbucket.org/edward-lombe/saleg8.nz.git`.
Or download the zip file that can be found on this page.

- ** Install dependencies **

This can be done with a simple `npm i` or `npm install`, this only needs to be
done once when you build the app.

- ** Build the app **

Most of the actions that need to be run are implemented as
[npm-scripts](https://docs.npmjs.com/misc/scripts), these can be run by
`npm run <script-name>`. Some of the ones you will need are

- `npm run update` - Downloads the latest version and builds
- `npm run build` - Rebuilds the app
- `npm run electron` - Runs electron without the need for building a binary
- `npm run auto-update` - checks for updates every 10 minutes and rebuilds

- ** Running the app **

Once you have installed dependencies and built, you can run the app. The
application bundle can be found under the `distribution/win` folder in the
project root.

`npm run electron` will run electron from the command line (as opposed to from a
bundle), which is significantly faster than waiting for it to build. You can also
open `localhost:8080` to see the app running from your browser.

## Overview of app structure

In the application root are several folders, each of whcih serve different
purposes and are used at different points along the development process.

- ** source **

The source directory is where all of the client side application logic is
located. It is written in TypeScript and the main entry point is the `main.tsx`
file.

- ** distribution **

This is where all the final copies of the bundles are built to. It is from this
folder that the `.exe` file is copied into the `application/download` folder
where it can be publically accessed.

- ** scripts **

The scripts folder contains all the neccessary files for building and developing
the application. Scripts for filewatching, linting and miscellaneous
copy/deleting related to building are all found here.

- ** typings **

This folder is created by the typings package and stores all the type
definitions needed. This allows typechecking, typehinting and autocompletion.

It's great :)