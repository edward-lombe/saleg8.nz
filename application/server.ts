'use strict';

import * as debug from 'debug';
import { exec } from 'child_process';
import { join } from 'path';
import { writeFile, readFile } from 'fs';
import * as express from 'express';
import * as cors from 'cors';
import * as morgan from 'morgan';
import * as bodyParser from 'body-parser';
import { shell } from 'electron';
import 'isomorphic-fetch';

import * as application from './application';
import * as printWindow from './windows/print-in-background';

const PDF_FOLDER = join(__dirname, 'resources/pdfs/');

const log = debug('formul8:server.ts');

const app = express();

const printBackground = { printBackground: true };

function loadAndSaveToDisk(app) {

  app.post('/saveData', (request, response, next) => {
    const data = request.body
    if (typeof data !== 'object') {
      return
    }
    writeFile
      (join(__dirname, 'resources/data.json'), JSON.stringify(data), error => {
      if (error) {
        response.send(error)
        return
      }
      response.send('success')
    })
  })

  app.get('/loadData', (request, response, next) => {
    readFile(join(__dirname, 'resources/data.json'), (error, data) => {
      try {
        const parsed = JSON.parse(data.toString())
        response.send(parsed)
      } catch (error) {
        response.send(error)
      }
    })
  })

}

export function initialize() {

  const PORT = process.env.SERVER_PORT || 8080;

  app.use(bodyParser.json({ limit: '10mb' }));

  app.use(cors());

  // Adds sale note disk saving route
  // SaveSaleNote(app)

  loadAndSaveToDisk(app)

  definePrintRoutes();

  onScreenKeyboardRoute();

  updateRoute();

  app.use(express.static(__dirname));

  app.get('*', (request, response, next) => {

    response.sendFile(join(__dirname, 'index.html'));

  });

  isServerRunning(PORT)
    .then(running => {

      if (!running) {

        app.listen(PORT);

      }

    });

}

function onScreenKeyboardRoute() {

  app.get('/onScreenKeyboard', (request, response) => {

    const { platform } = process;

    log(`Checking ${platform} can launch osk`);

    if (platform === 'win32') {

      log('Starting on screen keyboard');

      const oskProcess = exec('osk.exe');

      oskProcess.on('error', error => {
        log(`Could not start on screen keyboard, ${error}`);
      });

    }

    response.end();

  });

}

/**
 * Opens a browser with the page that they can use to update
 */
function updateRoute() {
  app.get('/update', (request, response) => {

    shell.openExternal('http://app.saleg8.net/formul8/');

    response.end();

  });
}

function definePrintRoutes() {

  app.post('/sendPDFToServer', (request, response) => {

    const { filename } = request.query;
    const { body } = request;
    const filepath = join(PDF_FOLDER, filename);

    readFile(filepath, (error, data) => {

      if (error) {
        response.send(error.toString());
        return;
      }

      uploadFile(filename, data, body)
        .then(() => {
          log('File uploaded successfully');
          response.end();
        })
        .catch(error => {
          log(`error, ${error}`);
          response.end();
        });

    });

  });

  app.get('/openPDF', (request, response) => {

    const { filename } = request.query;
    const fullPath = join(PDF_FOLDER, filename);

    const opened = shell.openExternal(fullPath);

    if (opened) {
      response.end('opened successfully');
    } else {
      shell.openItem(fullPath);
      response.end('did not open successfully');
    }

  });

  app.get('/generatePDF', (request, response) => {

    const browserWindow = application.getWindow('main');
    const { webContents } = browserWindow;
    const { filename } = request.query;

    webContents.printToPDF(printBackground, (error, data) => {

      if (error) {
        console.error(error);
        return;
      }

      writePDF(filename, data, () => {
        response.end(filename);
      });

    });

  });

  app.post('/generatePDFInBackground', (request, response) => {

    const { filename, saleNoteNumber } = request.query;
    const browserWindow = printWindow.initialize(saleNoteNumber);
    const { webContents } = browserWindow;

    log('starting the print process');

    webContents.on('dom-ready', () => {

      log('the dom is ready');

      setTimeout(() => {
        webContents.printToPDF(printBackground, (error, data) => {

          if (error) {
            log(error);
            return;
          }

          log('uploading file');

          const parameters = request.body;

          uploadFile(filename, data, parameters)
            .then(() => {
              log('uploaded file in background');
              printWindow.close();
            })
            .catch(error => {
              log('failed to upload file in background');
              printWindow.close();
              log(error);
            });

        });
      }, 10000)

    });

    response.end();

  });

}

function uploadFile(filename, buffer: Buffer, parameters) {

  const CID = parameters.CID || 'FM8';
  const { RELAY_SERVER } = process.env;
  const webMethod = 'Upload';
  const URL = `${RELAY_SERVER}/api/${webMethod}?CID=${CID}`;
  const pstrBase64 = buffer.toString('base64');
  const pstrFileName = filename;
  const pstrAgentCode = parameters.agentCode;
  const pstrSaleNote = parameters.saleNoteNumber;
  const body = JSON.stringify({
    pstrFileName,
    pstrBase64,
    pstrAgentCode,
    pstrSaleNote
  });
  const method = 'POST';
  const headers = new Headers();
  const options = { headers, method, body };

  headers.set('Content-Type', 'application/json');

  log(`Uploading ${filename} to ${URL}`);

  return fetch(URL, options)
    .then(response => response.json())
    .then(result => {
      log(`Got result ${JSON.stringify(result)}`);
    })
    .catch(error => {
      log(`Got error ${JSON.stringify(error)}}`);
    });

}

function writePDF(filename, data, done: Function) {

  const filepath = join(PDF_FOLDER, filename);

  writeFile(filepath, data, error => {

    if (error) {
      console.error(error);
    }

    log('File saved successfully');

    done();

  });

}

function isServerRunning(port) {
  return fetch(`http://127.0.0.1:${port}`)
    .then(response => {
      return true;
    })
    .catch(error => {

      if (error.code === 'ECONNREFUSED') {
        return false;
      }

      return false;

    });
}
