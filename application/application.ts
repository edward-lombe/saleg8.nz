'use strict';

import * as electron from 'electron';

const { BrowserWindow } = electron;
const windows: IWindows = {};

interface IWindows {
  [index: string]: Electron.BrowserWindow;
}

export function getWindows(): IWindows {
  return windows;
}

export function getWindow(name: string): Electron.BrowserWindow {
  return windows[name];
}

export function createWindow(name: string): Electron.BrowserWindow {

  const height = 900;
  const width = 1600;
  const show = false;
  const options = { width, height, show };
  let browserWindow = new BrowserWindow(options);

  windows[name] = browserWindow;

  browserWindow.on('closed', () => {
    browserWindow = null;
    windows[name] = null;
  });

  return browserWindow;

}
