'use strict';

const { join } = require('path');

require('ts-node').register({
  project: join(__dirname, 'tsconfig.json')
});

require(join(__dirname, 'main'));
