'use strict';

const application = require('../application');

export const windowName = 'main';
let browserWindow: Electron.BrowserWindow;

export function initialize(saleNoteNumber): Electron.BrowserWindow {

  browserWindow = application.createWindow(windowName);

  const PORT = process.env.SERVER_PORT;

  if (typeof saleNoteNumber === 'string') {
    browserWindow.loadURL(`http://127.0.0.1:${PORT}/printSaleNotePage?saleNoteNumber=${saleNoteNumber}`);
  } else {
    browserWindow.loadURL(`http://127.0.0.1:${PORT}/printSaleNotePage`);
  }

  return browserWindow;

}

export function close() {

  browserWindow.destroy();

}

