'use strict';

const application = require('../application');

export const windowName = 'main';
let browserWindow;

export function initialize() {

  browserWindow = application.createWindow(windowName);

  try {
    if (process.env.NODE_ENV === 'development') {
      browserWindow.webContents.openDevTools();
    }
  } catch (error) {
    console.error(error);
  }

  const PORT = process.env.SERVER_PORT;

  browserWindow.loadURL(`http://127.0.0.1:${PORT}`);
  browserWindow.show();

  return browserWindow;

}

