'use strict'

import * as fs from 'fs'
import { join } from 'path'
import * as express from 'express'

export default function (app: express.Application) {

  responseHandler(app)

}

/**
 * Adds the response handler to write sale notes
 * to an express app
 * 
 * @param {express.Application} app
 */
function responseHandler(app: express.Application) {

  const OUTPUT_FILE = join(__dirname, '../resources/sale-notes.json')
  const handler = (request, response) => {

    const sendError = msg => {
      response.statusCode = 400
      response.end(msg)
    }

    const { body, query } = request

    if (typeof body !== 'object') {
      sendError('Must have body as object')
      return
    }

    saveSaleNote(body)

  }

  app.post('/saveSaleNote', handler)

}

function saveSaleNote(saleNote) {

  const OUTPUT_FILE = join(__dirname, '../resources/sale-notes.json')

  fs.readFile(OUTPUT_FILE, (error, data) => {

    if (error) {
      console.error('Error reading json sale notes')
      console.error(error)
      return
    }

    try {

      const parsed = data.toJSON()

      if (Array.isArray(parsed)) {

        // Add the sale note to the list of sale notes
        parsed.push(saleNote)

        // Convert back to JSON
        const serialized = JSON.stringify(parsed)

        // Write the sale note to disk
        fs.writeFile(OUTPUT_FILE, serialized, error => {

          if (error) {
            console.error('Error writing JSON sale note file')
            console.error(error)
            return
          }

          console.log('Successfully saved')

        })

      }

    } catch (error) {

      console.error('Error parsing saved JSON sale notes')
      console.error(error)

    }

  })

}
