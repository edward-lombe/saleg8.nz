'use strict';

setEnvironmentVariables();

import * as electron from 'electron';

import * as server from './server';
import * as application from './application';
import * as main from './windows/main';
import * as debug from 'debug';

const log = debug('formul8:main.ts');

const { app, globalShortcut, Menu } = electron;

if (!require('electron-squirrel-startup') && !handleSquirrelEvent()) {
  initialize();
}

function initialize(): void {

  // Start HTTP server for the pages to access
  server.initialize();

  app.on('ready', () => {

    // Adds Command or Control + F12 as shortcut to open devtools
    registerDevToolsShortcut();

    // Opens the main window
    main.initialize();

  });

  app.on('window-all-closed', () => {
    app.quit();
  });

}

function setupMenu() {

  const template = [{
    label: 'Application',
    submenu: [
      { label: 'About Application', selector: 'orderFrontStandardAboutPanel:' },
      { label: 'Quit', accelerator: 'Command+Q'
      , click: function () { app.quit(); } }
    ]
  }, {
    label: 'Edit',
    submenu: [
      { label: 'Undo', accelerator: 'CmdOrCtrl+Z', selector: 'undo:' },
      { label: 'Redo', accelerator: 'Shift+CmdOrCtrl+Z', selector: 'redo:' },
      { label: 'Cut', accelerator: 'CmdOrCtrl+X', selector: 'cut:' },
      { label: 'Copy', accelerator: 'CmdOrCtrl+C', selector: 'copy:' },
      { label: 'Paste', accelerator: 'CmdOrCtrl+V', selector: 'paste:' },
      { label: 'Select All', accelerator: 'CmdOrCtrl+A'
      , selector: 'selectAll:' }
    ]
  }
  ];

  Menu.setApplicationMenu(Menu.buildFromTemplate(template));

}

/**
 * Allows us to open the devtools with a key combo
 */
function registerDevToolsShortcut() {

  const shortcut = 'CommandOrControl+F12';
  const registered = globalShortcut.register(shortcut, () => {
    const mainWindow = application.getWindow('main');
    const { webContents } = mainWindow;
    webContents.openDevTools();
  });

  if (!registered) {
    log('Could not register DevTools shortcut');
  }

  const message =
    `globalShortcut registered: ${globalShortcut.isRegistered(shortcut)}`;

  log(message);

}

function setEnvironmentVariables(): void {
  try {
    const variables = require('./env.json');
    for (const key in variables) {
      process.env[key] = variables[key];
    }
    return process.env;
  } catch (error) {
    const MODULE_NOT_FOUND = 'MODULE_NOT_FOUND';
    if (error.code === MODULE_NOT_FOUND) {
      const message =
        'Server could not load environment variables, ' +
        'is there an env.json file?';
      console.error(message);
    } else {
      console.error(error);
    }
  }
}

function handleSquirrelEvent(): boolean {
  if (process.argv.length === 1) {
    return false;
  }

  const ChildProcess = require('child_process');
  const path = require('path');

  const appFolder = path.resolve(process.execPath, '..');
  const rootAtomFolder = path.resolve(appFolder, '..');
  const updateDotExe = path.resolve(path.join(rootAtomFolder, 'Update.exe'));
  const exeName = path.basename(process.execPath);

  const spawn = function(command, args) {
    let spawnedProcess, error;

    try {
      spawnedProcess = ChildProcess.spawn(command, args, {detached: true});
    } catch (error) {
      console.error(error);
    }

    return spawnedProcess;
  };

  const spawnUpdate = function(args) {
    return spawn(updateDotExe, args);
  };

  const squirrelEvent = process.argv[1];
  switch (squirrelEvent) {
    case '--squirrel-install':
    case '--squirrel-updated':

      // Install desktop and start menu shortcuts
      spawnUpdate(['--createShortcut', exeName]);

      setTimeout(app.quit, 1000);
      return true;

    case '--squirrel-uninstall':
      // Undo anything you did in the --squirrel-install and
      // --squirrel-updated handlers

      // Remove desktop and start menu shortcuts
      spawnUpdate(['--removeShortcut', exeName]);

      setTimeout(app.quit, 1000);
      return true;

    case '--squirrel-obsolete':

      app.quit();
      return true;
  }
}
