'use strict'

const webpack = require('webpack')

module.exports = {
  entry: ['babel-polyfill', './source/main.tsx'],
  output: {
    filename: 'main.js',
    path: __dirname + '/application/bundles'
  },
  devtool: 'source-map',
  devServer: {
    port: 8081,
    historyApiFallback: true
  },
  resolve: {
    // Add `.ts` and `.tsx` as a resolvable extension.
    extensions: ['', '.webpack.js', '.web.js', '.ts', '.tsx', '.js', 'json']
  },
  module: {
    
    loaders: [
      
      // all files with a `.ts` or `.tsx` extension will be handled by `ts-loader`
      {
        test: /\.tsx?$/,
        loaders: ['babel-loader','ts-loader']
      },
      
      {
        test: /\.json$/,
        loader: 'json-loader'
      }
     
    ]
  },
  plugins: [
    new webpack.ProvidePlugin({
      'fetch': 'imports?this=>global!exports?global.fetch!isomorphic-fetch'
    })
  ]
}
