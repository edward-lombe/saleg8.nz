'use strict';

import { IIdentifier } from './interfaces';
import { entries, deepCopy } from './helpers';

import * as identifier from './identifier';

export let store: Redux.Store = null;

export function setStore(storeReference: Redux.Store): void {

  store = storeReference;

  storeReference.subscribe(() => {
    store = storeReference;
  });

}

export function globalGet
  ( componentIdentifier: IIdentifier
  , fullDescription?: boolean
  ) {

  if (store === null) {
    console.error('store is not set yet, cannot get appData');
    return null;
  }

  return get(store.getState().app, componentIdentifier, fullDescription);

}

export function get
  ( appData
  , componentIdentifier: IIdentifier
  , fullDescription: boolean = false)
: any {

  const { page, form, input, type } = componentIdentifier;
  const pageData = appData.pages;
  let selectedData = null;

  if (typeof appData === 'undefined') {
    console.error('Cannot get data from undefined');
    return;
  }

  // Safe property access in Javscript is hard
  try {

    if (type === 'page') {
      selectedData = pageData[page];
    } else if (type === 'form') {
      selectedData = pageData[page].forms[form];
    } else if (type === 'input') {
      selectedData = pageData[page].forms[form].inputs[input];
    }

    if (selectedData) {
      return fullDescription ? selectedData : extract(selectedData);
    }

  } catch (exception) {
    console.error('Failed to get data for IIdentifier:', componentIdentifier);
  }

}

export function extract(data: any) {

  const defined = value => typeof value !== 'undefined';
  const extractedData = {};

  if (defined(data.data)) {
    return data.data;
  } else if (defined(data.inputs)) {
    for (const inputKey in data.inputs) {
      extractedData[inputKey] = extract(data.inputs[inputKey]);
    }
    return extractedData;
  } else if (defined(data.forms)) {
    for (const formKey in data.forms) {
      extractedData[formKey] = extract(data.forms[formKey]);
    }
    return extractedData;
  } else if (defined(data.pages)) {
    for (const pageKey in data.pages) {
      extractedData[pageKey] = extract(data.pages[pageKey]);
    }
    return extractedData;
  } else {
    return data;
  }

}

export function *iterate
  ( appData
  , types: string[] = ['page', 'form', 'input']
): IterableIterator<any> {

  const pageData = appData.pages;

  for (const pageKey in pageData) {

    const page = pageData[pageKey];
    const pageIdentifier = identifier.generate(pageKey);

    if (types.indexOf('page') > -1) {
      yield [pageIdentifier, page];
    }

    const formData = page.forms;

    for (const formKey in formData) {

      const form = formData[formKey];
      const formIdentifier = identifier.generate(pageKey, formKey);

      if (types.indexOf('form') > -1) {
        yield [formIdentifier, form];
      }

      const inputData = form.inputs;

      for (const inputKey in inputData) {

        const input = inputData[inputKey];
        const inputIdentifier = identifier.generate(pageKey, formKey, inputKey);

        if (types.indexOf('input') > -1) {
          yield [inputIdentifier, input];
        }

      }
    }

  }

}

export function reset(inputDescription: any, identifier: IIdentifier): any {

  const { data, type } = inputDescription;
  const baseType = type.includes('.')
    ? type.slice(0, type.indexOf('.'))
    : type;

  switch (baseType) {
  case 'inputs/text':
    return '';
  case 'inputs/textarea':
    return '';
  case 'inputs/numeric':
    return '';
  case 'inputs/email':
    return '';
  case 'inputs/password':
    return '';
  case 'inputs/date':
    return '';
  case 'inputs/stock-description-table':
    return [];
  case 'inputs/status':
    return {
      status: 'unsynced',
      message: 'Form has not yet been saved to server'
    };
  case 'inputs/checkbox':

    if (identifier.input === 'commissionPayable') {
      return Object.assign({}, {
        vendor: true,
        purchaser: false
      });
    }

    const checkboxCopy = deepCopy(data);

    for (const key in checkboxCopy) {
      checkboxCopy[key] = false;
    }

    return checkboxCopy;

  case 'inputs/dropdown':
    const dropdownCopy = deepCopy(data);
    for (const key in dropdownCopy) {
      const value = dropdownCopy[key];
      if (typeof value === 'boolean') {
        dropdownCopy[key] = false;
      } else if (typeof value === 'string') {
        if (value === '1' || value === '0') {
          dropdownCopy[key] = false;
        }
      }
    }

    const none = '<none selected>';

    if (none in dropdownCopy) {
      dropdownCopy[none] = true;
    }

    return dropdownCopy;
  case 'inputs/signature':
    return '';
  default:
    return data;
  }

}
