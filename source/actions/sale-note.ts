import 'isomorphic-fetch'

export function saveLocal(saleNote) {

    const url = '/saveSaleNote'
    const method = 'POST'
    const headers = new Headers()
    headers.set('Content-Type', 'application/json')
    const body = JSON.stringify(saleNote)
    const options = { method, headers }

    return fetch(url)
      .then(response => {
        console.log('Saved successfully')
      })
      .catch(error => {
        console.error('Error sending sale note to local server')
        console.error(error)
      })

}
