'use strict';

import { IIdentifier } from './interfaces';

export const common = {
  saleNoteDisplay:
    generate('saleNoteDisplayPage', 'saleNoteDisplayForm', 'saleNoteDisplay'),
  saleNoteNumber:
    generate('saleNoteInputPage', 'saleNoteDetailsForm', 'saleNoteNumber'),
  nextSaleNoteNumber:
    generate('settingsPage', 'settingsForm', 'nextSaleNoteNumber'),
  clientDisplay:
    generate('clientDisplayPage', 'clientDisplayForm', 'clientDisplay'),
  saleNoteForm:
    generate('saleNoteInputPage', 'saleNoteForm'),
  saleNoteDetailsForm:
    generate('saleNoteInputPage', 'saleNoteDetailsForm'),
  settingsForm:
    generate('settingsPage', 'settingsForm'),
  settingsPage:
    generate('settingsPage'),
  agentCode:
    generate('settingsPage', 'settingsForm', 'agentCode'),
  lotProducts:
    generate('saleNoteInputPage', 'lotsForm', 'lotProducts'),
  auditData:
    generate('saleNoteInputPage', 'auditForm', 'auditData'),
  vendorAgentCode:
    generate('saleNoteInputPage', 'vendorForm', 'agentCode'),
  agentName:
    generate('settingsPage', 'settingsForm', 'agentName'),
  formVersion:
    generate('saleNoteInputPage', 'saleNoteDetailsForm', 'formVersion'),
  serverStatus:
    generate('saleNoteInputPage', 'saleNoteDetailsForm', 'serverStatus'),
  CID:
    generate('settingsPage', 'settingsForm', 'CID'),
  saleNoteInputPage:
    generate('saleNoteInputPage'),
  workflowForm:
    generate('saleNoteInputPage', 'workflowForm'),
  stockDescriptionTable:
    generate('saleNoteInputPage', 'lotsForm', 'stockDescriptionTable'),
  vendorEmail:
    generate('saleNoteInputPage', 'vendorForm', 'emailAddress'),
  purchaserEmail:
    generate('saleNoteInputPage', 'purchaserForm', 'emailAddress'),
  agentEmail:
    generate('settingsPage', 'settingsForm', 'agentEmail'),
  settingsLocked:
    generate('settingsPage', 'settingsForm', 'settingsLocked'),
  settingsHash:
    generate('settingsPage', 'settingsForm', 'settingsHash'),
  settingsPassword:
    generate('settingsPage', 'settingsForm', 'settingsPassword'),
};

export function generate
  ( pageName: string
  , formName?: string
  , inputName?: string
  ): IIdentifier {

  const type = inputName
    ? 'input'
    : (formName
      ? 'form' : 'page');

  return {
      type
    , page: pageName
    , form: formName
    , input: inputName
    };

}

export function modify
  ( original: IIdentifier
  , modification: any
  )
: IIdentifier {

  return Object.assign({}, original, modification);

}

export function compare(a: IIdentifier, b: IIdentifier): boolean {

  if (!a || !b) {
    return false;
  }

  for (const key in a) {
    if (a[key] !== b[key]) {
      return false;
    }
  }

  for (const key in b) {
    if (a[key] !== b[key]) {
      return false;
    }
  }

  return true;

}

/**
 * Returns true if identifier a is a parent to identifier b
 */
export function contains(a: IIdentifier, b: IIdentifier): boolean {

  const types = ['page', 'form', 'input'];

  if (types.indexOf(a.type) > types.indexOf(b.type)) {
    if (a.type === 'page') {
      return a.page === b.page;
    } else if (a.type === 'form') {
      return ((a.page === b.page) && (a.form === b.form));
    }
  } else {
    return false;
  }

}
