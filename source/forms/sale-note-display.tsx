'use strict';

import * as React from 'react';

import { Row, Col } from 'react-bootstrap';

export default class extends React.Component<any, any> {

  render() {

    const { inputs } = this.props;

    return (
      <Row>
        <Col xs={12}>
          <inputs.SaleNoteDisplay/>
        </Col>
      </Row>
    );
  }
}
