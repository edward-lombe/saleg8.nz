'use strict';

import * as React from 'react';

import * as actions from '../actions';
import * as data from '../data';
import * as identifier from '../identifier';
import { Row, Col } from 'react-bootstrap';

export default class extends React.Component<any, any> {

  render() {

    const { inputs } = this.props;
    const rowStyle = {
      borderRadius: '5px',
      padding: '1rem'
    };

    const heading = (
      <Row>
        <Col xs={12}>
          <h3>
            Purchaser
            {' '}
            <small>- must complete</small>
          </h3>
          <br/>
        </Col>
      </Row>
    );

    return (
      <div
        style={rowStyle}
        className='bg-info'>
        {heading}
        <Row>
          <Col xs={6}>
            <inputs.EntityName/>
          </Col>
          <Col xs={6}>
            <inputs.EntityType/>
          </Col>
        </Row>
        <Row>
          <Col xs={6}>
            <inputs.FarmSourceLivestockAccount/>
          </Col>
          <Col xs={3}>
            <inputs.PhoneNumber/>
          </Col>
          <Col xs={3}>
            <inputs.MobileNumber/>
          </Col>
        </Row>
        <Row>
          <Col xs={3}>
            <inputs.AgentName/>
          </Col>
          <Col xs={3}>
            <inputs.AgentCode/>
          </Col>
          <Col xs={6}>
            <inputs.RebateRate/>
          </Col>
        </Row>
        <Row>
          <Col xs={9}>
            <inputs.FullName/>
          </Col>
          <Col xs={3}>
            <inputs.DateOfBirth/>
          </Col>
        </Row>
        <Row>
          <Col xs={12}>
            <inputs.PostalAddress/>
          </Col>
        </Row>
        <Row>
          <Col xs={6}>
            <inputs.EmailAddress/>
          </Col>
          <Col xs={6}>
            <inputs.GSTNumber/>
          </Col>
        </Row>
        <Row>
          <Col xs={6}>
            <inputs.NAITNumber/>
          </Col>
          <Col xs={6}>
            <inputs.HerdPTPTCode/>
          </Col>
        </Row>
      </div>
    );

  }

}
