'use strict';

import * as data from '../data';
import * as actions from '../actions';
import * as React from 'react';
import * as id from '../identifier';
import { isCapital, capitalize, deepCopy } from '../helpers';
import { Row, Col, Button } from 'react-bootstrap';
import { default as Static, Password } from '../components/static';

export default class extends React.Component<any, any> {

  render() {

    const { inputs } = this.props;
    const isLocked = this.isLocked();

    return (
      <div>
        {isLocked ? this.renderSettings() : this.renderEditableSettings()}
        <Row>
          <Col xs={6}>
            <inputs.SettingsPassword/>
          </Col>
          <Col xs={6}>
            <inputs.SettingsLocked/>
          </Col>
        </Row>
        <Row>
          <Col xs={6}>
            <Button onClick={() => this.syncAgentSettings()}>
              Save agent settings
            </Button>
          </Col>
        </Row>
      </div>
    );

  }

  renderSettings() {

    const settingsData = data.get(this.props.app, id.common.settingsForm, true);
    const inputs = deepCopy(settingsData.inputs);
    const renderStatic = name => {
      const props = inputs[name];
      return (
        <Col xs={6}>
          <Static {...props}/>
        </Col>
      );
    };

    const renderPassword = name => {
      const props = inputs[name];
      return (
        <Col xs={6}>
          <Password {...props}/>
        </Col>
      );
    };

    return (
      <div>
        <Row>
          {renderStatic('agentName')}
          {renderStatic('agentCode')}
        </Row>
        <Row>
          {renderStatic('agentEmail')}
          {renderStatic('agentMobilePhone')}
        </Row>
        <Row>
          {renderPassword('agentPassword')}
          {renderStatic('agentLocation')}
        </Row>
        <Row>
          {renderStatic('nextSaleNoteNumber')}
          {renderStatic('CID')}
        </Row>
      </div>
    );
  }

  renderEditableSettings() {

    const { inputs } = this.props;

    return (
      <div>
        <Row>
          <Col xs={6}>
            <inputs.AgentName/>
          </Col>
          <Col xs={6}>
            <inputs.AgentCode/>
          </Col>
        </Row>
        <Row>
          <Col xs={6}>
            <inputs.AgentEmail/>
          </Col>
          <Col xs={6}>
            <inputs.AgentMobilePhone/>
          </Col>
        </Row>
        <Row>
          <Col xs={6}>
            <inputs.AgentPassword/>
          </Col>
          <Col xs={6}>
            <inputs.AgentLocation/>
          </Col>
        </Row>
        <Row>
          <Col xs={6}>
            <inputs.NextSaleNoteNumber/>
          </Col>
          <Col xs={6}>
            <inputs.CID/>
          </Col>
        </Row>
      </div>
    );
  }

  isLocked() {

    const appData = this.props.app;
    const settingsLocked = data.get(appData, id.common.settingsLocked);

    if (settingsLocked === 'locked') {
      return true;
    } else if (settingsLocked === 'unlocked') {
      return false;
    } else {
      console.error(settingsLocked);
    }

  }

  componentDidUpdate(prevProps, prevState) {

    const oldFormData = data.get(prevProps.app, this.props.identifier);
    const newFormData = data.get(this.props.app, this.props.identifier);

    if (!isCapital(newFormData.agentCode)) {

      const agentCode = newFormData.agentCode
        .split('')
        .map(l => l.toUpperCase())
        .join('');

      this.props.dispatch(actions.setInput({
        identifier: id.common.agentCode,
        data: agentCode
      }));

    }

    if (oldFormData.agentCode !== newFormData.agentCode) {
      this.props.dispatch(actions.setNextSaleNoteNumber());
    }

  }

  componentDidMount() {
    this.syncAgentSettings();
  }

  syncAgentSettings() {
    const { dispatch } = this.props;
    dispatch(actions.syncAgentSettings());
  }

}
