'use strict';

import * as React from 'react';

import * as actions from '../actions';
import * as data from '../data';
import * as identifier from '../identifier';
import { Row, Col } from 'react-bootstrap';

export default class extends React.Component<any, any> {

  render() {

    const { inputs } = this.props;

    return (
      <Row>
        <Col xs={12}>
          <inputs.StockDescriptionTable/>
        </Col>
      </Row>
    );

  }

}
