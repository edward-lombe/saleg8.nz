'use strict';

import * as React from 'react';

import { Row, Col } from 'react-bootstrap';
import * as actions from '../actions';

export default class extends React.Component<any, any> {

  render() {

    const { inputs } = this.props;

    return (
      <Row>
        <Col xs={12}>
          <inputs.ClientDisplay/>
        </Col>
      </Row>
    );

  }

  componentDidMount() {
    this.props.dispatch(actions.getAgentData());
  }

}
