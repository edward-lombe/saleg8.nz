'use strict';

import * as React from 'react';

import * as actions from '../actions';
import * as data from '../data';
import * as identifier from '../identifier';
import { safeOnChange, entries } from '../helpers';
import * as Bootstrap from 'react-bootstrap';
import { ClientSeachButton } from '../components/client-search';

const
  { Tab
  , Row
  , Col
  , Tabs
  , Button
  , ButtonGroup
  , FormGroup
  , FormControl
  , Glyphicon
  } = Bootstrap;

export default class extends React.Component<any, any> {

  render() {

    const { inputs } = this.props;

    return (
      <div>
        <Row>
          <Col xs={6}>
            <inputs.InCalfWarranty/>
          </Col>
          <Col xs={6}>
            <inputs.DeliveryDate/>
          </Col>
        </Row>
        <Row>
          <Col xs={3}>
            <inputs.CommissionRate/>
          </Col>
          <Col xs={3}>
            <inputs.CommissionType/>
          </Col>
          <Col xs={6}>
            <inputs.CommissionPayable/>
          </Col>
        </Row>
        <Row>
          <Col xs={12} sm={6}>
            <p>
              We confirm the Sale and Purchase of the above,
              subject to the terms and conditions on the back hereof.
              Remittance by RD1 Livestock is due 14 days from delivery
              date subject to conditions of Sale and Purchase.
            </p>
          </Col>
          <Col xs={12} sm={6}>
            <p>
              The Vendor agress to sell and the Purchaser agress to
              purchase the stock described above (subject to any
              reduction in numbers in accordance with the terms and
              conditions) in accordance of the contract and the
              conditions of Sale and Purchase on the reverse of this
              page.
            </p>
          </Col>
        </Row>
        <Row>
          <Col xs={6}>
            <div>
              <inputs.VendorSignature/>
            </div>
          </Col>
          <Col xs={6}>
            <div>
              <inputs.PurchaserSignature/>
            </div>
          </Col>
          <Col xs={6}>
            <inputs.VendorPresenceOf/>
          </Col>
          <Col xs={6}>
            <inputs.PurchaserPresenceOf/>
          </Col>
          <Col xs={6}>
            <div>
              <inputs.VendorAgentSignature/>
            </div>
          </Col>
          <Col xs={6}>
            <div>
              <inputs.PurchaserAgentSignature/>
            </div>
          </Col>
          <Col xs={6}>
            <inputs.VendorSignatureDate/>
          </Col>
          <Col xs={6}>
            <inputs.PurchaserSignatureDate/>
          </Col>
        </Row>
      </div>
    );

  }

}
