'use strict';

import * as React from 'react';

import * as actions from '../actions';
import * as data from '../data';
import * as identifier from '../identifier';
import { Row, Col } from 'react-bootstrap';

export default class extends React.Component<any, any> {

  render() {

    const { inputs } = this.props;

    return (
      <div>
        <Row>
          <Col xs={6}>
            <inputs.InternalNotes/>
          </Col>
          <Col xs={6}>
            <inputs.AdministratorNotes/>
          </Col>
        </Row>
        <Row>
          <Col xs={6}>
            <inputs.VendorNotes/>
          </Col>
          <Col xs={6}>
            <inputs.PurchaserNotes/>
          </Col>
        </Row>
      </div>

    );

  }

}
