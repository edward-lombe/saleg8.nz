'use strict';

import * as React from 'react';

import { Row, Col } from 'react-bootstrap';

export default class extends React.Component<any, any> {

  render() {

    const { inputs } = this.props;

    return (
      <Row>
        <Col xs={6}>
          <inputs.NAITTransfer/>
        </Col>
        <Col xs={6}>
          <inputs.AnimalRecordTransfer/>
        </Col>
        <Col xs={6}>
          <inputs.EmailedToVendor/>
        </Col>
        <Col xs={6}>
          <inputs.EmailedToPurchaser/>
        </Col>
        <Col xs={6}>
          <inputs.SentToServer/>
        </Col>
        <Col xs={6}>
          <inputs.MethodOfPayment/>
        </Col>
      </Row>
    );

  }

}
