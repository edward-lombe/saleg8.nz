'use strict';

import * as React from 'react';

import * as actions from '../actions';
import * as data from '../data';
import * as identifier from '../identifier';
import * as helpers from '../helpers';
import { Row, Col } from 'react-bootstrap';

export default class extends React.Component<any, any> {

  render() {

    const auditItems = this.renderAuditData();

    return (
      <Row>
        <Col xs={12}>
          {auditItems}
        </Col>
      </Row>
    );

  }

  renderAuditData() {

    const auditData = data.get(this.props.app, identifier.common.auditData);

    const noChanges = (
      <p key={0}>No audit changes to track</p>
    );

    if (!Array.isArray(auditData) || auditData.length === 0) {
      return [noChanges];
    }

    const orderedData = [...auditData].reverse();

    return orderedData.map((auditItem, i) => {

      const { time, field } = auditItem;
      const timestamp = String(new Date(time));
      const inputIdentifier = identifier.modify(
        identifier.common.workflowForm,
        {
          type: 'input',
          input: field
        }
      );
      const inputData = data.get(this.props.app, inputIdentifier, true);

      let [oldValue, newValue] = ['', ''];

      for (const key in auditItem['old']) {
        if (auditItem['old'][key]) {
          oldValue = key;
        }
      }

      for (const key in auditItem['new']) {
        if (auditItem['new'][key]) {
          newValue = key;
        }
      }

      if (newValue === '') {
        newValue = 'blank';
      }

      if (oldValue === '') {
        oldValue = 'blank';
      }

      return (
        <div key={i}>
          <hr/>
          <p>{inputData.label}</p>
          <p>@{timestamp}</p>
          <p>Changed from
            <code>{oldValue}</code> to <code>{newValue}</code>
          </p>
        </div>
      );

    });

  }

  componentWillReceiveProps(nextProps: any) {

    const auditData = data.get(this.props.app, identifier.common.auditData);
    const formOld = data.get(this.props.app, identifier.common.workflowForm);
    const formNew = data.get(nextProps.app, identifier.common.workflowForm);

    for (const key in formOld) {

      const convert = a => JSON.stringify(helpers.convertStringSelect(a));

      const a = convert(formOld[key]);
      const b = convert(formNew[key]);

      if (a !== b) {

        const newData = [...auditData, {
          field: key,
          time: new Date().toISOString(),
          old: helpers.convertStringSelect(formOld[key]),
          new: helpers.convertStringSelect(formNew[key])
        }];

        this.props.dispatch(actions.setInput({
          identifier: identifier.common.auditData,
          data: newData
        }));

      }

    }

  }

}
