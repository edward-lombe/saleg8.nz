'use strict';

/**
 * Collection of small functions that are best
 * all kept in one place, to help you
 */

import * as React from 'react';
import { Link } from 'react-router';

import { IIdentifier } from './interfaces';
import * as identifier from './identifier';

/**
 * Gets some test data from the json-server
 * utility which is used to mock a HTTP
 * data source
 */
export function fetchTestData() {

  try {

    const testData = require('./db.json');

    if (typeof testData !== 'undefined') {
      return Promise.resolve(testData);
    }

  } catch (error) {
    return Promise.reject(error);
  }

}

export function removeIndex(array: any[], index: number) {
  return [...array.slice(0, index), ...array.slice(index + 1)];
}

/**
 * Takes a hyphon-seperated string and converts
 * it to hyphonSeperated
 */
export function hyphonToCamel(input: string) {

  const names = input.split('-');

  return names.reduce(
    (accumulator, name, i) => {
      return accumulator + (i === 0
        ? name
        : capitalize(name));
    },
    ''
  );

}

/**
 * Takes a camelCased string and returns camel-case
 */
export function camelToHyphon(input: string) {

  const letters = input.split('');

  return letters.reduce(
    (accumulator, letter) => {
      return accumulator + (isCapital(letter)
        ? '-' + letter.toLowerCase()
        : letter);
    },
    ''
  );

}

/**
 * Converts the first letter of a string to
 * uppercase
 */
export function capitalize(input: string): string {

  if (input && input.length > 0) {
    return input[0].toUpperCase() + input.slice(1);
  } else {
    return '';
  }

}

export function decapitalize(input: string) {
  if (input && input.length > 0) {
    return input[0].toLowerCase() + input.slice(1);
  } else {
    return '';
  }
}

/**
 * Tests whether a string is made of capital
 * letters or not
 */
export function isCapital(input: string): boolean {
  return input.split('').every(letter => {
    return letter.charAt(0) === letter.charAt(0).toUpperCase();
  });
}

export function sliceCharacters(input: string, start: string, end: string) {
  const startIndex = input.indexOf(start) + 1;
  const endIndex = input.indexOf(end);
  return input.slice(startIndex, endIndex);
}

export function safeOnChange(component, value) {
  if (typeof component.props.onChange === 'function') {
    component.props.onChange(value);
  }
}

export function entries(object: any): any[] {
  return Object.keys(object).map(key => [key, object[key]]);
}

export function values(object: any): any[] {
  return Object.keys(object).map(key => object[key]);
}

export function *iterateInputs(appData) {

  const pageData = appData.pages;

  for (const pageKey in pageData) {

    const formData = pageData[pageKey].forms;

    for (const formKey in formData) {

      const inputData = formData[formKey].inputs;

      for (const inputKey in inputData) {

        const input = inputData[inputKey];
        const inputIdentifier = identifier.generate(pageKey, formKey, inputKey);

        yield [inputIdentifier, input];

      }
    }

  }

}

export function matchedIndexes(data: any[], value: string): number[] {

  if (value === '') {
    return [...Array(data.length)].map((_, i) => i);
  }

  return data
    .map(item => JSON.stringify(item))
    .reduce(
      (matched: number[], item: string, i) => {
        return item.toLowerCase().includes(value.toLowerCase())
          ? [...matched, i] : matched;
      },
      []
    );
}

export function isUppercase(input: string): boolean {
  return Array.from(input).every(letter => {
    return letter === letter.toUpperCase();
  });
}

export function pstrify(input: any): any {

  if (typeof input === 'undefined') {
    return;
  }

  if (input.length === 0) {
    return;
  }

  if (typeof input === 'string') {
    return 'pstr' + input[0] + input.slice(1);
  } else if (typeof input === 'object') {
    const cacheObject = {};
    for (const key in input) {
      cacheObject[pstrify(key)] = input[key];
    }
    return cacheObject;
  }
}

export class PageNotFound extends React.Component<any, any> {

  render() {
    return (
      <div>
        Page not found, return home <Link to='/'>here</Link>
      </div>
    );
  }

}

export function getVersion() {

  const savedVersion = sessionStorage.getItem('version')
  const wasSaved = typeof savedVersion === 'string'

  if (wasSaved) {
    return Promise.resolve(savedVersion);
  }

  try {

    const URL = '/package.json';

    return fetch(URL)
      .then(response => response.json())
      .then(result => {
        const version = result['version'];
        if (!wasSaved) {
          sessionStorage.setItem('savedVersion', version)
        }
        return version
      });

  } catch (error) {
    return Promise.reject(error);
  }

}

export function convertStringSelect(data) {
  for (const key in data) {
    const value = data[key];
    if (value === '0') {
      data[key] = false;
    } else if (value === '1') {
      data[key] = true;
    }
  }
  return data;
}

export function typeOf(input: any) {

  const type = typeof input;

  switch (type) {
  case 'boolean':
  case 'function':
  case 'undefined':
  case 'symbol':
  case 'string':
  case 'number':
    return type;
  case 'object':
    if (input === null) {
      return 'null';
    } else if (Array.isArray(input)) {
      return 'array';
    } else {
      return type;
    }
  default:
    return type;
  }

}

export function deepCopy(input: any) {

  const type = typeOf(input);

  switch (type) {
  case 'boolean':
  case 'function':
  case 'undefined':
  case 'symbol':
  case 'string':
  case 'number':
  case 'null':
    return input;
  case 'array':
    return [...input].map(value => deepCopy(value));
  case 'object':
    const copy = {};
    for (const key in input) {
      copy[key] = deepCopy(input[key]);
    }
    return copy;
  default:
    return input;
  }

}
