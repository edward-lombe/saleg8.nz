'use strict';

import { globalGet } from './data';
import { entries } from './helpers';
import * as id from './identifier';
import * as validator from 'validator';

interface IValidationResult {
  valid: boolean;
  message?: string;
  type?: 'error' | 'warning';
}

const VALID: IValidationResult = { valid: true };
const INVALID: IValidationResult = { valid: false };

export function required(data: any, enabled: boolean): IValidationResult {

  if (typeof data === 'string') {
    data = data.trim();
  }

  if (!enabled) {
    return VALID;
  }

  if (data && data.length > 0 && typeof data === 'string') {
    return VALID;
  } else {
    const message = 'Input is required';
    return Object.assign({}, INVALID, { message });
  }

}

export function numeric(data: any, enabled: boolean): IValidationResult {

  if (!enabled) {
    return VALID;
  }

  return INVALID;

}

export function email(data: any, enabled: boolean) {

  const result = required(data, enabled);

  if (!result.valid) {
    const message = 'Warning no email set';
    return Object.assign({}, VALID, { message });
  }

  const isEmail = validator.isEmail(data);

  if (!isEmail) {
    const message = 'Warning email is not valid';
    return Object.assign({}, VALID, { message });
  }

  return VALID;

}

export function saleNoteNumber(data: any, enabled: boolean) {

  const result = createRequired('Sale note number')(data, enabled);

  if (!result.valid) {
    return result;
  }

  const agentCode = globalGet(id.common.agentCode);

  if (!data.includes(agentCode)) {
    const message = 'Sale note number must be prefixed with agent code';
    return Object.assign({}, INVALID, { message });
  }

  const split = data.split('-');

  if (split.length < 2) {
    const message = 'Sale note number must contain a hyphen';
    return Object.assign({}, INVALID, { message });
  }

  const saleNumber = split[split.length - 1];

  if (Number.isNaN(Number(saleNumber)) || saleNumber === '') {
    const message = 'Sale note number must contain a number after the hyphen';
    return Object.assign({}, INVALID, { message });
  }

  return result;

}

export const entityName = createRequired('Entity name');

export const agentCode = createRequired('Agent code');

export const agentName = createRequired('Agent name');

export const commissionRate = createRequired('Commission rate');

export const commissionType = createSelected('Commission type');

export const commissionPayable = createSelected('Commission payable');

export const NAITNumber = createRequired('NAIT Number');

export const retentionToBeHeld = createSelected('Retention to be held');

export const agentEmail = createRequired('Agent Email');

export function stockDescriptionTable(data: any[], enabled: boolean) {

  if (!enabled) {
    return VALID;
  }

  if (data.length > 0) {
    return VALID;
  } else {
    const message = 'Needs to be at least one lot entered';
    return Object.assign({}, INVALID, { message });
  }

}

export function deliveryDate(data: any, enabled: boolean) {

  const requiredResult = required(data, enabled);

  if (!requiredResult.valid) {
    const message = 'Delivery date is required';
    return Object.assign({}, requiredResult, { message });
  }

  return requiredResult;

}

/**
 * Ensures that at least one of the options is selected
 */
function createSelected(inputName: string) {
  return function (data: any, enabled: boolean): IValidationResult {

    const hasTrueKey = entries(data).some(([key, value]) => !!value);

    if (hasTrueKey) {
      return VALID;
    }

    const message = `${inputName} needs to be selected`;

    return Object.assign({}, INVALID, { message });

  };
}
/**
 * Returns a function that ensures that the data passed to it
 * is not a blank string or otherwise null data
 */
function createRequired(inputName: string) {
  return function (data: any, enabled: boolean): IValidationResult {

    const result = required(data, enabled);

    if (result.valid) {
      return result;
    }

    const message = `${inputName} is required`;

    return Object.assign({}, INVALID, { message });

  };
}
