'use strict';

export const defaultVariables = {
  RELAY_SERVER: 'https://app.saleg8.net',
  NODE_ENV: 'development',
  DEBUG: 'formul8*',
  SERVER_PORT: 8080
};

export const variables = require('../application/env.json');

export function loadVariables() {

  window['process'] = {};
  window['process']['env'] = {};

  const allVariables = Object.assign(defaultVariables, variables);

  for (const key in allVariables) {
    process.env[key] = allVariables[key];
  }

  return allVariables;

}
