'use strict';

/**
 * Small collection of layout classes
 * to keep library specfic class names
 * un-hardcoded
 */

import * as React from 'react';

import Navbar from './components/navbar';
import Sidebar from './components/sidebar';

const ReactBootstrap = require('react-bootstrap');

export const Bootstrap = ReactBootstrap;
export const Grid = ReactBootstrap.Grid;
export const Row = ReactBootstrap.Row;
export const Col = ReactBootstrap.Col;

export class Page extends React.Component<any, any> {

  render() {
    return (
      <Grid fluid>
        <br/>
        <Row>
          <Col xs={12}>
            <Navbar/>
          </Col>
        </Row>
        <Row>
          <Col xs={4} sm={3} md={2}>
            <Sidebar/>
          </Col>
          <Col xs={8} sm={9} md={10}>
            {this.props.children}
          </Col>
        </Row>
        <Row>
          <Col xs={12}>
            <HalfPage/>
          </Col>
        </Row>
      </Grid>
    );
  }

}

export class Title extends React.Component<any, any> {

  render() {
    return (
      <Col xs={12}>
        <h2>{this.props.children}</h2>
        <hr/>
      </Col>
    );
  }
}

export class HalfPage extends React.Component<any, any> {

  render() {
    return (
      <div style={{ height: '50vh' }}></div>
    );
  }

}
