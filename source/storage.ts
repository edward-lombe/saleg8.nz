'use strict';

import * as data from './data';
import { iterateInputs } from './helpers';
import { SET_INPUT } from './actions';
import 'isomorphic-fetch'

const debounce = require('debounce');

export const LOCAL_STORAGE_KEY = 'store';

export function useLocalStorage(store: Redux.Store) {

  const serialzeStore = debounce(
    () => {
      const storeJSON = JSON.stringify(store.getState());
      window.localStorage.setItem(LOCAL_STORAGE_KEY, storeJSON);
    },
    200
  );

  store.subscribe(serialzeStore);

}

export function loadFromLocalStorage(store: Redux.Store) {

  try {

    const state = JSON.parse(window.localStorage.getItem(LOCAL_STORAGE_KEY));

    for (const [identifier, input] of data.iterate(state.app, ['input'])) {
      store.dispatch({
        type: SET_INPUT,
        payload: { identifier, data: input.data }
      });
    }

  } catch (exception) {
    console.error(exception, 'Unable to parse saved JSON');
  }

}

export function useDiskStorage(store: Redux.Store): void {

  const serialzeStore = debounce(
    () => {
      const body = JSON.stringify(store.getState());
      const method = 'POST'
      const headers = new Headers()
      const options = { method, headers, body }
      headers.set('Content-Type', 'application/json')
      fetch('/saveData', options)
    },
    1000 * 10
  )

  store.subscribe(serialzeStore)

}

export function loadFromDiskStorage(store: Redux.Store) {

  console.log('loading data')

  try {
    fetch('/loadData')
      .then(response => response.json())
      .then(state => {
        const iterator = data.iterate(state['app'], ['input'])
        for (const [identifier, input] of iterator) {
          store.dispatch({
            type: SET_INPUT,
            payload: { identifier, data: input.data }
          });
        }
      })
      .catch(error => {
        console.error(error, 'Unable to parse saved JSON');
      })
  } catch (error) {
    console.error(error, 'Unable to parse saved JSON');
  }

}
