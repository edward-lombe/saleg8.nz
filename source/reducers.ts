'use strict';

import { createStore, combineReducers } from 'redux';

import * as id from './identifier';
import { IIdentifier } from './interfaces';
import { SET_INPUT, CONFIGURE_INPUT } from './actions';


/**
 * This function generates the reducers to use
 * for the app structure as defined in the db.json
 * 
 * @export
 * @param {any} appData
 * @returns
 */
export function generate(appData) {

  const populatedObject = object => Object.keys(object).length > 0;
  const pageReducers = {};
  const pageData = appData.pages;

  for (const pageKey in pageData) {

    const formReducers = {};
    const formData = pageData[pageKey].forms;

    for (const formKey in formData) {

      const inputReducers = {};
      const inputData = formData[formKey].inputs;

      for (const inputKey in inputData) {

        const input = inputData[inputKey];
        const inputIdentifier = id.generate(pageKey, formKey, inputKey);

        inputReducers[inputKey] = inputReducer(input, inputIdentifier);

      }

      if (populatedObject(inputReducers)) {
        formReducers[formKey] = combineReducers({
          inputs: combineReducers(inputReducers)
        });
      }

    }

    if (populatedObject(formReducers)) {
      pageReducers[pageKey] = combineReducers({
        forms: combineReducers(formReducers)
      });
    }

  }

  return combineReducers({ pages: combineReducers(pageReducers) });

}

function inputReducer(
    initialState = { data: null }
  , inputIdentifier: IIdentifier) {

  return (state = initialState, action) => {

    const { payload, type } = action;

    if (typeof payload === 'undefined') {
      return state;
    }

    if (!id.compare(payload.identifier, inputIdentifier)) {
      return state;
    }

    if (type === SET_INPUT) {
      return Object.assign({}, state, { data: payload.data });
    }

    if (type === CONFIGURE_INPUT) {
      return Object.assign({}, state, payload);
    }

    return state;

  };

}
