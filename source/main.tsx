'use strict';

/**
 * Main entry point to the app,
 * combines all the stuff together
 */

/**
 * We need to load the environment
 * variable right at the start so that
 * anything can use them.
 */
import * as environment from './environment';
environment.loadVariables();

import * as React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Router, Route, browserHistory } from 'react-router';
import { syncHistoryWithStore, routerReducer } from 'react-router-redux';
import Thunk from 'redux-thunk';
import * as debug from 'debug'

import * as actions from './actions';
import * as container from './container';
import * as helpers from './helpers';
import * as storage from './storage';
import * as reducers from './reducers';
import * as identifier from './identifier';
import { IIdentifier } from './interfaces';
import { setStore } from './data';

window['eal'] = actions

debug.enable('sale-note*')

const createLogger = require('redux-logger');
const log = debug('sale-note:main.tsx')

// Call it
main();

/**
 * This function is the main entry point to
 * the app. It is the function that injects
 * the Root component into the page
 */
export function main() {

  log('Starting main client side')

  // Needs to be replaced with actual data
  return helpers.fetchTestData()
    .then(renderRoot);

}

/**
 * Root class responsible for wrapping our app
 * in a store (redux) and routes (react-router).
 * All app data is injected into the props after
 * it has been fetched from the external data source.
 */
export class Root extends React.Component<any, {} | void> {

  render() {

    const { appData, store, history } = this.props;
    const routes = generateRoutes(appData);

    return (
      <Provider store={store}>
        <Router history={history}>
          {routes}
        </Router>
      </Provider>
    );

  }

}

/**
 * Assembles the various stores and objects needed for
 * redux, react-redux and the router
 */
export function renderRoot(appData): React.Component<any, any> {

  const rootElement = document.querySelector('#root');
  const rootReducer = combineReducers({
    routing: routerReducer,
    app: reducers.generate(appData)
  });
  const store = createStore(
    rootReducer,
    applyMiddleware(Thunk)
    /** applyMiddleware(Thunk, createLogger()) */
  );
  const history = syncHistoryWithStore(browserHistory, store);
  const root = React.createElement(Root, { store, history, appData });

  /**
   * We need to save a reference to the store so that we can get data
   * without a reference to appData
   */
  setStore(store);

  /*
   storage.loadFromLocalStorage(store);
   storage.useLocalStorage(store);
  */


  storage.useDiskStorage(store)
  storage.loadFromDiskStorage(store)

  window['state'] = selector => {

    console.clear();

    const state = store.getState();

    if (typeof selector === 'undefined') {
      console.log(state);
      return;
    }

    const selectors: any[] = selector.split('.');
    let key, value = state;

    while ((key = selectors.shift())) {

      value = value[key];

      if (!value) {
        console.log(`invalid selector, ${key} ${selectors}`);
        break;
      }

    }

    console.log(value);

  };

  return render(
    root,
    rootElement
  );

}

/**
 * This function takes the appData variable and
 * maps it to a series of <Route/> elements
 * 
 * @param {any} appData
 * @returns
 */
function generateRoutes(appData) {

  const routes = [];

  for (const pageKey in appData.pages) {
    const page = appData.pages[pageKey];
    routes.push(generateRoute(page));
  }

  // Adds a route not found for any paths that slip through
  routes.push(
    <Route key={0} path='*' component={helpers.PageNotFound}/>
  );

  return routes;

  /**
   * Helper function that takes a page object and uses
   * the redux connect function to give it functionality
   * 
   * @param {any} page
   * @returns
   */
  function generateRoute(page) {

    const componentIdentifier = identifier.generate(page.name);
    const component = container.connect(appData, componentIdentifier);

    return (
      <Route
        key={page.link}
        path={page.link}
        component={component}/>
    );
  }

}
