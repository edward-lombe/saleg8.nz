'use strict';

import * as React from 'react';
import { Link } from 'react-router';

import { ListGroup, ListGroupItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';

export default class extends React.Component<any, any> {

  render () {
    return (
      <ListGroup className='no-print'>
        <LinkItem to='/'>Home</LinkItem>
        <LinkItem to='/saleNotePage'>Enter Sale Note</LinkItem>
        <LinkItem to='/saleNoteDisplayPage'>View Sale Notes</LinkItem>
        <LinkItem to='/clientDisplayPage'>View Clients</LinkItem>
        <LinkItem to='/settings'>Settings</LinkItem>
        <LinkItem to='/feedbackPage'>Feedback Page</LinkItem>
      </ListGroup>
    );
  }

}

class LinkItem extends React.Component<any, any> {

  render() {

    const to = this.props.to || '/';

    return (
      <LinkContainer to={to}>
        <ListGroupItem>
          {this.props.children}
        </ListGroupItem>
      </LinkContainer>
    );
  }
}
