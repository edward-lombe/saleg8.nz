'use strict';

import * as React from 'react';
import { Link } from 'react-router';

import { entries, capitalize } from '../helpers';
import * as actions from '../actions';
import * as id from '../identifier';
import * as data from '../data';

import { LinkContainer } from 'react-router-bootstrap';
import { Row, Col, Button, ButtonGroup, Table, Modal } from 'react-bootstrap';

export default class extends React.Component<any, any> {

  constructor() {
    super();
    this.state = {
      showModal: false
    };
  }

  render() {

    const { data } = this.props;
    const { saleNoteDetailsForm } = data;
    const { saleNoteNumber } = saleNoteDetailsForm;
    const link = `/saleNotePage?saleNoteNumber=${saleNoteNumber}`;

    return (
      <div>
        <Row>
          <Col xs={12}>
            <h3>Sale Note Number: {saleNoteNumber}</h3>
            <hr/>
          </Col>
          <Col xs={12}>
            <ButtonGroup>
              <LinkContainer to={link}>
                <Button
                  bsStyle='info'>
                  Edit
                </Button>
              </LinkContainer>
              <Button
                onClick={() => this.onSync()}
                bsStyle='success'>
                Sync
              </Button>
              <Button
                onClick={() => this.onShow()}
                bsStyle='danger'>
                Delete
              </Button>
            </ButtonGroup>
            <hr/>
          </Col>
        </Row>
        <Row>
          <Col xs={12} md={6}>
            <AgentTable {...this.props}/>
          </Col>
          <Col xs={12} md={6}>
            <LotsTable {...this.props}/>
          </Col>
        </Row>
        <Modal
          show={this.state.showModal}
          onHide={() => this.onHide()}>
          <Modal.Header closeButton>
            Delete
          </Modal.Header>
          <Modal.Body>
            <strong>Are you sure you wish to delete this record?</strong>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={() => this.onHide()}>Cancel</Button>
            <Button
              bsStyle='danger'
              onClick={() => this.onDelete()}>Delete</Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }

  onHide() {
    this.setState({ showModal: false });
  }

  onShow() {
    this.setState({ showModal: true });
  }

  onDelete() {
    const saleNoteNumber = this.getSaleNoteNumber();
    this.props.dispatch(actions.deleteSaleNoteNumberServer(saleNoteNumber));
  }

  onSync() {
    const saleNoteNumber = this.getSaleNoteNumber();
    this.props.dispatch(actions.syncSaleNoteNumber(saleNoteNumber));
  }

  getSaleNoteNumber() {
    try {
      const { data } = this.props;
      const { saleNoteDetailsForm } = data;
      const { saleNoteNumber } = saleNoteDetailsForm;
      return saleNoteNumber;
    } catch (error) {
      return;
    }
  }

}

class AgentTable extends React.Component<any, any> {

  render() {

    const { data } = this.props;
    const { saleNoteDetailsForm } = data;
    const { saleNoteNumber } = saleNoteDetailsForm;
    const { vendorForm, purchaserForm } = data;

    return (
      <div>
        <h3>Details</h3>
        <Table striped condensed>
          <thead>
            <tr>
              <th>Sale Note Number</th>
              <th>Vendor Entity Name</th>
              <th>Vendor Agent Code</th>
              <th>Purchaser Entity Name</th>
              <th>Purchaser Agent Code</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>{saleNoteDetailsForm.saleNoteNumber}</td>
              <td>{vendorForm.entityName}</td>
              <td>{vendorForm.agentCode}</td>
              <td>{purchaserForm.entityName}</td>
              <td>{purchaserForm.agentCode}</td>
            </tr>
          </tbody>
        </Table>
      </div>
    );

  }

}

class LotsTable extends React.Component<any, any> {

  render() {

    const items = this.renderItems();
    const display = items.length === 0
      ? (
        <p>No lots entered on form</p>
      )
      : (
        <Table>
          <thead>
            <tr>
              <th>Tally</th>
              <th>Product</th>
              <th>Description</th>
              <th>Price</th>
              <th>Commision Override</th>
            </tr>
          </thead>
          <tbody>
            {items}
          </tbody>
        </Table>
      );

    return (
      <div>
        <h3>Lots</h3>
        {display}
      </div>
    );
  }

  renderItems() {
    const data = this.getLots();
    return data.map((item, i) => {
      return this.renderItem(i, item);
    });
  }

  renderItem(key, item) {
    return (
      <tr key={key}>
        <td>{item.tally}</td>
        <td>{item.selectedProduct}</td>
        <td>{item.description}</td>
        <td>{item.price}</td>
        <td>{item.commissionOverride}</td>
      </tr>
    );
  }

  getLots() {

    const { data } = this.props;
    if (typeof data === 'undefined') {
      return [];
    }
    const { lotsForm } = data;
    if (typeof lotsForm === 'undefined') {
      return [];
    }
    const { stockDescriptionTable } = lotsForm;
    if (typeof stockDescriptionTable === 'undefined') {
      return [];
    }
    return stockDescriptionTable || [];

  }

}
