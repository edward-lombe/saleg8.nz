'use strict';

import * as React from 'react';

const sanitizeHTML = require('sanitize-html');

export default class extends React.Component<any, any> {

  constructor() {
    super();
    this.state = {
      quote: '',
      author: ''
    };
    this.getData();
  }

  render() {
    return (
      <blockquote>
        <div dangerouslySetInnerHTML={{ __html: this.state.quote }}></div>
        <footer>
          {this.state.author}
        </footer>
      </blockquote>
    );
  }

  getData() {

    const URL
      = 'http://quotesondesign.com'
      + '/wp-json/posts?filter[orderby]=rand&filter[posts_per_page]=1';
    const options = { mode: 'cors' };

    return fetch(URL, { mode: 'cors' })
      .then(response => response.json())
      .then(results => {
        const quoteData = results[0];
        const quote = sanitizeHTML(quoteData.content, {
          allowedTags: ['p']
        });
        const author = quoteData.title;
        this.setState({ quote, author });
      });

  }

}
