'use strict';

import * as React from 'react';
import { Link } from 'react-router';

import { getVersion } from '../helpers';

import
  { Navbar
  , FormGroup
  , FormControl
  , ControlLabel
  , Nav
  , NavItem
  } from 'react-bootstrap';

const Text = Navbar['Text'];

export default class extends React.Component<any, any> {

  constructor() {
    super();
    this.state = {
      version: 'unknown',
      latestVersion: 'unknown',
      outdated: false
    };
  }

  render() {

    const { version, outdated } = this.state;

    return (
      <Navbar fluid className='no-print'>
        <Navbar.Header>
          <Navbar.Brand>
            <Link to='/'>Home</Link>
          </Navbar.Brand>
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav pullRight>
            <NavItem
              eventKey={1}
              onClick={() => this.toggleKeyboard()}>
              Activate On Screen Keyboard
            </NavItem>
            {outdated
              ? (
                <NavItem
                  eventKey={2}
                  onClick={() => this.update()}>
                  Click here to update
                </NavItem>
              )
              : []
            }
          </Nav>
          <Text pullRight>
            {version}
          </Text>
        </Navbar.Collapse>
      </Navbar>
    );
  }

  update() {
    return fetch('/update');
  }

  componentDidMount() {
    this.getData()
      .then(() => this.checkUpToDate());
  }

  toggleKeyboard() {
    fetch('/onScreenKeyboard');
    return false;
  }

  checkUpToDate() {
    const { version, latestVersion } = this.state;
    const outOfDate = version !== latestVersion;
    if (outOfDate) {
      this.setState({ outdated: true });
      console.log(`version is ${version}, latest is ${latestVersion}`);
    }
  }

  getData() {

    return getVersion()
      .then(version => {
        this.setState({ version });
      })
      .then(() => this.getLatestVersion())
      .catch(error => {
        console.error(error);
        this.setState({ version: 'Could not fetch latest version' });
      });

  }

  getLatestVersion() {

    const URL = 'https://relaytest.saleg8.net/api/SaleNoteLatestVersion?CID=FM8'
    const headers = new Headers();
    const options = { cors: 'no-cors', method: 'POST' };

    return fetch(URL, options)
      .then(response => {
        return response.text();
      })
      .then(version => {
        console.log(`latest version is ${version}`);
        this.setState({ latestVersion: version });
        return version;
      });

  }

}
