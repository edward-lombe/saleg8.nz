'use strict';

import * as React from 'react';
import { findDOMNode } from 'react-dom';
import { Link } from 'react-router';

import { matchedIndexes } from '../helpers';

import * as Bootstrap from 'react-bootstrap';
const
  { Row
  , Col
  , Table
  , Modal
  , Button
  , ButtonGroup
  , FormControl
  , FormGroup
  , ControlLabel
  } = Bootstrap;

export default class ClientSearch extends React.Component<any, any> {

  constructor() {
    super();
    this.state = {
      matched: [],
      clientCode: '',
      showModal: false
    };
  }

  render() {

    const { data } = this.props;
    const headings = this.renderHeadings();
    const items = this.renderItems();
    const modal = this.renderModal();

    return (
      <Row>
        <Col xs={12}>
          <SearchClients
            data={data}
            onChange={e => this.onSearch(e)}/>
        </Col>
        <Col xs={12}>
          <Table responsive striped hover condensed>
            <thead>
              {headings}
            </thead>
            <tbody>
              {items}
            </tbody>
          </Table>
        </Col>
        {modal}
      </Row>
    );
  }

  onSearch(matched: any[]): void {
    this.setState({ matched });
  }

  onSelect(clientCode: string): void {

    const { type } = this.props;

    this.setState({ clientCode }, () => {

      if (typeof type === 'undefined') {
        this.setState({ showModal: true });
      } else {
        this.onChange(type);
      }

    });
  }

  onChange(type?: string) {

    const { clientCode } = this.state;

    if (typeof this.props.onChange === 'function') {

      if (typeof type !== 'undefined') {
        this.props.onChange({ type, clientCode });
      }

      if (typeof this.props.type !== 'undefined') {
        this.props.onChange({ clientCode, type: this.props.type });
      }

      this.setState({ showModal: false });

    }

  }

  renderModal() {

    const { showModal, clientCode } = this.state;

    return (
      <Modal
        keyboard
        bsSize='small'
        onHide={() => this.setState({ showModal: false })}
        show={showModal}>

        <Modal.Header closeButton>
          <Modal.Title>Client type</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>
            Would you like to load account
            {' '}
            <strong>{clientCode}</strong>
            {' '}
            as a vendor or purchaser
          </p>
          <ButtonGroup>
            <Button onClick={() => this.onChange('vendor')}>
              Load as Vendor
            </Button>
            <Button onClick={() => this.onChange('purchaser')}>
              Load as Purchaser
            </Button>
          </ButtonGroup>
        </Modal.Body>
        <Modal.Footer></Modal.Footer>

      </Modal>
    );

  }

  renderHeadings() {
    return (
      <tr>
        <th>Account</th>
        <th>Name</th>
        <th>Address</th>
        <th>Mobile Number</th>
        <th>NAIT</th>
        <th>MINDA</th>
        <th>Email</th>
        <th>Contact</th>
        <th>Country</th>
        <th>Province</th>
        <th>District</th>
        <th>Mail Code</th>
        <th>Tax code</th>
        <th>Tax number</th>
      </tr>
    );
  }

  renderItems() {

    const { matched } = this.state;
    const { data } = this.props;

    return data
      .filter((_, i) => {
        return matched.indexOf(i) !== -1;
      })
      .map((client, i) => {
        return this.renderItem(i, client);
      });

  }

  renderItem(key, client) {
    return (
      <Client
        onChange={e => this.onSelect(e)}
        {...{ key, data: client}}/>
    );
  }

}

class SearchClients extends React.Component<any, any> {

  render() {
    return (
      <FormGroup>
        <ControlLabel>Search Clients</ControlLabel>
        <FormControl
          ref='search'
          onChange={e => this.onChange(e)}
          type='text'
          placeholder='Enter search term...'/>
      </FormGroup>
    );
  }

  onChange(e) {
    const value = e.target.value;
    const matched = matchedIndexes(this.props.data, value);
    if (typeof this.props.onChange === 'function') {
      this.props.onChange(matched);
    }
  }

  componentDidMount() {
    const search = findDOMNode(this.refs['search']);
    if (search && typeof search['focus'] === 'function') {
      search['focus']();
    }
    const matched = matchedIndexes(this.props.data, '');
    if (typeof this.props.onChange === 'function') {
      this.props.onChange(matched);
    }
  }

}

export class ClientModal extends React.Component<any, any> {

  render() {

    const { show, type, data } = this.props;

    return (
      <Modal
        keyboard
        bsSize='large'
        onHide={() => this.onClose()}
        show={show}>
        <Modal.Header closeButton>
          <Modal.Title>Load Client</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <ClientSearch
            onChange={e => this.props.onChange(e)}
            type={type}
            data={data}/>
        </Modal.Body>
        <Modal.Header></Modal.Header>
      </Modal>
    );

  }

  onClose() {
    if (typeof this.props.onClose === 'function') {
      this.props.onClose();
    }
  }

  onChange(e) {
    if (typeof this.props.onChange === 'function') {
      this.props.onChange(e);
    }
  }

}

export class ClientSeachButton extends React.Component<any, any> {

  constructor() {
    super();
    this.state = {
      showModal: false
    };
  }

  render() {

    const modal = this.renderModal();

    return (
      <Button onClick={() => this.showModal(true)}>
        Load Client
        {modal}
      </Button>
    );
  }

  renderModal() {

    const { showModal } = this.state;

    return (
      <Modal
        keyboard
        bsSize='large'
        onHide={() => this.showModal(false)}
        show={showModal}>
        <Modal.Header closeButton>
          <Modal.Title>Load Client</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <ClientSearch
            onChange={e => this.onChange(e)}
            data={this.props.data}/>
        </Modal.Body>
        <Modal.Header></Modal.Header>
      </Modal>
    );
  }

  showModal(showModal?: boolean) {
    if (typeof showModal === 'undefined') {
      this.setState({ showModal });
    } else {
      this.setState({ showModal: !this.state.showModal });
    }
  }

  onChange(e) {

    if (typeof this.props.onChange === 'function') {
      this.props.onChange(e);
      this.showModal(false);
    }

  }

}

class Client extends React.Component<any, any> {

  render() {

    const { data } = this.props;
    const address  = data.addresses[0];
    const
      { contact
      , email
      , mailCode
      , province
      , tax
      , taxNumber
      , country
      , key
      , mobile
      , name
      , district
      , MINDA
      , NAIT
      , phone
      } = data;

    return (
      <tr onClick={() => this.onChange()}>
        <td>{key}</td>
        <td>{name}</td>
        <td>{address}</td>
        <td>{mobile}</td>
        <td>{NAIT}</td>
        <td>{MINDA}</td>
        <td>{email}</td>
        <td>{contact}</td>
        <td>{country}</td>
        <td>{province}</td>
        <td>{district}</td>
        <td>{mailCode}</td>
        <td>{tax}</td>
        <td>{taxNumber}</td>
      </tr>
    );

  }

  onChange(): void {
    if (typeof this.props.onChange === 'function') {
      this.props.onChange(this.props.data.key);
    }
  }

}
