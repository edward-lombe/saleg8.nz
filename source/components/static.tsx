'use strict';

import * as React from 'react';
import { FormGroup, ControlLabel, FormControl } from 'react-bootstrap';

const { Static } = FormControl;

export default class StaticControl extends React.Component<any, any> {

  render() {
    return (
      <FormGroup>
        <ControlLabel>{this.props.label}</ControlLabel>
        <Static>{this.props.data}</Static>
      </FormGroup>
    );
  }

}

export class Password extends React.Component<any, any> {

  render() {

    try {
      let length = this.props.data.length
    } catch (error) {
      length = 8
    }

    if (typeof length !== 'number') {
      length = 8
    }

    const props = Object.assign(
      {}, this.props, { data: '•'.repeat(length) });

    return (
      <StaticControl {...props}/>
    );
  }

}
