'use strict';

import * as React from 'react';
import { Link } from 'react-router';

import * as data from '../data';
import * as id from '../identifier';
import { Row, Col } from '../layout';
import { safeOnChange, removeIndex, convertStringSelect } from '../helpers';
import PreviewSaleNote from '../components/preview-sale-note';

const Bootstrap = require('react-bootstrap');
const
  { ListGroup
  , ListGroupItem
  , Label
  , FormControl
  , FormGroup
  , Form
  , ControlLabel
  } = Bootstrap;

export default class extends React.Component<any, any> {

  constructor() {
    super();
    this.state = {
      matched: [],
      filter: {}
    };
  }

  componentWillMount() {
    this.setState({ filter: getFilter(this.props.app) });
  }

  render() {

    const items = this.renderItems();
    const { data } = this.props;
    const { matched, filter } = this.state;

    const noSaleNotes = (
      <Col xs={12}>
        <p>
          No sale notes found, enter a sale note
          {' '}
          <Link to='/saleNotePage'>here</Link>.
        </p>
      </Col>
    );

    const saleNotes = (
      <div>
        <Col xs={12}>
          <SearchSaleNotes
            onFilter={e => this.onFilter(e)}
            onSearch={e => this.onSearch(e)}
            {...this.props}/>
          <hr/>
        </Col>
        <Col xs={12}>
          {items}
        </Col>
      </div>
    );

    return (
      <Row>
        {this.props.data.length === 0 ? noSaleNotes : saleNotes}
      </Row>
    );
  }

  renderItems() {
    const { data } = this.props;
    const { matched, filter } = this.state;
    return data
      .filter((_, i) => {
        const includes = matched.indexOf(i) > -1;
        return includes;
      })
      .filter(saleNoteData => {

        if (values(filter).every(key => key === '<none selected>')) {
          return true;
        }

        const { workflowForm } = saleNoteData;
        let matched = false;

        for (const key in filter) {

          const value = filter[key];

          if (value === '<none selected>') {
            continue;
          }

          const workflowObject = convertStringSelect(workflowForm[key]);

          if (typeof workflowObject[value] === 'boolean') {
            if (workflowForm[key][value]) {
              matched = true;
              break;
            }
          }

        }

        return matched;

      })
      .map((saleNoteData, i) => {
        return (
          <PreviewSaleNote
            {...this.props}
            key={i}
            data={saleNoteData}/>
        );
      });
  }

  onFilter(filter) {
    this.setState({ filter });
  }

  onDelete(key) {
    const data = removeIndex(this.props.data, key);
    safeOnChange(this, data);
  }

  onSearch(matched) {
    let { selectedSaleNote } = this.state;
    if (matched.length > 0 && matched.indexOf(selectedSaleNote) === -1) {
      selectedSaleNote = matched[0];
      this.setState({ matched, selectedSaleNote });
    }
    this.setState({ matched });
  }

}

class SearchSaleNotes extends React.Component<any, any> {

  constructor() {
    super();
    this.state = {
      filter: {}
    };
  }

  componentWillMount() {
    this.setState({ filter: getFilter(this.props.app) });
  }

  render() {

    const filter = name => e => this.onFilter(name, e.target.value);

    return (
      <Form>
        <Row>
          <Col xs={12}>
            <FormGroup>
              <FormControl
                placeholder='Search...'
                onChange={e => this.onChange(e)}
                type='text'/>
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col xs={6}>
            <FormGroup>
              <ControlLabel>NAIT transfer</ControlLabel>
              <FormControl
                onChange={filter('NAITTransfer')}
                componentClass='select'
                placeholder='select'>
                <option value='<none selected>'>
                  {'<none selected>'}
                </option>
                <option value='not applicable'>
                  Not applicable
                </option>
                <option value='complete'>
                  Complete
                </option>
              </FormControl>
            </FormGroup>
          </Col>
          <Col xs={6}>
            <FormGroup>
              <ControlLabel>Animal Record Transfer</ControlLabel>
              <FormControl
                onChange={filter('animalRecordTransfer')}
                componentClass='select'
                placeholder='select'>
                <option value='<none selected>'>
                  {'<none selected>'}
                </option>
                <option value='not applicable'>
                  Not applicable
                </option>
                <option value='complete'>
                  Complete
                </option>
              </FormControl>
            </FormGroup>
          </Col>
          <Col xs={6}>
            <FormGroup>
              <ControlLabel>Emailed to Vendor</ControlLabel>
              <FormControl
                onChange={filter('emailedToVendor')}
                componentClass='select'
                placeholder='select'>
                <option value='<none selected>'>
                  {'<none selected>'}
                </option>
                <option value='not applicable'>
                  Not applicable
                </option>
                <option value='complete'>
                  Complete
                </option>
              </FormControl>
            </FormGroup>
          </Col>
          <Col xs={6}>
            <FormGroup>
              <ControlLabel>Emailed to Purchaser</ControlLabel>
              <FormControl
                onChange={filter('emailedToPurchaser')}
                componentClass='select'
                placeholder='select'>
                <option value='<none selected>'>
                  {'<none selected>'}
                </option>
                <option value='not applicable'>
                  Not applicable
                </option>
                <option value='complete'>
                  Complete
                </option>
              </FormControl>
            </FormGroup>
          </Col>
          <Col xs={6}>
            <FormGroup>
              <ControlLabel>Sent to Server</ControlLabel>
              <FormControl
                onChange={filter('sentToServer')}
                componentClass='select'
                placeholder='select'>
                <option value='<none selected>'>
                  {'<none selected>'}
                </option>
                <option value='not sent'>
                  Not sent
                </option>
                <option value='ready'>
                  Ready
                </option>
                <option value='complete'>
                  Complete
                </option>
              </FormControl>
            </FormGroup>
          </Col>
        </Row>
      </Form>
    );
  }

  onChange(e) {
    const { value } = e.target;
    this.props.onSearch(this.search(value));
  }

  onFilter(filterName, value) {
    const { filter } = this.state;
    filter[filterName] = value;
    this.setState({ filter }, () => {
      if (typeof this.props.onFilter === 'function') {
        this.props.onFilter(this.state.filter);
      }
    });
  }

  search(input): any[] {

    return this.props.data
      .reduce(
        (array, saleNote, i) => {
          const saleNoteJSON = JSON.stringify(saleNote).toLowerCase();
          const includes =  saleNoteJSON.indexOf(input.toLowerCase()) > -1;
          return includes ? [...array, i] : array;
        },
        []
      );

  }

  componentDidMount() {
    const all = [...Array(this.props.data.length)].map((_, i) => i);
    this.props.onSearch(all);
  }

}

function values(object: any) {
  return Object.keys(object).map(key => object[key]);
}

function getFilter(appData) {

  const workflowForm = data.get(appData, id.common.workflowForm);
  const filter = {};

  for (const key in workflowForm) {
    filter[key] = '<none selected>';
  }

  return filter;

}
