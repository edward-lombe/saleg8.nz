'use strict';

import { get } from '../data';
import * as React from 'react';

import
  { safeOnChange
  , entries
  , capitalize
  , convertStringSelect
  , deepCopy
  } from '../helpers';

const Bootstrap = require('react-bootstrap');
const { FormControl, FormGroup, ControlLabel } = Bootstrap;

export default class extends React.Component<any, any> {

  render() {

    const items = this.renderItems();
    const selected = this.getSelected();

    return (
      <FormGroup controlId='formControlsSelect'>
        <ControlLabel>{this.props.label}</ControlLabel>
        <FormControl
          value={selected}
          onChange={e => this.onChange(e)}
          componentClass='select'
          placeholder='select'>
          {items}
        </FormControl>
      </FormGroup>
    );
  }

  renderItems() {

    const data = this.getMerged();

    return entries(data || []).map(([value, selected], i) => {
      return this.renderItem(i, value, selected);
    });

  }

  getSelected(): string {

    const data = this.getMerged();

    for (const key in data) {
      if (typeof data[key] === 'boolean' && data[key]) {
        return key;
      }
    }

    return '';

  }

  renderItem(key: number, value: string = '', selected: boolean = false) {
    return (
      <option
        key={key}
        value={value}>
        {capitalize(value)}
      </option>
    );
  }

  /**
   * Gets the definition from the JSON file.
   * This is due to defintions not updating
   * or reflecting old version of the data
   */
  getMerged() {

    const JSONDefinition = require('../db.json');
    const extracted = Object.assign(
      {}, get(JSONDefinition, this.props.identifier));

    for (const key in extracted) {
      extracted[key] = false;
    }

    const merged = Object.assign({}, extracted, this.props.data);
    const keys = [];

    for (const key in merged) {
      if (keys.indexOf(key.toUpperCase()) > -1) {
        delete merged[key];
      } else {
        keys.push(key.toUpperCase());
      }
    }

    return merged;

  }

  onChange(e) {

    const { value } = e.target;
    const data = Object.keys(this.props.data)
      .reduce(
        (object, key) => {
          object[key] = (key === value);
          return object;
        },
        {}
      );

    const converted = convertStringSelect(data);

    safeOnChange(this, deepCopy(converted));

  }

}
