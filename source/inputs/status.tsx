'use strict';

import * as React from 'react';

import { safeOnChange, capitalize } from '../helpers';

const Bootstrap = require('react-bootstrap');
const { Alert, FormGroup, FormControl, ControlLabel } = Bootstrap;

export default class extends React.Component<any, any> {

  render() {

    const bsStyle = this.getBsStyle();
    const { status, message } = this.props.data;
    const alert = (
      <FormGroup>
        <ControlLabel>{this.props.label}</ControlLabel>
        <br/>
        <Alert bsStyle={bsStyle}>
          <p>
            <strong>{capitalize(status)}</strong>:
            {' '}
            {this.props.data.message}
          </p>
        </Alert>
      </FormGroup>
    );

    if (status && message) {
      return alert;
    } else {
      return (
        <span></span>
      );
    }

  }

  onChange(e) {
    safeOnChange(this, e.target.value);
  }

  getBsStyle() {
    switch (this.props.data.status) {
      case 'unsynced': return 'warning';
      case 'synced': return 'success';
      default: return 'danger';
    }
  }

}
