'use strict';

import * as React from 'react';
import
  { FormControl
  , FormGroup
  , ControlLabel
  , HelpBlock
  } from 'react-bootstrap';

import * as actions from '../actions';
import { safeOnChange } from '../helpers';

export default class extends React.Component<any, any> {

  render() {

    const { valid } = this.props;
    const feedbackText = this.props.feedbackText || '';
    let validProp = {};

    if (typeof valid === 'boolean' && !valid) {
      const validationState = 'error';
      validProp = { validationState };
    }

    return (
      <FormGroup {...validProp}>
        <ControlLabel>{this.props.label}</ControlLabel>
        <FormControl
          type='date'
          value={this.props.data}
          onChange={e => this.onChange(e)}/>
        <HelpBlock>{feedbackText}</HelpBlock>
      </FormGroup>
    );
  }

  onChange(e) {
    safeOnChange(this, e.target.value);
    if (typeof this.props.validation !== 'undefined') {
      this.props.dispatch(actions.validate(this.props.identifier));
    }
  }

}
