'use strict';

import * as React from 'react';

import { safeOnChange, capitalize, entries, matchedIndexes } from '../helpers';
import ClientSearch from '../components/client-search';
import * as actions from '../actions';

const Bootstrap = require('react-bootstrap');
const { Row, Col, Table, Modal, Button, ButtonGroup } = Bootstrap;

export default class extends React.Component<any, any> {

  constructor() {
    super();
    this.state = {
      clientCode: ''
    };
  }

  render() {

    return (
      <ClientSearch
        data={this.props.data}
        onChange={e => this.onChange(e)}/>
    );

  }

  onChange(clientCode) {
    this.setState({ clientCode });
  }

  loadClient(type: string) {
    const { clientCode } = this.state;
    this.props.dispatch(actions.loadClientIntoSaleNote({ clientCode, type }));
  }

}
