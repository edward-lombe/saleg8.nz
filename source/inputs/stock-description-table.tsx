'use strict';

import * as React from 'react';

import * as actions from '../actions';
import * as id from '../identifier';
import * as data from '../data';
import { safeOnChange, deepCopy } from '../helpers';

import
  { Table
  , Button
  , ButtonToolbar
  , Glyphicon
  , FormGroup
  , FormControl
  , ControlLabel
  , Checkbox
  , HelpBlock
  } from 'react-bootstrap';

export default class extends React.Component<any, any> {

  constructor() {
    super();
    this.state = {
      tally: '',
      description: '',
      products: {},
      price: '',
      commissionOverride: '',
      selectedProduct: '',
      editRow: null,
      editRowValues: {}
    };
  }

  componentDidMount() {
    this.setProducts();
  }

  setProducts() {

    const products = data.get(this.props.app, id.common.lotProducts);

    this.setState({ products });

  }

  render() {

    const items = this.renderItems();
    const edit = this.edit();
    const { valid } = this.props;
    const feedbackText = this.props.feedbackText || '';
    let validProp = {};

    if (typeof valid === 'boolean' && !valid) {
      const validationState = 'error';
      validProp = { validationState };
    }

    return (
      <div>
        <Table>
          <thead>
            <tr>
              <th>Tally</th>
              <th>Products</th>
              <th>Description</th>
              <th>Price (GST exclusive)</th>
              <th>Commission override</th>
              <th className='text-center'></th>
            </tr>
          </thead>
          <tbody>
            {items}
            {edit}
          </tbody>
        </Table>
        <FormGroup {...validProp}>
          <HelpBlock>{feedbackText}</HelpBlock>
        </FormGroup>
      </div>
    );
  }

  renderItems() {
    return this.props.data.map((item, i) => {

      const
        { tally
        , description
        , price
        , commissionOverride
        , selectedProduct
        } = item;

      return this.renderItem(
        i, tally, description, price, commissionOverride, selectedProduct
      );
    });
  }

  renderItem(
      key: string
    , tally: string
    , description: string
    , price: string
    , commissionOverride: boolean
    , selectedProduct: string
    ) {

    const { editRow } = this.state;

    if (editRow === key) {
      return this.renderEditItem(
        key, tally, description, price, commissionOverride, selectedProduct
      );
    }

    return (
      <tr key={key}>
        <td>{tally}</td>
        <td>{selectedProduct}</td>
        <td>{description}</td>
        <td>{price}</td>
        <td>{commissionOverride.toString()}</td>
        <td className='text-right'>
          <ButtonToolbar>
            <Button
              onClick={() => this.onToggleEditLot(key)}
              bsSize='xsmall'
              bsStyle='info'>
              <Glyphicon glyph='pencil'></Glyphicon>
            </Button>
            <Button
              onClick={() => this.onDelete(key)}
              bsSize='xsmall'
              bsStyle='danger'>
              <Glyphicon glyph='remove'></Glyphicon>
            </Button>
          </ButtonToolbar>
        </td>
      </tr>
    );
  }

  renderEditItem(
      key: string
    , tally: string
    , description: string
    , price: string
    , commissionOverride: boolean
    , selectedProduct: string
    ) {

    const { products } = this.state;

    const options = [];

    for (const productKey in products) {
      const productName = products[productKey];
      options.push((
        <option
          key={productKey}
          value={productKey}>
          {productName}
        </option>
      ));
    }

    const { editRowValues } = this.state;
    const editState = (e, dataKey) => {

      this.onUpdate();

      const { type } = e;

      if (e.key === 'Enter') {
        this.onCloseEdit();
      }

      if (e.key === 'Enter' || type === 'blur' || type === 'change') {
        editRowValues[dataKey] = e.target.value;
        this.setState({ editRowValues }, () => {
          this.onEditLot(key, editRowValues);
        });
      }

    };
    const editItem = (value, key) => {
      return (
        <FormGroup>
          <FormControl
            defaultValue={value}
            onChange={e => editState(e, key)}
            onKeyPress={e => editState(e, key)}
            onBlur={e => editState(e, key)}
            type='text'/>
        </FormGroup>
      );
    };

    return (

      <tr key={key}>
        <td>{editItem(tally, 'tally')}</td>
        <td>
          <FormGroup>
            <FormControl
              defaultValue={selectedProduct}
              componentClass='select'
              placeholder='select'
              onChange={e => editState(e, 'selectedProduct')}
              type='select'>
              {options}
            </FormControl>
          </FormGroup>
        </td>
        <td>{editItem(description, 'description')}</td>
        <td>{editItem(price, 'price')}</td>
        <td>{editItem(commissionOverride, 'commissionOverride')}</td>
        <td className='text-right'>
          <ButtonToolbar>
            <Button
              onClick={() => this.onToggleEditLot(key)}
              bsSize='xsmall'
              bsStyle='info'>
              <Glyphicon glyph='pencil'></Glyphicon>
            </Button>
            <Button
              onClick={() => this.onDelete(key)}
              bsSize='xsmall'
              bsStyle='danger'>
              <Glyphicon glyph='remove'></Glyphicon>
            </Button>
          </ButtonToolbar>
        </td>
      </tr>

    );

  }

  edit() {

    const
      { tally
      , description
      , price
      , commissionOverride
      , products
      } = this.state;

    const options = [];

    for (const productKey in products) {
      const productName = products[productKey];
      options.push((
        <option
          key={productKey}
          value={productKey}>
          {productName}
        </option>
      ));
    }

    return (
      <tr>
        <td>
          <FormGroup>
            <FormControl
              ref='tally'
              value={tally}
              onKeyDown={e => this.onChange(e)}
              onChange={e => this.onTally(e)}
              placeholder='Tally...'
              type='text'/>
          </FormGroup>
        </td>
        <td>
          <FormGroup>
            <FormControl
              componentClass='select'
              placeholder='select'
              onChange={e => this.onProduct(e)}
              type='select'>
              {options}
            </FormControl>
          </FormGroup>
        </td>
        <td>
          <FormGroup>
            <FormControl
              value={description}
              onKeyDown={e => this.onChange(e)}
              onChange={e => this.onDescription(e)}
              placeholder='Description...'
              type='text'/>
          </FormGroup>
        </td>
        <td>
          <FormGroup>
            <FormControl
              value={price}
              onKeyDown={e => this.onChange(e)}
              onChange={e => this.onPrice(e)}
              placeholder='Price...'
              type='text'/>
          </FormGroup>
        </td>
        <td>
          <FormGroup>
            <FormControl
              value={commissionOverride}
              onKeyDown={e => this.onChange(e)}
              onChange={e => this.onCommission(e)}
              placeholder='Commission override...'
              type='text'/>
          </FormGroup>
        </td>
        <td>
          <FormGroup>
            <FormControl
              onClick={() => this.onChange({ key: 'Enter' })}
              value='Add'
              type='button'/>
          </FormGroup>
        </td>
      </tr>
    );
  }

  toggleCommission() {
    this.setState({ commissionOverride: !this.state.commissionOverride });
  }

  onChange(e) {

    if (e.key !== 'Enter') {
      return false;
    }

    const
      { tally
      , description
      , price
      , selectedProduct
      , commissionOverride
      } = this.state;

    const object = {
      tally, description, price, commissionOverride, selectedProduct
    };

    if (tally && description && price) {
      const data = [...this.props.data, object];
      safeOnChange(this, deepCopy(data));
      this.reset();
    }

    if (typeof this.props.validation !== 'undefined') {
      this.props.dispatch(actions.validate(this.props.identifier));
    }

  }

  onUpdate() {
    safeOnChange(this, deepCopy(this.props.data));
  }

  // Makes changes to the edited lot
  onEditLot(i, newValues: any) {

    if (newValues) {
      const oldValues = this.props.data[i];
      const updatedValues = Object.assign({}, oldValues, newValues);
      const data = [
        ...this.props.data.slice(0, i),
        updatedValues,
        ...this.props.data.slice(i + 1)
      ];
      safeOnChange(this, deepCopy(data));
    }

  }

  onCloseEdit() {
    this.setState({ editRow: null });
  }

  // Sets up a row for editing
  onToggleEditLot(i) {

    const { editRow, editRowValues } = this.state;

    if (editRow === i) {
      this.onEditLot(i, editRowValues);
    } else {
      this.setState({ editRow: i });
      this.setState({ editRowValues: {} });
    }

  }

  onDelete(i) {
    this.setState({ editRow: null });
    const data = [
      ...this.props.data.slice(0, i),
      ...this.props.data.slice(i + 1)
    ];
    safeOnChange(this, deepCopy(data));
  }

  reset() {
    this.setState(
      {
        tally: '',
        description: '',
        price: '',
        overrideCommission: ''
      }
    );
  }

  onTally(e) {
    this.setState({ tally: e.target.value });
  }

  onDescription(e) {
    this.setState({ description: e.target.value });
  }

  onPrice(e) {
    this.setState({ price: e.target.value });
  }

  onCommission(e) {
    this.setState({ commissionOverride: e.target.value });
  }

  onProduct(e) {
    this.setState({ selectedProduct: e.target.value });
  }

}
