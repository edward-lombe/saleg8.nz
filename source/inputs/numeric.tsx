'use strict';

import * as React from 'react';

import { safeOnChange } from '../helpers';

const Bootstrap = require('react-bootstrap');
const { FormControl, FormGroup, ControlLabel } = Bootstrap;

export default class extends React.Component<any, any> {

  render() {
    return (
      <FormGroup>
        <ControlLabel>{this.props.label}</ControlLabel>
        <FormControl
          type='number'
          value={this.props.data}
          onChange={e => this.onChange(e)}/>
      </FormGroup>
    );
  }

  onChange(e) {
    safeOnChange(this, e.target.value);
  }

}

export class Large extends React.Component<any, any> {

  render() {
    return (
      <FormGroup>
        <ControlLabel>{this.props.label} should be large</ControlLabel>
        <FormControl
          type='text'
          value={this.props.data}
          onChange={e => this.onChange(e)}/>
      </FormGroup>
    );
  }

  onChange(e) {
    safeOnChange(this, e.target.value);
  }

}
