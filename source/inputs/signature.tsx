'use strict';

import * as React from 'react';
import { findDOMNode } from 'react-dom';

import * as actions from '../actions';

import { safeOnChange } from '../helpers';

import { Button, ButtonGroup } from 'react-bootstrap';

const debounce = require('debounce');
const SignaturePad = require('signature_pad');

export default class extends React.Component<any, any> {

  constructor() {
    super();
    this.state = {
      editing: false,
      scaled: false
    };
  }

  componentDidMount() {

    const canvas = this['_canvas'];

    const signaturePad =
      this['signaturePad'] = new SignaturePad(this['_canvas']);

    this['signaturePad'].onEnd = () => {
      safeOnChange(this, this['signaturePad'].toDataURL());
    };

    this['signaturePad'].onBegin = () => {
      if (this.state.scaled) {
        this.clear();
      }
    };

    if (this.props.data !== '') {
      this['signaturePad'].fromDataURL(this.props.data);
      const ratio =  Math.max(window.devicePixelRatio || 1, 1);
      canvas.getContext('2d').scale(ratio, ratio);
      this.setState({ ratio, scaled: true });
    }

  }

  componentDidUpdate() {
    if (this.props.data === '') {
      this['signaturePad'].clear();
    }
  }

  clear() {
    safeOnChange(this, '');
    if (this.state.scaled) {
      const ratio = 1 / this.state.ratio;
      const canvas = this['_canvas'];
      canvas.getContext('2d').scale(ratio, ratio);
      this.setState({ scaled: false });
    }
  }

  render() {

    const { data } = this.props;
    const { editing } = this.state;
    const style = {
      boxSizing: 'border-box',
      border: '1px dashed grey',
      borderRadius: '5px'
    };

    const buttonStyle = {
      position: 'absolute',
      top: 0,
      right: 0
    };

    return (
      <div>
        <Button style={buttonStyle} onClick={() => this.clear()}>
          Clear
        </Button>
        <canvas
          style={style}
          ref={c => this['_canvas'] = c}
          width={400}
          height={100}>
        </canvas>
      </div>
    );
  }

}

export class Signature extends React.Component<any, any> {

  constructor() {
    super();
    this.state = {
      canvas: null,
      img: null,
      edit: true,
      refs: {},
      cleared: 0
    };
  }

  componentWillMount() {
    this.setRefs();
  }

  componentDidMount() {
    const { data } = this.props;
    const { canvas } = this.state.refs;
    if (typeof data === 'undefined' || data === '') {
      this.showPad();
    } else {
      this.state.refs.img.src = data;
      this.showImg();
    }
  }

  setRefs() {
    const style = {
      boxSizing: 'border-box',
      border: '1px dashed grey',
      borderRadius: '5px',
      display: 'none'
    };
    const img =
      (<img style={style} src='' ref={r => this.initImg(r) } alt=''/>);
    const canvas =
      (<canvas width={300} style={style} ref={r => this.initPad(r) }></canvas>);
    this.setState({ img, canvas });
  }

  initImg(img) {
    const { refs } = this.state;
    refs.img = img;
    this.setState({ refs });
  }

  initPad(canvas) {
    const { refs } = this.state;
    const signaturePad = new SignaturePad(canvas);
    refs.canvas = canvas;
    this.setState({ signaturePad });
  }

  render() {

    const { img, canvas, edit } = this.state;
    const buttonText = edit ? 'Save' : 'Edit';
    const buttonStyle = {
      position: 'absolute',
      top: 0,
      right: 0
    };

    return (
      <div>
        {img}
        {canvas}
        <br/>
        <Button style={buttonStyle} onClick={() => this.toggle()}>
          {buttonText}
        </Button>
      </div>
    );
  }

  toggle() {

    const src = this.state.signaturePad.toDataURL();

    if (this.state.edit) {
      this.onChange(src);
      this.state.refs.img.src = src;
      this.showImg();
    } else {
      this.showPad();
    }

  }

  componentDidUpdate(prevProps) {
    const { data } = this.props;
    console.log('props change',this.props, prevProps);
    if (data === '') {
      console.log('should clear');
      this.clear();
    }
  }

  showPad() {

    const { signaturePad } = this.state;

    if (typeof signaturePad === 'undefined') {
      return;
    }

    if (typeof signaturePad.clear !== 'function') {
      return;
    }

    signaturePad.clear();

    this.state.refs.canvas.style.display = 'block';
    this.state.refs.img.style.display = 'none';
    this.setState({ edit: true });

  }

  clear() {

    const { signaturePad, cleared } = this.state;
    const now = Date.now();

    if (Math.abs(now - cleared) < 5000) {
      return;
    }

    if (typeof signaturePad === 'undefined') {
      return;
    }

    if (typeof signaturePad.clear !== 'function') {
      return;
    }

    this.setState({ cleared: now });
    signaturePad.clear();
    this.showImg();

  }

  showImg() {
    this.state.refs.canvas.style.display = 'none';
    this.state.refs.img.style.display = 'block';
    this.setState({ edit: false });
  }

  onChange(data) {

    console.log('new data is', data)

    if (typeof this.props.onChange === 'function') {
      this.props.onChange(data);
    }
  }

}


