'use strict';

import * as React from 'react';

import * as actions from '../actions';
import { safeOnChange } from '../helpers';

import
  { FormControl
  , FormGroup
  , ControlLabel
  , HelpBlock
  } from 'react-bootstrap';

export default class extends React.Component<any, any> {

  render() {

    const feedbackText = this.props.feedbackText || '';
    let validProp = {};

    if (feedbackText !== '') {
      const validationState = 'warning';
      validProp = { validationState };
    }

    return (
      <FormGroup {...validProp}>
        <ControlLabel>{this.props.label}</ControlLabel>
        <FormControl
          type='text'
          value={this.props.data}
          onChange={e => this.onChange(e)}/>
        <HelpBlock>{feedbackText}</HelpBlock>
      </FormGroup>
    );
  }

  onChange(e) {
    safeOnChange(this, e.target.value);
    if (typeof this.props.validation !== 'undefined') {
      this.props.dispatch(actions.validate(this.props.identifier));
    }
  }

}
