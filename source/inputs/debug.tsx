'use strict';

import * as React from 'react';

import { safeOnChange } from '../helpers';

const Bootstrap = require('react-bootstrap');
const { FormControl, FormGroup, ControlLabel } = Bootstrap;

export default class extends React.Component<any, any> {

  render() {
    return (
      <div>
        <pre>
          <code>
            {JSON.stringify(this.props, null, '  ')}
          </code>
        </pre>
        <hr/>
        <FormGroup>
          <ControlLabel>{this.props.label}</ControlLabel>
          <FormControl
            type='number'
            value={this.props.data}
            onChange={e => this.onChange(e)}/>
        </FormGroup>
      </div>

    );
  }

  onChange(e) {
    safeOnChange(this, e.target.value);
  }

}
