'use strict';

// TODO: rename to radio lol

import * as React from 'react';

import * as actions from '../actions';
import
  { entries
  , capitalize
  , safeOnChange
  , convertStringSelect
  , deepCopy
  } from '../helpers';

import
  { FormControl
  , FormGroup
  , ControlLabel
  , Glyphicon
  , HelpBlock
  , Row
  , Col
  , Radio
  } from 'react-bootstrap';

const Static = FormControl['Static'];

export default class extends React.Component<any, any> {

  render() {

    const items = this.renderItems();
    const { valid } = this.props;
    const feedbackText = this.props.feedbackText || '';
    let validProp = {};

    if (typeof valid === 'boolean' && !valid) {
      const validationState = 'error';
      validProp = { validationState };
    }

    return (
      <FormGroup {...validProp}>
        <ControlLabel>{this.props.label}</ControlLabel>
        <Static>
          {items}
        </Static>
        <HelpBlock>{feedbackText}</HelpBlock>
      </FormGroup>
    );

  }

  renderItems() {

    let { data } = this.props;

    data = convertStringSelect(data);

    return entries(data).map(([value, checked], i) => {

      if (typeof checked !== 'boolean') {
        let castChecked: boolean;
        if (checked === '0') {
          castChecked = false;
        } else if (checked === '1') {
          castChecked = true;
        }
        return this.renderItem(i, value, castChecked);
      } else {
        return this.renderItem(i, value, checked);
      }

    });

  }

  renderItem(key: number, text: string = '', checked: boolean = false) {
    return (
      <Radio
        onChange={() => this.onChange(text)}
        checked={checked}
        key={key}
        inline>
        {capitalize(text)}
      </Radio>
    );
  }

  onChange(selectedKey: string) {

    let data = Object.keys(this.props.data)
      .reduce(
        (object, key) => {
          object[key] = (key === selectedKey);
          return object;
        },
        {}
      );

    data = convertStringSelect(data);

    const dataDereferenced = deepCopy(data);

    safeOnChange(this, dataDereferenced);

    if (typeof this.props.validation !== 'undefined') {
      this.props.dispatch(actions.validate(this.props.identifier));
    }

  }

}
