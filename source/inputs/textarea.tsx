'use strict';

import * as React from 'react';

import { safeOnChange } from '../helpers';

const Bootstrap = require('react-bootstrap');
const { FormControl, FormGroup, ControlLabel } = Bootstrap;

export default class extends React.Component<any, any> {

  render() {
    return (
      <FormGroup>
        <ControlLabel>{this.props.label}</ControlLabel>
        <FormControl
          rows={10}
          componentClass='textarea'
          value={this.props.data}
          onChange={e => this.onChange(e)}/>
      </FormGroup>
    );
  }

  onChange(e) {
    safeOnChange(this, e.target.value);
  }

}
