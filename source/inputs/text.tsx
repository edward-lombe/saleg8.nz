'use strict';

import * as React from 'react';

import * as data from '../data';
import * as id from '../identifier';
import * as actions from '../actions';
import { safeOnChange } from '../helpers';
import { ClientModal } from '../components/client-search';

import
  { FormControl
  , FormGroup
  , ControlLabel
  , Glyphicon
  , HelpBlock
  } from 'react-bootstrap';

const { InputGroup } = require('react-bootstrap');

export default class extends React.Component<any, any> {

  render() {

    const { valid } = this.props;
    const feedbackText = this.props.feedbackText || '';
    let validProp = {};

    if (typeof valid === 'boolean' && !valid) {
      const validationState = 'error';
      validProp = { validationState };
    }

    return (
      <FormGroup {...validProp}>
        <ControlLabel>{this.props.label}</ControlLabel>
        <FormControl
          type='text'
          value={this.props.data}
          onChange={e => this.onChange(e)}/>
        <HelpBlock>{feedbackText}</HelpBlock>
      </FormGroup>
    );
  }

  onChange(e) {

    /** safeOnChange(this, e.target.value); */
    const { dispatch, identifier, label } = this.props;

    dispatch(actions.setInput({
      identifier, data: e.target.value
    }));

    if (typeof this.props.validation !== 'undefined') {

      const result =
        dispatch(actions.validate(identifier));
      if (!result) {
        dispatch(actions.setStatusInvalid(label + ' is not valid'));
      }

    }

  }

  shouldComponentUpdate(nextProps, nextState) {

    const { identifier } = this.props;
    const newData = data.get(nextProps.app, identifier);
    const oldData = data.get(this.props.app, identifier);

    return (oldData !== newData) || (nextProps.valid !== this.props.valid);
  }

}

export class VendorLookup extends React.Component<any, any> {

  constructor() {
    super();
    this.state = {
      showModal: false
    };
  }

  render() {

    const { showModal } = this.state;

    return (
      <FormGroup>
        <ControlLabel>{this.props.label}</ControlLabel>
        <InputGroup>
          <FormControl
            type='text'
            value={this.props.data}
            onChange={e => this.onChange(e)}/>
          <InputGroup.Addon
            onClick={() => this.setState({ showModal: !showModal })}>
            <Glyphicon glyph='search' />
          </InputGroup.Addon>
        </InputGroup>
        <ClientModal
          onClose={() => this.setState({ showModal: false })}
          show={showModal}
          data={this.getData()}
          onChange={e => this.onLookup(e)}
          type='vendor'/>
      </FormGroup>
    );
  }

  getData() {
    const clientData = data.get(this.props.app, id.common.clientDisplay);
    return clientData;
  }

  onChange(e) {
    safeOnChange(this, e.target.value);
  }

  onLookup(e) {
    this.props.dispatch(actions.loadClientIntoSaleNote(e));
    this.setState({ showModal: false });
  }

}

export class PurchaserLookup extends React.Component<any, any> {

  constructor() {
    super();
    this.state = {
      showModal: false
    };
  }

  render() {

    const { showModal } = this.state;

    return (
      <FormGroup>
        <ControlLabel>{this.props.label}</ControlLabel>
        <InputGroup>
          <FormControl
            type='text'
            value={this.props.data}
            onChange={e => this.onChange(e)}/>
          <InputGroup.Addon
            onClick={() => this.setState({ showModal: !showModal })}>
            <Glyphicon glyph='search' />
          </InputGroup.Addon>
        </InputGroup>
        <ClientModal
          onClose={() => this.setState({ showModal: false })}
          show={showModal}
          data={this.getData()}
          onChange={e => this.onLookup(e)}
          type='purchaser'/>
      </FormGroup>
    );
  }

  getData() {
    const clientData = data.get(this.props.app, id.common.clientDisplay);
    return clientData;
  }

  onChange(e) {
    safeOnChange(this, e.target.value);
  }

  onLookup(e) {
    this.props.dispatch(actions.loadClientIntoSaleNote(e));
    this.setState({ showModal: false });
  }

}

export class Large extends React.Component<any, any> {

  render() {
    return (
      <FormGroup>
        <ControlLabel>{this.props.label}</ControlLabel>
        <FormControl
          type='text'
          bsSize='large'
          value={this.props.data}
          onChange={e => this.onChange(e)}/>
      </FormGroup>
    );
  }

  onChange(e) {
    safeOnChange(this, e.target.value);
  }

}
