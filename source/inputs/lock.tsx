'use strict';

import * as React from 'react';

import * as actions from '../actions';
import
  { FormControl
  , FormGroup
  , ControlLabel
  , Glyphicon
  , HelpBlock
  , Row
  , Col
  , Radio
  , ButtonGroup
  , Button
  } from 'react-bootstrap';

export default class extends React.Component<any, any> {

  render() {

    const locked = this.props.data;

    console.log('locked', locked);

    let lockDisabled: boolean, unlockDisabled: boolean;

    if (locked !== 'locked') {
      lockDisabled = false;
      unlockDisabled = true;
    } else {
      lockDisabled = true;
      unlockDisabled = false;
    }

    return (
      <FormGroup>
        <ControlLabel>{this.props.label}</ControlLabel>
        <FormControl.Static>
          <ButtonGroup>
            <Row>
              <Col xs={6}>
                <Button
                  block
                  bsStyle='danger'
                  onClick={() => this.lockSettings()}
                  disabled={lockDisabled}>
                  <Glyphicon glyph='lock'/> Lock
                </Button>
              </Col>
              <Col xs={6}>
                <Button
                  block
                  bsStyle='success'
                  onClick={() => this.unlockSettings()}
                  disabled={unlockDisabled}>
                  <Glyphicon glyph='edit'/> Unlock
                </Button>
              </Col>
            </Row>
          </ButtonGroup>
        </FormControl.Static>
        <HelpBlock>{this.props.feedbackText}</HelpBlock>
      </FormGroup>
    );
  }

  lockSettings() {

    const { dispatch } = this.props;

    dispatch(actions.lockSettings());

  }

  unlockSettings() {

    const { dispatch } = this.props;

    dispatch(actions.unlockSettings());

  }

}
