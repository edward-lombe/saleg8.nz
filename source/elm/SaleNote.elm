module SaleNote exposing (..)

import Dict exposing (Dict)
import Json.Decode
    exposing
        ( string
        , Decoder
        , Value
        )
import Json.Decode.Pipeline
    exposing
        ( decode
        , required
        , optional
        )


type alias Object =
    Dict String String


type alias Forms a =
    { forms : a }


type alias Inputs a =
    { inputs : a }


type alias Input a =
    { data : a
    , label : String
    , name : String
    , type' : String
    }


type alias LotsForm =
    { stockDescriptionTable : Input (List Object)
    }


type alias NotesForm =
    { administratorNotes : Input String
    , internalNotes : Input String
    , purchaserNotes : Input String
    , vendorNotes : Input String
    }


type alias PurchaserForm =
    { gstNumber : Input String
    , naitNumber : Input String
    , agentCode : Input String
    , agentName : Input String
    , dateOfBirth : Input String
    , emailAddress : Input String
    , entityName : Input String
    , entityType : Input String
    , farmSourceLivestockAccount : Input String
    , fullName : Input String
    , herdPTPTCode : Input String
    , mobileNumber : Input String
    , phoneNumber : Input String
    , postalAddress : Input String
    , rebateRate : Input String
    }


type alias TestForm =
    { oneField : Input String }



-- decodePurchaserForm : Decoder PurchaserForm
-- decodePurchaserForm =
--     decode PurchaserForm
--         |> r "gstNumber" s
--         |> r "naitNumber" s
--         |> r "agentCode" s
--         |> r "agentName" s
--         |> r "dateOfBirth" s
--         |> r "emailAddress" s
--         |> r "entityName" s
--         |> r "entityType" s
--         |> r "farmSourceLivestockAccount" s
--         |> r "fullName" s
--         |> r "herdPTPTCode" s
--         |> r "mobileNumber" s
--         |> r "phoneNumber" s
--         |> r "postalAddress" s
--         |> r "rebateRate" s


type alias VendorForm =
    { gstNumber : Input String
    , naitNumber : Input String
    , agentCode : Input String
    , agentName : Input String
    , dateOfBirth : Input String
    , emailAddress : Input String
    , entityName : Input String
    , entityType : Input String
    , farmSourceLivestockAccount : Input String
    , fullName : Input String
    , herdPTPTCode : Input String
    , mobileNumber : Input String
    , phoneNumber : Input String
    , postalAddress : Input String
    , rebateRate : Input String
    , security : Input String
    , thirdParty : Input String
    }



-- decodeVendorForm : Decoder VendorForm
-- decodeVendorForm =
--     decode VendorForm
--         |> rs "gstNumber"
--         |> rs "naitNumber"
--         |> rs "agentCode"
--         |> rs "agentName"
--         |> rs "dateOfBirth"
--         |> rs "emailAddress"
--         |> rs "entityName"
--         |> rs "entityType"
--         |> rs "farmSourceLivestockAccount"
--         |> rs "fullName"
--         |> rs "herdPTPTCode"
--         |> rs "mobileNumber"
--         |> rs "phoneNumber"
--         |> rs "postalAddress"
--         |> rs "rebateRate"
--         |> rs "security"
--         |> rs "thirdParty"


type alias SaleNote =
    { purchaserForm :
        PurchaserForm
    , vendorForm :
        VendorForm
    }


stringInput : Input String
stringInput =
    Input "" "" "" ""


decode : Value -> Result String SaleNote
decode a =
    Err "Not implemented"
