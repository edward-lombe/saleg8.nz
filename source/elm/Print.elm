module Print exposing (..)

import String


print : a -> String
print a =
    let
        stringified =
            toString a
    in
        ""


prettyify : String -> String
prettyify a =
    ""


indent : Int -> String
indent n =
    String.repeat n " "
