module Main exposing (..)

import Html exposing (Html, text)
import Html.App exposing (program, programWithFlags)
import Json.Encode exposing (Value)
import Debug


-- Local Imports

import SaleNote


type alias Model =
    { saleNote : SaleNote.SaleNote
    }


type Message
    = NoOp


model : Model
model =
    { saleNote = SaleNote.saleNote
    }


effects : Cmd Message
effects =
    Cmd.none


init : Value -> ( Model, Cmd Message )
init value =
    let
        a =
            Debug.log "flags" (toString value)

        b =
            case SaleNote.decode value of
                Err error ->
                    Debug.log "error" (toString error)

                Ok v ->
                    Debug.log "value" (toString value)
    in
        ( model
        , effects
        )


update : Message -> Model -> ( Model, Cmd Message )
update message model =
    ( model
    , Cmd.none
    )


view : Model -> Html a
view model =
    text "0ne in a million"


main : Program Value
main =
    programWithFlags
        { init = init
        , update = update
        , view = view
        , subscriptions = always Sub.none
        }
