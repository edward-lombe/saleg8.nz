'use strict';

import * as React from 'react';
import * as reactRedux from 'react-redux';

import * as data from './data';
import * as pages from './pages/index';
import * as forms from './forms/index';
import * as inputs from './inputs/index';
import * as helpers from './helpers';
import * as actions from './actions';
import * as id from './identifier';
import { IIdentifier } from './interfaces';

/**
 * Takes a type string such as 'inputs/text' and
 * returns the unconnected react component.
 */
export function select(type: string): React.ComponentClass<any> {

  const componentType = type.split('/')[0];
  const componentSubType = type.split('.')[1] || 'default';
  const componentName = componentSubType === 'default'
    ? helpers.hyphonToCamel(type.split('/')[1])
    : helpers.hyphonToCamel(helpers.sliceCharacters(type, '/', '.'));

  try {

    switch (componentType) {

      case 'pages':

        return pages[componentName][componentSubType];

      case 'forms':

        return forms[componentName][componentSubType];

      case 'inputs':

        return inputs[componentName][componentSubType];

    }

  } catch (error) {
    console.error(
      `could not find component: ${componentName}, make sure to update indexes`
    );
  }

  return pages.indexPage.default;

}

/**
 * Takes the appData and an identifier within that data
 * and returns the component 'wired' up to the data that
 * is specified by the identifier
 */
export function connect(appData, identifier: IIdentifier) {

  const componentData = data.get(appData, identifier, true);
  const component = select(componentData.type);
  let mapState = state => Object.assign({}, state, { identifier });
  let mapDispatch = dispatch => ({ dispatch: a => dispatch(a) });

  switch (identifier.type) {

    case 'input':

      mapState = state => {
        return Object.assign(
          {},
          state,
          componentData,
          data.get(state.app, identifier, true),
          { identifier }
        );
      };

      mapDispatch = dispatch => ({
        dispatch: a => dispatch(a),
        onChange: data => {
          dispatch(actions.setInput({ identifier, data }));
        }
      });

      break;

    case 'form':

      const propInputs = generatePropComponents(appData, identifier);

      mapState = state => {
        return Object.assign({}, state, { inputs: propInputs }, { identifier });
      };

      break;

    case 'page':

      const propForms = generatePropComponents(appData, identifier);

      mapState = state => {
        return Object.assign({}, state, { forms: propForms }, { identifier });
      };

      break;

    default:

      break;

  }

  return reactRedux.connect(mapState, mapDispatch)(component);

}

/**
 * Takes the appData and an identifier and returns
 * an object with the connected component as the keys.
 * The result of this function can the be passed
 * to a component through it props.
 * @example
 * <Component forms={generatePropComponents(appData, identifier)}
 */
function generatePropComponents(appData, componentIdentifier: IIdentifier) {

  const propComponents = {};
  const componentData = data.get(appData, componentIdentifier, true);

  if (componentIdentifier.type === 'page') {
    for (const formName in componentData.forms) {
      const capitalName = helpers.capitalize(formName);
      const newIdentifier = id.modify(componentIdentifier, {
        type: 'form', form: formName
      });
      propComponents[capitalName] = connect(appData, newIdentifier);
    }
  } else if (componentIdentifier.type === 'form') {
    for (const inputName in componentData.inputs) {
      const capitalName = helpers.capitalize(inputName);
      const newIdentifier = id.modify(componentIdentifier, {
        type: 'input', input: inputName
      });
      propComponents[capitalName] = connect(appData, newIdentifier);
    }
  } else {
    ;
  }

  return propComponents;

}
