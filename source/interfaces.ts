'use strict';

export interface IIdentifier {
  type: string;
  page: string;
  form?: string;
  input?: string;
}
