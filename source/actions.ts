'use strict';

/**
 * actions.ts
 *
 * Manages the action dispatch
 * for all state mutation. I hope.
 */

import
  { decapitalize
  , isUppercase
  , pstrify
  , getVersion
  , convertStringSelect
  } from './helpers';
import * as data from './data';
import * as id from './identifier';
import * as validators from './validators';
import { IIdentifier } from './interfaces';
import * as cryptoJS from 'crypto-js';

import * as saleNoteActions from './actions/sale-note'

export const SET_INPUT = 'SET_INPUT';
export const CONFIGURE_INPUT = 'CONFIGURE_INPUT';

/**
 * Our single atomic 'form-action' that
 * allows us to manipulate all form values
 */
interface ISetInput {
  data: any;
  identifier: IIdentifier;
}
export function setInput(options: ISetInput) {

  const { identifier, data } = options;
  const type = SET_INPUT;
  const payload = { identifier, data };

  return { type, payload };

}

export function lockSettings() {

  const lock = () => {
    const identifier = id.common.settingsLocked;

    const options = {
      identifier,
      data: 'locked'
    };

    return setInput(options);
  };


  return (dispatch, getState) => {

    const appData = getState().app;
    const settingsHash = data.get(appData, id.common.settingsHash);
    const settingsPassword = data.get(appData, id.common.settingsPassword);
    const settingsLocked = data.get(appData, id.common.settingsLocked);

    if (settingsLocked === 'locked') {
      return;
    }

    dispatch(lock());

    dispatch(setInput({
      identifier: id.common.settingsPassword,
      data: ''
    }));

  };

}

export function unlockSettings() {

  const unlock = () => {
    return setInput({
      identifier: id.common.settingsLocked,
      data: 'unlocked'
    });
  };

  return (dispatch, getState) => {

    const appData = getState().app;
    const settingsHash = data.get(appData, id.common.settingsHash);
    const settingsPassword = data.get(appData, id.common.settingsPassword);
    const passwordHash = hash(settingsPassword);

    if (settingsPassword === '') {
      dispatch(setFeedbackText(
        id.common.settingsLocked,
        'Please enter a password first'
      ));
      return;
    } else {
      dispatch(setFeedbackText(
        id.common.settingsLocked,
        ''
      ));
    }

    if (passwordHash === hash('make it happen')) {
      dispatch(unlock());
    } else {
      window.alert('Incorrect password');
    }

    dispatch(setInput({
      identifier: id.common.settingsPassword,
      data: ''
    }));

  };
}

function hash(input: string) {
  return cryptoJS.SHA1(input).toString();
}

/**
 * Validates the content of a particular identifier,
 * provided it has some sort of validation specified
 * in the db.json file
 *
 * @export
 * @param {any} identifier
 * @returns
 */
export function validate(identifier) {
  return (dispatch, getState) => {

    const appData = getState().app;
    const fullData = data.get(appData, identifier, true);

    if (typeof fullData.validation === 'undefined') {
      const message =
        'Cannot validate an input with no validation data';
      console.error(message);
      return;
    }

    const { validation } = fullData;
    const value = fullData.data;
    let results: any[] = [];

    for (const validationKey in validation) {

      if (!(validationKey in validators)) {
        console.error(`${validationKey} not found in validators`);
        continue;
      }

      const validator = validators[validationKey];

      if (typeof validator !== 'function') {
        console.error(`validators.${validationKey} is not a function`);
        continue;
      }

      const validationOptions = validation[validationKey];
      const result = validator(value, validationOptions);

      results = [...results, result];

    }

    const allValid = results.every(v => !!v.valid);

    if (!allValid) {
      dispatch(setStatusInvalid(fullData.label + ' is not valid'));
    }

    dispatch(setValid(identifier, allValid));

    const messages = results.map(v => v.message).join('');

    dispatch(setFeedbackText(identifier, messages));

    return allValid;

  };
}

/**
 * Takes a whole page and validates all children of that page,
 * provided they are a validate-able control
 *
 * @export
 * @param {any} pageIdentifier
 * @returns
 */
export function validatePage(pageIdentifier) {
  return (dispatch, getState) => {

    const appData = getState().app;
    const results: boolean[] = [];

    for (const [identifier, input] of data.iterate(appData, ['input'])) {

      if (identifier.page !== pageIdentifier.page) {
        continue;
      }

      if (typeof input.validation === 'undefined') {
        continue;
      }

      const valid = dispatch(validate(identifier));

      results.push(valid);

    }

    return results.every(v => v);

  };
}

/**
 * Sets the text underneath the control, this is mainly used
 * for displaying error messages and warnings
 *
 * @export
 * @param {IIdentifier} identifier
 * @param {string} feedbackText
 * @returns
 */
export function setFeedbackText(identifier: IIdentifier, feedbackText: string) {

  const type = CONFIGURE_INPUT;
  const payload = { identifier, feedbackText };

  return { type, payload };

}

/**
 * Sets a control as either valid or invalid
 *
 * @export
 * @param {IIdentifier} identifier
 * @param {boolean} valid
 * @returns
 */
export function setValid(identifier: IIdentifier, valid: boolean) {

  const type = CONFIGURE_INPUT;
  const payload = { identifier, valid };

  return { type, payload };

}

/**
 * A few actions that we like to perform together
 * on startup, these are also called when the user
 * navigates to the home page
 *
 * @export
 * @returns
 */
export function initial() {
  return (dispatch, getState) => {
    dispatch(attemptToSetOldAgentCode());
    setTimeout(() => {
      dispatch(setFormVersion());
      dispatch(getAgentData());
      dispatch(getAgentSettings());
      dispatch(getSaleNotes());
    }, 5000)
  };
}

export function attemptToSetOldAgentCode() {
  return (dispatch, getState) => {
    try {
      const appData = getState().app
      const currentAgentCode = data.get(appData, id.common.agentCode)
      if (currentAgentCode !== '') {
        return
      }
      const store = JSON.parse(localStorage.getItem('store'))
      const inputs = store.app.pages.settingsPage.forms.settingsForm.inputs
      for (const inputKey in inputs) {
        const data = inputs[inputKey].data
        const identifier = id.generate('settingsPage', 'settingsForm', inputKey)
        dispatch(setInput({
          identifier, data
        }))
      }
    } catch (error) {
      console.log('Error trying to restore agent code')
      console.log(error)
    }
  }
}

/**
 * Takes an input and resets it to its default state
 * as specified in `data.reset`
 *
 * @export
 * @param {any} identifier
 * @returns
 */
export function resetInput(identifier) {

  return (dispatch, getState) => {

    const inputData = data.get(getState().app, identifier, true);
    const resetData = data.reset(inputData, identifier);

    dispatch(setInput({ identifier, data: resetData }));

  };

}

/**
 * Takes an array of key value fields and then sets a form with it
 *
 * For example an object like { input1: 'text', input2: 'text' }
 * can be used, instead of having to specify input, name and
 * label fields
 *
 * @export
 * @param {any} { identifier, data }
 * @returns
 */
export function setForm({ identifier, data }) {

  return dispatch => {

    for (const inputKey in data) {

      const inputIdentifier = id.modify(identifier, {
        input: inputKey, type: 'input'
      });

      dispatch(setInput({ identifier: inputIdentifier, data: data[inputKey] }));

    }

  };

}

export function resetForm(identifier) {

  return (dispatch, getState) => {

    const appData = getState().app;
    const formData = data.get(appData, identifier);

    for (const inputKey in formData) {

      const inputIdentifier = id.modify(identifier, {
        input: inputKey, type: 'input'
      });

      dispatch(resetInput(inputIdentifier));

    }

  };

}

export function setPage({ identifier, pageData }) {
  return (dispatch, getState) => {

    const appData = getState().app;

    for (const formKey in pageData) {

      const formIdentifier = id.modify(identifier, {
        form: formKey, type: 'form'
      });

      const newFormData = pageData[formKey];
      const oldFormData = data.get(appData, formIdentifier);

      let dataUpdated: boolean = false;

      /**
       * Checks if the data has changed, and skips if it has not
       */
      for (const inputKey in oldFormData) {
        const [oldData, newData] =
          [oldFormData[inputKey], newFormData[inputKey]];
        if (oldData !== newData) {
          dataUpdated = true;
          break;
        }
      }

      if (dataUpdated) {
        dispatch(setForm({
          identifier: formIdentifier,
          data: pageData[formKey]
        }));
      }

    }

  };
}

export function resetPage(identifier) {
  return (dispatch, getState) => {

    const appData = getState().app;
    const pageData = data.get(appData, identifier);

    for (const formKey in pageData) {

      const formIdentifier = id.modify(identifier, {
        form: formKey, type: 'form'
      });

      dispatch(resetForm(formIdentifier));

    }

  };
}

export function setSaleNotes(data: any[]) {

  const { saleNoteDisplay } = id.common;

  return dispatch => {

    dispatch(setInput({ identifier: saleNoteDisplay, data }));

  };

}

/**
 * This function calls the email webMethod on the Cache server,
 * and also converts our mode into the correct one ('V', 'P' or ''
 * otherwise known as the null string or <blank>, not to be
 * confused with the popular 00's <blink> tag)
 *
 * @export
 * @param {('vendor' | 'purchaser' | 'all')} to
 * @returns
 */
export function email(to: 'vendor' | 'purchaser' | 'all') {

  let mode: string;

  switch (to) {

  case 'vendor':
    mode = 'V';
    break;

  case 'purchaser':
    mode = 'P';
    break;

  case 'all':
    mode = '';
    break;

  default:
    break;
  }

  return (dispatch, getState) => {

    const appData = getState().app;
    const pstrSaleNote = data.get(appData, id.common.saleNoteNumber);
    const pstrAgentCode = data.get(appData, id.common.agentCode);
    const pstrSendMode = mode;
    const parameters = { pstrSaleNote, pstrAgentCode, pstrSendMode };
    const webMethod = 'Email';
    const request = generateRelayRequest({ appData, parameters, webMethod });

    return request();

  };

}

export function setFormVersion() {
  return (dispatch, getState) => {
    return getVersion()
      .then(version => {
        dispatch(setInput({
          identifier: id.common.formVersion,
          data: version
        }));
      });
  };
}

/**
 * Sets the status message to an error, with an optionally
 * specified message, which just defaults to telling the
 * user they have errors
 *
 * @export
 * @param {string} [message]
 * @returns
 */
export function setStatusInvalid(message?: string) {
  return (dispatch, getState) => {
    dispatch(setInput({
      identifier: id.common.serverStatus,
      data: {
        status: 'error',
        message: message || ('Cannot save form, invalid data, ' +
          'fix errors before attempting to save again')
      }
    }));
  };
}

/**
 * This is the save function of sorts for the sale note.
 * It takes whatever values are currently in the form,
 * validates them, and if valid, either saves or updates
 * the sale note. A new sale note will be created if there
 * is not currently a sale note with that number and updated if not
 *
 * @export
 * @returns
 */
export function addSaleNote() {

  const
    { saleNoteDisplay
    , saleNoteForm
    , formVersion
    , saleNoteInputPage
    } = id.common;

  return (dispatch, getState) => {

    const saleNotePageValid = dispatch(validatePage(saleNoteInputPage));

    // We return as to not save an invalid sale note
    if (!saleNotePageValid) {
      return;
    }

    const appData = getState().app;
    const saleNoteInputPageData = data.get(appData, saleNoteInputPage);
    const { saleNoteDetailsForm } = saleNoteInputPageData;

    const saleNotes = data.get(appData, saleNoteDisplay);

    const version = data.get(appData, formVersion);

    let newSaleNote = true;

    if (version !== '') {
      saleNoteDetailsForm.formVersion = version;
    }

    // We loop over all the sale notes and try to find a new one
    saleNotes.forEach(checkSaleNote => {

      const saleNoteNumber = getSaleNoteNumber(checkSaleNote);

      if (typeof saleNoteNumber === 'undefined') {
        return;
      }

      if (saleNoteDetailsForm.saleNoteNumber === saleNoteNumber) {
        // We have found a sale note number that matches
        newSaleNote = false;
      }

    });

    if (newSaleNote) {
      // If it is a new sale note append it to our current list and save
      dispatch(setSaleNotes([...saleNotes, saleNoteInputPageData]));
    } else {
      // Encapsulates the logic for updating the correct index
      dispatch(updateSaleNote(saleNoteInputPageData));
    }

    // We then save it to the server
    dispatch(syncSaleNote(saleNoteInputPageData));

  };

}

export function _createSaleNotes() {

  console.log('creating sale notes')

  if (data.store === null) {
    return
  }

  for (let i = 0; i < 10; i++) {

    console.log('creating sale note', i)

    data.store.dispatch(addSaleNote())
    data.store.dispatch(incrementSaleNoteNumber())

  }

}

/**
 * This is our primitive function for removing an index
 * from our array of sale notes, which is where all sale
 * notes get saved to. This is not intended to be called
 * directly.
 *
 * @export
 * @param {number} index
 * @returns
 */
export function deleteSaleNote(index: number) {

  const { saleNoteDisplay } = id.common;

  return (dispatch, getState) => {

    const oldData = data.get(getState().app, saleNoteDisplay);
    const removeData = [
      ...oldData.slice(0, index),
      ...oldData.slice(index + 1)
    ];

    dispatch(setSaleNotes(removeData));

  };

}

/**
 * This function will delete the passed sale note number
 * if it can be found within our list of local sale notes
 *
 * @export
 * @param {any} saleNoteNumber
 * @returns
 */
export function deleteSaleNoteNumber(saleNoteNumber) {

  const { saleNoteDisplay } = id.common;

  return (dispatch, getState) => {

    const oldData = data.get(getState().app, saleNoteDisplay);
    const index = oldData.findIndex(saleNote => {
      const checkSaleNoteNumber = getSaleNoteNumber(saleNote);
      return checkSaleNoteNumber === saleNoteNumber;
    });

    if (index !== -1) {
      dispatch(deleteSaleNote(index));
    }

  };

}

/**
 * Deletes a sale note from the server and then removes it
 * from the local storage
 *
 * @export
 * @param {any} saleNoteNumber
 * @returns
 */
export function deleteSaleNoteNumberServer(saleNoteNumber) {
  return (dispatch, getState) => {

    const appData = getState().app;
    const webMethod = 'DeleteSaleNote';
    const pstrSaleNote = saleNoteNumber;
    const pstrAgentCode = data.get(appData, id.common.agentCode);
    const parameters = { pstrAgentCode, pstrSaleNote };
    const request = generateRelayRequest({ webMethod, parameters, appData });

    return request()
      .then(response => response.json())
      .then(() => {
        dispatch(deleteSaleNoteNumber(saleNoteNumber));
      })
      .catch(error => {
        dispatch(deleteSaleNoteNumber(saleNoteNumber));
      });

  };
}

/**
 * Takes two sets of props (commonly got from the
 * componentWillUpdate or componentDidUpdate methods) and
 * determines if data on the form has changed in a way
 * which requires the user to be alerted that they have made
 * changes to the form
 *
 * @export
 * @param {any} { prevProps, nextProps }
 * @returns
 */
export function setSaleNoteUnsynced({ prevProps, nextProps }) {
  return (dispatch, getState) => {

    const prevAppData = prevProps.app;
    const nextAppData = nextProps.app;
    const prevSaleNote = data.get(prevAppData, id.common.saleNoteInputPage);
    const nextSaleNote = data.get(nextAppData, id.common.saleNoteInputPage);

    const currentStatus = data.get(prevAppData, id.common.serverStatus);

    if (currentStatus.status === 'unsynced') {
      return;
    }

    try {
      delete prevSaleNote.auditForm;
      delete prevSaleNote.lotsForm.lotProducts;
      delete prevSaleNote.saleNoteDetailsForm.formVersion;
      delete prevSaleNote.saleNoteDetailsForm.serverStatus;
      delete nextSaleNote.auditForm;
      delete nextSaleNote.lotsForm.lotProducts;
      delete nextSaleNote.saleNoteDetailsForm.formVersion;
      delete nextSaleNote.saleNoteDetailsForm.serverStatus;
    } catch (error) {
      console.error(error);
    }

    if (JSON.stringify(prevSaleNote) !== JSON.stringify(nextSaleNote)) {
      dispatch(setInput({
        identifier: id.common.serverStatus,
        data: {
          status: 'unsynced',
          message: 'changes not saved to server'
        }
      }));
    }

  };
}

/**
 * Takes a sale note object and attempts to 'slot'
 * it into the array index of a record that already
 * exists with the same sale note number
 *
 * @export
 * @param {any} updatedSaleNote
 * @returns
 */
export function updateSaleNote(updatedSaleNote) {

  const { saleNoteDisplay } = id.common;

  return (dispatch, getState) => {

    const updatedSaleNoteNumber = getSaleNoteNumber(updatedSaleNote);
    const oldData = data.get(getState().app, saleNoteDisplay);

    // Finds the index of the sale note we wish to update
    const index = oldData.findIndex(saleNote => {

      const saleNoteNumber = getSaleNoteNumber(saleNote);

      if (typeof updatedSaleNoteNumber === 'undefined') {
        console.error('saleNoteNumber not found');
        return;
      }

      return saleNoteNumber === updatedSaleNoteNumber;

    });

    if (index !== -1) {

      // Insert our updated sale note into the sale note array
      const newData = [
        ...oldData.slice(0, index),
        updatedSaleNote,
        ...oldData.slice(index + 1)
      ];

      dispatch(setSaleNotes(newData));
      dispatch(setPage({
        identifier: id.common.saleNoteInputPage,
        pageData: updatedSaleNote
      }));

    }

  };

}

/**
 * Takes an object containing sale note data and sends it to
 * Cache. Also handles the response from the cache server,
 * including some 'normalization'
 *
 * @export
 * @param {any} saleNoteData
 * @returns
 */
export function syncSaleNote(saleNoteData) {

  try {
    delete saleNoteData.lotsForm.lotsProducts;
  } catch (error) {
    console.error(error);
  }

  return (dispatch, getState) => {

    const now = Date.now();

    const appData = getState().app;
    const agentData = data.get(appData, id.common.settingsForm);
    const pstrSaleNote = saleNoteData.saleNoteDetailsForm.saleNoteNumber;
    const parameters = {
      pstrSaleNote,
      data: Object.assign({}, saleNoteData, agentData)
    };
    const webMethod = 'SyncSaleNote';
    const request = generateRelayRequest({ webMethod, parameters, appData });

    return request()
      .then(response => response.json())
      .then(result => {

        const { data } = result;

        if (
          data.saleNoteDetailsForm.saleNoteNumber !==
          saleNoteData.saleNoteDetailsForm.saleNoteNumber
        ) {
          console.log('Data mismatch')
          console.log(data.saleNoteDetailsForm.saleNoteNumber)
          console.log(saleNoteData.saleNoteDetailsForm.saleNoteNumber)
        }
        data.saleNoteDetailsForm.saleNoteNumber =
          saleNoteData.saleNoteDetailsForm.saleNoteNumber;

        // No support for literals in cache, so must convert
        if (typeof data.workflowForm !== 'undefined') {

          for (const workflowStatus in data.workflowForm) {
            const workflowObject = data.workflowForm[workflowStatus];
            data.workflowForm[workflowStatus] =
              convertStringSelect(workflowObject);
          }

        }

        const updatedSaleNoteData = Object.assign(saleNoteData, data);

        if (data) {
          dispatch(updateSaleNote(updatedSaleNoteData));

          try {
            if (data.saleNoteDetailsForm.generateEmail === 'A') {
              dispatch(generateAndSendAdminPDF(pstrSaleNote));
            }
          } catch (error) {
            console.error(error);
          }

        }

      });

  };
}

/**
 * This function syncs all of the sale notes
 * currently stored in the sale note array
 *
 * @export
 * @returns
 */
export function syncAllSaleNotes() {
  return (dispatch, getState) => {

    const appData = getState().app;
    const pstrSaleNotes = data.get(appData, id.common.saleNoteDisplay);
    const webMethod = 'SyncAllSaleNotes';
    const parameters = { pstrSaleNotes };
    const request = generateRelayRequest({ webMethod, parameters, appData });

    return request()
      .then(response => response.json())
      .then(result => {
        alert('Backup success');
      })
      .catch(error => {
        alert('Backup failed');
      });

  };
}

/**
 * Syncs a sale note that matches the passes sale note number
 *
 * @export
 * @param {any} saleNoteNumber
 * @returns
 */
export function syncSaleNoteNumber(saleNoteNumber) {

  const { saleNoteDisplay } = id.common;

  return (dispatch, getState) => {

    const updatedData = data.get(getState().app, saleNoteDisplay);
    const index = updatedData.findIndex(saleNote => {

      if (typeof saleNote.saleNoteDetailsForm === 'undefined') {
        return;
      }

      return saleNote.saleNoteDetailsForm.saleNoteNumber === saleNoteNumber;

    });

    if (typeof index !== 'undefined') {
      dispatch(syncSaleNote(updatedData[index]));
    }

  };
}

/**
 * Takes a sale note number, trys to find it in the list
 * of existing sale notes and if so loads it into the sale
 * note input
 *
 * @export
 * @param {any} saleNoteNumber
 * @returns
 */
export function loadSaleNoteNumber(saleNoteNumber) {

  const { saleNoteDisplay, saleNoteForm } = id.common;

  return (dispatch, getState) => {

    const appData = getState().app;
    const saleNotes = data.get(appData, saleNoteDisplay);
    const selectedSaleNote = saleNotes.find(saleNote => {

      if (typeof saleNote.saleNoteDetailsForm === 'undefined') {
        return;
      }

      return saleNote.saleNoteDetailsForm.saleNoteNumber === saleNoteNumber;

    });

    if (typeof selectedSaleNote !== 'undefined') {

      dispatch(setPage({
        identifier: id.common.saleNoteInputPage,
        pageData: selectedSaleNote
      }));
    }

  };
}

/**
 * Increments the sale note number and sets the sale note number
 *
 * @export
 * @returns
 */
export function incrementSaleNoteNumber() {

  return (dispatch, getState) => {

    // Increment the sale note number in the settings
    dispatch(setNextSaleNoteNumber());

    const appData = getState().app;
    const nextSaleNoteNumber = data.get(appData, id.common.nextSaleNoteNumber);

    if (!nextSaleNoteNumber.includes('-')) {
      console.error(`nextSaleNoteNumber is invalid, ${nextSaleNoteNumber}`);
      return;
    }

    const split = nextSaleNoteNumber.split('-');
    const last = split[split.length - 1];
    const nextNumber = Number(last);

    if (typeof nextNumber !== 'number' || Number.isNaN(nextNumber)) {
      console.error('could parse nextSaleNoteNumber into number');
      return;
    }

    // Sets the sale note number on the input form with the next one
    dispatch(setInput({
      identifier: id.common.saleNoteNumber,
      data: nextSaleNoteNumber
    }));

  };

}

/**
 * Takes a client code and a type (either 'vendor' or 'purchaser')
 * and attempts to load one of the records within the client list
 * into the sale note input page
 *
 * @export
 * @param {any} { clientCode, type }
 * @returns
 */
export function loadClientIntoSaleNote({ clientCode, type }) {

  return (dispatch, getState) => {

    const appData = getState().app;
    const clientList = data.get(appData, id.common.clientDisplay);
    const saleNoteData = data.get(appData, id.common.saleNoteInputPage);
    const agentCode = data.get(appData, id.common.agentCode);
    const agentName = data.get(appData, id.common.agentName);

    const client = clientList.find(client => {
      return client.key === clientCode;
    });

    if (client) {

      const newSaleNoteData = clientToSaleNote(client, type);

      if (
        type === 'vendor'
        &&
        newSaleNoteData.vendorForm.fullName !== ''
      ) {
        newSaleNoteData.vendorForm.entityName =
          newSaleNoteData.vendorForm.fullName;
      }

      if (
        type === 'purchaser'
        &&
        newSaleNoteData.purchaserForm.fullName !== ''
      ) {
        newSaleNoteData.purchaserForm.entityName =
          newSaleNoteData.purchaserForm.fullName;
      }

      if (client.agentCode === agentCode) {

        if (newSaleNoteData.purchaserForm) {
          newSaleNoteData.purchaserForm.agentCode = agentCode;
          newSaleNoteData.purchaserForm.agentName = agentName;
        } else if (newSaleNoteData.vendorForm) {
          newSaleNoteData.vendorForm.agentCode = agentCode;
          newSaleNoteData.vendorForm.agentName = agentName;
        }

      }

      const { saleNoteInputPage } = id.common;

      dispatch(setPage({
        identifier: saleNoteInputPage,
        pageData: Object.assign({}, newSaleNoteData)
      }));

      dispatch(validatePage(saleNoteInputPage));

    }

  };

  /**
   * Helper function that takes the client object
   * stored as in the client list, and sets it into
   * a shape that can be loaded into the sale note
   *
   * @param {*} client
   * @param {string} type
   * @returns {*}
   */
  function clientToSaleNote(client: any, type: string): any {

    if (typeof client === 'undefined') {
      return;
    }

    if (type !== 'vendor' && type !== 'purchaser') {
      return;
    }

    const form = {
      fullName: client.name,
      phoneNumber: client.phone,
      mobileNumber: client.mobile,
      postalAddress: client.addresses[0],
      emailAddress: client.email,
      farmSourceLivestockAccount: client.key,
      NAITNumber: client.NAIT,
      GSTNumber: client.taxNumber
    };

    const page = {
      [type + 'Form']: form,
      saleNoteForm: {
        [type + 'HerdPTPTCode']: client.MINDA
      }
    };

    return page;

  }

}

/**
 * This function sets the next sale note in the settings page
 * to the next value that it thinks the sale note number should be.
 * It does not set any data on the input form
 *
 * @export
 * @returns
 */
export function setNextSaleNoteNumber() {
  return (dispatch, getState) => {

    const appData = getState().app;
    const agentCode = data.get(appData, id.common.agentCode);
    const nextSaleNoteNumber = data.get(appData, id.common.nextSaleNoteNumber);
    const saleNotes = data.get(appData, id.common.saleNoteDisplay);

    if (agentCode === '') {
      return;
    }

    if (saleNotes.length > 0) {

      // Sort mutates, so create shallow clone
      const sortedSaleNotes = [...saleNotes];

      // Puts the sale notes in order, so we can get the last one
      sortedSaleNotes.sort((a, b) => {

        const [aSaleNoteNumber, bSaleNoteNumber] =
          [getSaleNoteNumber(a), getSaleNoteNumber(b)];

        if (aSaleNoteNumber.includes('-') && bSaleNoteNumber.includes('-')) {

          const splitA = aSaleNoteNumber.split('-');
          const splitB = bSaleNoteNumber.split('-');

          const aIndex = splitA[splitA.length - 1];
          const bIndex = splitB[splitB.length - 1];

          if (Number(aIndex) > Number(bIndex)) {
            return 1;
          } else {
            return -1;
          }

        }

        return 0;

      });

      // Gets the last sale note number, then increments it
      const lastSaleNoteNumber = getSaleNoteNumber(sortedSaleNotes.pop());
      const lastSplit = lastSaleNoteNumber.split('-');
      const lastIndex = lastSplit[lastSplit.length - 1];
      const nextSaleNoteNumber = `${agentCode}-${Number(lastIndex) + 1}`;

      setNext(nextSaleNoteNumber);

      return;

    }

    if (nextSaleNoteNumber === '') {

      setNext(agentCode + '-1');

      return;

    }

    const nextSplit = nextSaleNoteNumber.split('-');

    if (nextSplit.length > 2) {

      setNext(agentCode + '-1');

      return;

    }

    if (nextSplit[0] !== agentCode || nextSplit[1] !== agentCode) {
      setNext(agentCode + '-1');
    }

    function setNext(value: string) {
      value = 'SN-' + value;
      dispatch(setInput({
        identifier: id.common.nextSaleNoteNumber,
        data: value
      }));
    }

  };
}

/**
 * Gets all of the sale notes from the Cache server
 *
 * @export
 * @returns
 */
export function getSaleNotes() {
  return (dispatch, getState) => {

    const appData = getState().app;
    const pstrAgentCode = data.get(appData, id.common.agentCode);
    const webMethod = 'GetSaleNotes';
    const parameters = { pstrAgentCode };
    const request = generateRelayRequest({ webMethod, parameters, appData });

    return request()
      .then(response => response.json())
      .then(results => {
        if (Array.isArray(results)) {
          dispatch(setSaleNotes(results));
        }
      });

  };
}

/**
 * Gets the client, location and product lists.
 * I have no idea what the location is supposed
 * to be used for, and the product field is used
 * within the lot description table
 *
 * @export
 * @returns
 */
export function getAgentData() {

  const { clientDisplay } = id.common;

  return (dispatch, getState) => {

    const appData = getState().app;
    const pstrAgentCode = data.get(appData, id.common.agentCode);
    const webMethod = 'GetAgentData';
    const parameters = { pstrAgentCode };
    const request = generateRelayRequest({ webMethod, parameters, appData });

    return request()
      .then(response => response.json())
      .then(results => {

        const { Client, Location, Product } = results;

        if (!Array.isArray(Client)) {
          return;
        }

        // Normalize the client list a bit, mainly removing capitals
        const clients = Client.map(cacheClient => {

          const addresses = [];
          const formattedClient = { addresses };

          for (const key in cacheClient) {

            const value = cacheClient[key];
            const lowerKey = decapitalize(key);

            if (lowerKey.includes('address')) {
              formattedClient.addresses.push(value);
            } else if (!isUppercase(key)) {
              formattedClient[lowerKey] = value;
            } else if (isUppercase(key)) {
              formattedClient[key] = value;
            }

          }

          return formattedClient;

        });

        const products = {};

        for (const productKey in Product) {
          const product = Product[productKey];

          if (!product) {
            continue;
          }

          const { Name, Key } = product;

          products[productKey] = Name;

        }

        // Set the client list
        dispatch(setInput({
          identifier: clientDisplay,
          data: clients
        }));

        // Set our lot products
        dispatch(setInput({
          identifier: id.common.lotProducts,
          data: products
        }));

      });

  };

}

export function syncAgentSettings() {
  return (dispatch, getState) => {

    const appData = getState().app;
    const pstrAgentCode = data.get(appData, id.common.agentCode);
    const settingsForm = data.get(appData, id.common.settingsForm);
    const parameters = { settingsForm, pstrAgentCode };
    const webMethod = 'SyncAgentSettings';
    const request = generateRelayRequest({ webMethod, parameters, appData });

    return request()
      .then(response => response.json())
      .then(result => {
        ; // TODO: something with the result
      });

  };
}

export function getAgentSettings() {
  return (dispatch, getState) => {

    const appData = getState().app;
    const pstrAgentCode = data.get(appData, id.common.agentCode);

    if (pstrAgentCode === '') {
      return;
    }

    const webMethod = 'GetAgentSettings';
    const parameters = { pstrAgentCode };
    const request = generateRelayRequest({ webMethod, parameters, appData });

    return request()
      .then(response => response.json())
      .then(result => {

        if (!result) {
          return;
        }

        if (!result.settingsForm) {
          return;
        }

        const data = result.settingsForm;

        dispatch(setForm({
          identifier: id.common.settingsForm,
          data
        }));

      })
      .catch(error => {
        console.error(error);
      });

  };
}

export function backupData() {
  return (dispatch, getState) => {

    dispatch(syncAllSaleNotes());
    dispatch(syncAgentSettings());

  };
}

export function submitFeedback(feedback) {
  return (dispatch, getState) => {

    const appData = getState().app;
    const webMethod = 'SubmitFeedback';
    const parameters = pstrify(feedback);
    const request = generateRelayRequest({ webMethod, parameters, appData });
    return request()
      .then(result => {
        ;
      });
  };
}

/**
 * This function when called will generate a pdf of the currently
 * loaded form, then send it to the server. It will NOT generate a
 * local copy, just send it directly to the server
 *
 * @export
 * @returns
 */
export function generateAndSendAdminPDF(saleNoteNumber: string) {
  return (dispatch, getState) => {

    const appData = getState().app;
    const CID = data.get(appData, id.common.CID);
    // Const saleNoteNumber = data.get(appData, id.common.saleNoteNumber);
    const agentCode = data.get(appData, id.common.agentCode);
    const filename = saleNoteNumber + '.pdf';
    const URL = `/generatePDFInBackground?filename=${filename}&saleNoteNumber=${saleNoteNumber}`;
    const body = JSON.stringify({
      saleNoteNumber, agentCode, CID
    });
    const method = 'POST';
    const headers = new Headers();
    const options = { method, body, headers };

    headers.set('Content-Type', 'application/json');

    return fetch(URL, options);

  };
}

/**
 * This function takes whatever settings are saved
 * and attempts to load them into the sale note input
 * form if possible
 *
 * @export
 * @returns
 */
export function fillFromAgentSettings() {
  return (dispatch, getState) => {

    const appData = getState().app;
    const saleNoteData = data.get(appData, id.common.saleNoteInputPage);
    const settingsData = data.get(appData, id.common.settingsForm);

    const { agentName, agentCode } = settingsData;

    if (saleNoteData.vendorForm.agentCode === agentCode) {

      if (saleNoteData.vendorForm.agentName === '') {
        saleNoteData.vendorForm.agentName = agentName;
      }

      if (saleNoteData.vendorForm.agentCode === '') {
        saleNoteData.vendorForm.agentCode = agentCode;
      }

      const { saleNoteInputPage } = id.common;

      dispatch(setPage({
        identifier: saleNoteInputPage,
        pageData: saleNoteData
      }));

    }

  };
}

function getCID(appData) {

  if (typeof appData === 'undefined') {
    return;
  }

  return data.get(appData, id.common.CID);

}

interface IGenerateRelayRequest {
  webMethod: string;
  parameters: any;
  appData?: any;
}

/**
 * This is a helper function to make requests to cache
 * without some of the boiler plate of setting up a
 * HTTP request
 *
 * @param {IGenerateRelayRequest} options
 * @returns {() => Promise<IResponse>}
 */
function generateRelayRequest
  ( options: IGenerateRelayRequest
  ): () => Promise<IResponse> {

  const { webMethod, parameters, appData } = options;
  const defaultCID = 'FM8';
  const CID = getCID(appData) || defaultCID;
  const path = `/api/${webMethod}?CID=${CID}`;
  const URL = `${process.env.RELAY_SERVER}${path}`;
  const method = 'POST';
  const body = JSON.stringify(parameters);
  const headers = new Headers();
  const fetchOptions = { method, body, headers };

  headers.set('Content-Type', 'application/json');

  return () => fetch(URL, fetchOptions);

}

function getSaleNoteNumber(saleNote) {
  try {
    return saleNote.saleNoteDetailsForm.saleNoteNumber;
  } catch (error) {
    console.error(error);
    console.error(saleNote);
    return;
  }
}
