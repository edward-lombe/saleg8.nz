'use strict';

import * as React from 'react';
import { Link } from 'react-router';

import * as actions from '../actions';
import * as data from '../data';
import * as identifier from '../identifier';
import { Page, Title } from '../layout';
import { ClientSeachButton } from '../components/client-search';
import * as Bootstrap from 'react-bootstrap';

const
  { Tab
  , Tabs
  , Button
  , ButtonGroup
  , FormGroup
  , Row
  , Col
  , FormControl
  , Glyphicon
  , Modal
  } = Bootstrap;

export default class SaleNoteInput extends React.Component<any, any> {

  render() {

    const { forms } = this.props;

    return (
      <Page>
        <Title>Enter Sale Note</Title>
        <Col xs={12}>
          <Controls {...this.props}/>
          <hr/>
        </Col>
        <Col xs={12}>
          <forms.SaleNoteDetailsForm/>
        </Col>
        <Col xs={12}>
          <FormTabs {...this.props}/>
        </Col>
        <Col xs={12}>
          <br/>
          <hr/>
          <br/>
          <forms.SaleNoteForm/>
        </Col>
      </Page>
    );
  }

  componentDidMount() {
    this.loadFromQuery();
    this.loadFromSettings();
  }

  componentWillReceiveProps(nextProps) {
    const prevProps = this.props;
    this.props.dispatch(actions.setSaleNoteUnsynced({ prevProps, nextProps }));
  }

  getData(querySaleNoteNumber?) {
    const id = identifier.common.saleNoteNumber;
    const saleNoteNumber = data.get(this.props.app, id);
    this.props.dispatch(
      actions.loadSaleNoteNumber(querySaleNoteNumber || saleNoteNumber)
    );
  }

  loadFromSettings() {
    this.props.dispatch(actions.fillFromAgentSettings());
  }

  loadFromQuery() {

    try {

      const { locationBeforeTransitions } = this.props.routing;
      const { query } = locationBeforeTransitions;
      const { saleNoteNumber } = query;

      if (typeof saleNoteNumber !== 'undefined') {
        this.getData(saleNoteNumber);
      }

    } catch (error) {
      console.error('loadFromQuery()', error);
    }

  }

}

class FormTabs extends React.Component<any, any> {

  render() {

    const { forms } = this.props;

    return (
      <Tabs
        id='saleNoteTabs'
        bsStyle='pills'
        animation={false}
        defaultActiveKey={1}>
        <br/>
        <Tab eventKey={1} title='Vendor'>
          <forms.VendorForm/>
        </Tab>
        <Tab eventKey={2} title='Purchaser'>
          <forms.PurchaserForm/>
        </Tab>
        <Tab eventKey={3} title='Lots'>
          <forms.LotsForm/>
        </Tab>
        <Tab eventKey={4} title='Workflow'>
          <forms.WorkflowForm/>
        </Tab>
        <Tab eventKey={5} title='Audit'>
          <forms.AuditForm/>
        </Tab>
        <Tab eventKey={6} title='Notes'>
          <forms.NotesForm/>
        </Tab>
      </Tabs>
    );

  }

}

import { ListGroup, ListGroupItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';

class Controls extends React.Component<any, any> {

  constructor() {
    super();
    this.state = {
      showModal: false
    };
  }

  render() {
    return (
      <span>
        <ButtonGroup>
          <Button
            bsSyle='warning'
            onClick={() => this.onReset()}>
            New
          </Button>
          <Button
            bsSyle='danger'
            onClick={() => this.onShow()}>
            Delete
          </Button>
          <ClientSeachButton
            data={this.getClientData()}
            onChange={e => this.onLoadClient(e)}/>
          <Button
            bsSyle='success'
            onClick={() => this.onSave()}>
            Save
          </Button>
          {/*<Button onClick={() => this.onBackup()}>Backup</Button>*/}
          <LinkContainer to={'/printSaleNotePage'}>
            <Button>
              Print / Email
            </Button>
          </LinkContainer>
          <Modal
            show={this.state.showModal}
            onHide={() => this.onHide()}>
            <Modal.Header closeButton>
              Delete
            </Modal.Header>
            <Modal.Body>
              <strong>Are you sure you wish to delete this record?</strong>
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={() => this.onHide()}>
                Cancel
              </Button>
              <Button
                bsStyle='danger'
                onClick={() => this.onDelete()}>
                Delete
              </Button>
            </Modal.Footer>
          </Modal>
        </ButtonGroup>
      </span>
    );
  }

  onHide() {
    this.setState({ showModal: false });
  }

  onShow() {
    this.setState({ showModal: true });
  }

  onLoadClient(client: any) {
    this.props.dispatch(actions.loadClientIntoSaleNote(client));
  }

  onSave() {
    this.props.dispatch(actions.addSaleNote());
  }

  onReset() {
    const { dispatch } = this.props;
    dispatch(actions.resetPage(identifier.common.saleNoteInputPage));
    dispatch(actions.incrementSaleNoteNumber());
    dispatch(actions.fillFromAgentSettings());
  }

  onDelete() {

    const { dispatch } = this.props;
    const saleNoteNumber =
      data.get(this.props.app, identifier.common.saleNoteNumber);

    dispatch(actions.deleteSaleNoteNumber(saleNoteNumber));
    dispatch(actions.resetPage(identifier.common.saleNoteInputPage));
    dispatch(actions.fillFromAgentSettings());

    this.onHide();

  }

  onBackup() {
    this.props.dispatch(actions.backupData());
  }

  getClientData() {
    const clientIdentifier = identifier.common.clientDisplay;
    const clientData = data.get(this.props.app, clientIdentifier);
    return clientData;
  }

}
