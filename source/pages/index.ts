'use strict';

import * as indexPage from './index-page';
import * as saleNoteInput from './sale-note-input';
import * as testPage from './test-page';
import * as settings from './settings';
import * as saleNoteDisplay from './sale-note-display';
import * as clientDisplay from './client-display';
import * as feedbackPage from './feedback-page';
import * as printSaleNotePage from './print-sale-note-page';

export
  { indexPage
  , saleNoteInput
  , testPage
  , settings
  , saleNoteDisplay
  , clientDisplay
  , feedbackPage
  , printSaleNotePage
  }
