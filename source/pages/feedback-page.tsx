'use strict';

import * as React from 'react';
import { Link } from 'react-router';

import * as actions from '../actions';
import { Page, Title } from '../layout';
import * as Bootstrap from 'react-bootstrap';

const
  { FormControl
  , FormGroup
  , ControlLabel
  , Button
  , Row
  , Col
  , Grid
  } = Bootstrap;

export default class extends React.Component<any, any> {

  constructor() {
    super();
    this.state = {
      name: '',
      contact: '',
      feedback: ''
    };
  }

  render() {

    const { forms } = this.props;

    return (
      <Page>
        <Row>
          <Title>Submit feedback</Title>
          <Col xs={12} sm={8} md={6}>
            <p>
              Feedback on the app can be submitted through this form.
              Make sure to include a detailed description of the problem
              as well as the steps required to reproduce it. This ensures
              that your issue can be addressed and fixed as fast as possible.
              Including valid contact details enables us to contact you for
              any further details that we might need.
            </p>
            <FormGroup >
              <ControlLabel>Name</ControlLabel>
              <FormControl
                onChange={e => this.onName(e)}
                placeholder='Enter name...'
                type='text'/>
            </FormGroup>
            <FormGroup >
              <ControlLabel>Contact details</ControlLabel>
              <FormControl
                onChange={e => this.onContact(e)}
                placeholder='Email, phone number, etc...'
                type='text'/>
            </FormGroup>
            <FormGroup >
              <ControlLabel>Feedback</ControlLabel>
              <FormControl
                onChange={e => this.onFeedback(e)}
                componentClass='textarea'
                placeholder='Enter detailed feedback here'/>
            </FormGroup>
            <Button
              onClick={() => this.onSubmit()}>
              Submit
            </Button>
          </Col>
        </Row>
      </Page>
    );
  }

  onContact(e) {
    const contact = e.target.value;
    this.setState({ contact });
  }

  onName(e) {
    const name = e.target.value;
    this.setState({ name });
  }

  onFeedback(e) {
    const feedback = e.target.feedback;
    this.setState({ feedback });
  }

  onSubmit() {
    this.props.dispatch(actions.submitFeedback(this.state));
  }

}
