'use strict';

import * as React from 'react';

import * as id from '../identifier';
import * as data from '../data';
import * as actions from '../actions';
import { Page, Title } from '../layout';
import { ClientSeachButton } from '../components/client-search';
import * as Bootstrap from 'react-bootstrap';

const { Col } = Bootstrap;

const
  { Button, ButtonGroup } = Bootstrap;

export default class extends React.Component<any, any> {

  render() {

    const { forms } = this.props;

    return (
      <Page>
        <Title>View Sale Notes</Title>
        <Col xs={12}>
          <forms.SaleNoteDisplayForm/>
        </Col>
      </Page>
    );

  }

}
