'use strict';

import * as React from 'react';

import Navbar from '../components/navbar';
import Sidebar from '../components/sidebar';
import { Page, Title } from '../layout';
import * as Bootstrap from 'react-bootstrap';

const { Col } = Bootstrap;

export default class extends React.Component<any, any> {

  render() {

    const { forms } = this.props;

    return (
      <Page>
        <Title>Settings</Title>
        <Col xs={12}>
          <forms.SettingsForm/>
        </Col>
      </Page>
    );

  }

}
