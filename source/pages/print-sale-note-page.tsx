'use strict';
import * as React from 'react';
import { Link } from 'react-router';

import * as actions from '../actions';
import * as data from '../data';
import * as id from '../identifier';
import { entries, capitalize } from '../helpers';
import { Page, Title } from '../layout';
import * as Bootstrap from 'react-bootstrap';
import 'isomorphic-fetch';
import { LinkContainer } from 'react-router-bootstrap';

// Const Elm = require('react-elm-components');
// Const { Main } = require('../elm');

const { PropTypes } = React;
const
  { Row
  , Col
  , Grid
  , FormControl
  , FormGroup
  , ControlLabel
  , Table
  , Button
  , ButtonToolbar
  , ButtonGroup
  , Form
  , Modal
  } = Bootstrap;
const headerStyle = {
  color: '#ffffff !important',
  backgroundColor: '#337ab7 !important'
};
const Static = FormControl['Static'];
const Image = Bootstrap['Image'];

export default class extends React.Component<any, any> {

  constructor() {
    super();
    this.state = {
      filename: null,
      vendorForm: true,
      purchaserForm: true,
      showModal: false,
      mode: 'all'
    };
  }

  render() {

    const data = this.getData();
    const { vendorForm, purchaserForm } = this.state;
    // Const elm = (<Elm src={Main} flags={data}></Elm>);

    return (
      <div>
      <Grid>
        <style>
          {`
            @media print {
              .print-background {
                color: white !important;
                background-color: #337ab7 !important;
                -webkit-print-color-adjust: exact;
              }
            }
            @media screen {
              .print-background {
                color: white !important;
                background-color: #337ab7 !important;
              }
            }
          `}
        </style>
        <Row className='hidden-print'>
          <Col xs={12}>
            <br/>
            <ButtonToolbar>
              <LinkContainer to={'/saleNotePage'}>
                <Button>Back</Button>
              </LinkContainer>
              <ButtonGroup>
                {/*<Button onClick={e => this.onGeneratePDF()}>
                  Generate PDF
                </Button>*/}
                <Button onClick={() => this.onOpen()}>
                  Open PDF
                </Button>
                {/*<Button onClick={() => this.onSendPDFToServer()}>
                  Send PDF to Server
                </Button>*/}
              </ButtonGroup>
              <ButtonGroup>
                <Button onClick={() => this.setMode('vendor')}>
                  Vendor Summary
                </Button>
                <Button onClick={() => this.setMode('purchaser')}>
                  Purchaser Summary
                </Button>
                <Button onClick={() => this.setMode('all')}>
                  Administrator Summary
                </Button>
              </ButtonGroup>
            </ButtonToolbar>
          </Col>
          <Col xs={12}>
            <br/>
            <ButtonToolbar>
              <ButtonGroup>
                <Button onClick={() => this.onEmail('vendor')}>
                  Email to Vendor
                </Button>
                <Button onClick={() => this.onEmail('purchaser')}>
                  Email to Purchaser
                </Button>
                <Button onClick={() => this.onEmail('all')}>
                  Email to Administrator
                </Button>
              </ButtonGroup>
            </ButtonToolbar>
            <hr/>
          </Col>
        </Row>
        <Header
          purchaserForm={purchaserForm}
          vendorForm={vendorForm}/>
        <SaleNoteNumber data={data}/>
        {vendorForm
          ? <VendorForm data={data}/>
          : ''}
        {purchaserForm
          ? <PurchaserForm data={data}/>
          : ''}
        <StockDescriptionTable data={data}/>
        <SaleNoteForm data={data}/>
        <Signatures data={data}/>
        <Modal
          show={this.state.showModal}
          onHide={() => this.setState({ showModal: false })}>
          <Modal.Header closeButton>
            Email Confimation
          </Modal.Header>
          <Modal.Body>
            Are you sure you wish to send an email
            {!(vendorForm && purchaserForm)
              ? ' to ' + this.getEmailAddress() : ''}
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={() => this.setState({ showModal: false })}>
              Cancel
            </Button>
            <Button onClick={() => this.onSendEmail()}>Send</Button>
          </Modal.Footer>
        </Modal>
      </Grid>
      </div>
    );
  }

  componentWillMount() {
    this.setMode('all');
  }

  getEmailAddress() {

    const { dispatch, app } = this.props;
    const { mode } = this.state;

    if (mode === 'vendor') {
      return data.get(app, id.common.vendorEmail);
    } else if (mode === 'purchaser') {
      return data.get(app, id.common.purchaserEmail);
    } else {
      return data.get(app, id.common.agentEmail);
    }

  }

  onEmail(to: 'vendor' | 'purchaser' | 'all') {

    this.setMode(to);
    this.setState({ showModal: true });

  }

  onSendEmail() {

    const to = this.state.mode;
    this.setState({ showModal: false });

    return this.onGeneratePDF()
      .then(() => this.onSendPDFToServer())
      .then(() => {
        this.props.dispatch(actions.email(to));
      });
  }

  onOpen() {

    const { filename } = this.state;
    const open = () => fetch(`/openPDF?filename=${filename}`);

    return this.onGeneratePDF()
      .then(open);

  }

  setMode(mode: 'vendor' | 'purchaser' | 'all') {

    this.setState({ mode });

    const appData = this.props.app;
    const saleNoteNumber = data.get(appData, id.common.saleNoteNumber);
    const vendor = {
      vendorForm: true,
      purchaserForm: false
    };
    const purchaser = {
      vendorForm: false,
      purchaserForm: true
    };
    const all = {
      vendorForm: true,
      purchaserForm: true
    };

    switch (mode) {
      case 'vendor':
        this.setState(vendor);
        this.setState({ filename: `${saleNoteNumber}V.pdf`});
        break;

      case 'purchaser':
        this.setState(purchaser);
        this.setState({ filename: `${saleNoteNumber}P.pdf`});
        break;

      case 'all':
        this.setState(all);
        this.setState({ filename: `${saleNoteNumber}.pdf`});
        break;

      default:
        break;
    }

  }

  onSendPDFToServer() {

    const { filename } = this.state;

    if (typeof filename !== 'string') {
      console.error('Cannot generate PDF without filename');
      return;
    }

    const { dispatch, app } = this.props;
    const CID = data.get(app, id.common.CID);
    const agentCode = data.get(app, id.common.agentCode);
    const saleNoteNumber = data.get(app, id.common.saleNoteNumber);
    const method = 'POST';
    const body = JSON.stringify({ agentCode, saleNoteNumber, CID });
    const headers = new Headers();
    const options = { method, body, headers };

    headers.set('Content-Type', 'application/json');

    return this.onGeneratePDF()
      .then(() => fetch(`/sendPDFToServer?filename=${filename}`, options));

  }

  onGeneratePDF() {

    const { filename } = this.state;

    if (typeof filename !== 'string') {
      console.error('Cannot generate PDF without filename');
      return;
    }

    return fetch(`/generatePDF?filename=${filename}`);

  }

  getData() {

    const appData = this.props.app;
    let saleNote

    const saleNoteNumber = getParameterByName('saleNoteNumber')
    if (saleNoteNumber) {
      console.log('got sale note number', saleNoteNumber)
      this.props.dispatch(actions.loadSaleNoteNumber(saleNoteNumber));
      saleNote = data.get(appData, id.common.saleNoteInputPage, true);

    } else {
      saleNote = data.get(appData, id.common.saleNoteInputPage, true);
    }

    try {
      delete saleNote.forms.lotsForm.inputs.lotProducts;
      delete saleNote.forms.saleNoteDetailsForm.inputs.serverStatus;
      delete saleNote.forms.auditForm;
    } catch (error) {
      console.error(error);
    }

    try {
      return JSON.parse(JSON.stringify(saleNote));
    } catch (error) {
      console.log('error cloning print json')
      return saleNote
    }

  }

}

function getParameterByName(name, url?) {
  try {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  } catch (error) {
//
  }
}

class SaleNoteNumber extends React.Component<any, any> {

  render() {

    const saleNoteNumber = this.getSaleNoteNumber();

    return (
      <Row>
        <Col xs={6} xsOffset={6}>
          <Form horizontal>
            <FormGroup>
              <Col xs={6}>
                <ControlLabel><h4>SALE NOTE NUMBER</h4></ControlLabel>
              </Col>
              <Col xs={6}>
                <Static><h4>{saleNoteNumber}</h4></Static>
              </Col>
            </FormGroup>
          </Form>
        </Col>
      </Row>
    );
  }

  getSaleNoteNumber() {
    try {

      const { data } = this.props;
      const { forms } = data;
      const { saleNoteDetailsForm } = forms;
      const { inputs } = saleNoteDetailsForm;
      const { saleNoteNumber } = inputs;
      const saleNote = saleNoteNumber.data;

      return saleNote;

    } catch (error) {
      console.error(error);
    }
  }

}

class VendorForm extends React.Component<any, any> {

  render() {

    const { forms } = this.props.data;
    const { vendorForm, purchaserForm, notesForm } = forms;
    const
      { entityName
      , entityType
      , farmSourceLivestockAccount
      , phoneNumber
      , mobileNumber
      , agentCode
      , rebateRate
      , agentName
      , dateOfBirth
      , postalAddress
      , emailAddress
      , GSTNumber
      , NAITNumber
      , security
      , thirdParty
      , herdPTPTCode
      } = vendorForm.inputs;

    const { vendorNotes } = notesForm.inputs;

    entityType.data = selectToString(entityType.data);
    thirdParty.data = selectToString(thirdParty.data);

    NAITNumber.label = 'Vendor NAIT number';
    herdPTPTCode.label = 'Vendor Herd / PTPT Code';
    purchaserForm.inputs.herdPTPTCode.label = 'Purchaser Herd / PTPT Code';
    purchaserForm.inputs.NAITNumber.label = 'Purchaser NAIT number';

    return (
      <Row>
        <Col
          xs={12}
          style={{ color: 'white' }}
          className='print-background bg-primary'>
          <h4>VENDOR</h4>
        </Col>
        <Form horizontal>
          {inline(entityName)}
          {inline(entityType)}
          {inline(farmSourceLivestockAccount)}
          {inline(phoneNumber)}
          {inline(mobileNumber)}
          {inline(rebateRate)}
          {inline(agentName)}
          {inline(dateOfBirth)}
          {inline(postalAddress)}
          {inline(emailAddress)}
          {inline(GSTNumber)}
          {inline(NAITNumber)}
          {inline(thirdParty)}
          {inline(security)}
          {inline(herdPTPTCode)}
          {inline(purchaserForm.inputs.NAITNumber)}
          {inline(purchaserForm.inputs.herdPTPTCode)}
          {inline(vendorNotes)}
        </Form>
      </Row>
    );
  }

}

class PurchaserForm extends React.Component<any, any> {

  render() {

    const { forms } = this.props.data;
    const { purchaserForm, notesForm } = forms;
    const
      { entityName
      , entityType
      , farmSourceLivestockAccount
      , phoneNumber
      , mobileNumber
      , agentCode
      , rebateRate
      , agentName
      , dateOfBirth
      , postalAddress
      , emailAddress
      , GSTNumber
      , NAITNumber
      , herdPTPTCode
      } = purchaserForm.inputs;

    const { purchaserNotes } = notesForm.inputs;

    entityType.data = selectToString(entityType.data);

    return (
      <Row>
        <Col
          xs={12}
          style={{ color: 'white' }}
          className='print-background bg-primary'>
          <h4>PURCHASER</h4>
        </Col>
        <Form horizontal>
          {inline(entityName)}
          {inline(entityType)}
          {inline(farmSourceLivestockAccount)}
          {inline(phoneNumber)}
          {inline(mobileNumber)}
          {inline(rebateRate)}
          {inline(agentName)}
          {inline(dateOfBirth)}
          {inline(postalAddress)}
          {inline(emailAddress)}
          {inline(GSTNumber)}
          {inline(NAITNumber)}
          {inline(herdPTPTCode)}
          {inline(purchaserNotes)}
        </Form>
      </Row>
    );
  }

}

class StockDescriptionTable extends React.Component<any, any> {

  render() {

    const tableData = this.getTableData();
    const rows = tableData.data.map((row, i) => {

      const
        { tally
        , description
        , price
        , selectedProduct
        , commissionOverride
        } = row;

      return (
        <tr key={i}>
          <td>{tally}</td>
          <td>{selectedProduct}</td>
          <td>{description}</td>
          <td>{price}</td>
          <td>{commissionOverride}</td>
        </tr>
      );

    });

    return (
      <div>
        <Table>
          <thead>
            <tr>
              <th>Tally</th>
              <th>Product</th>
              <th>Description</th>
              <th>Price</th>
              <th>Commission Override</th>
            </tr>
          </thead>
          <tbody>
            {rows}
          </tbody>
        </Table>
      </div>
    );
  }

  getTableData() {

    const { data } = this.props;

    try {
      const { data } = this.props;
      const { forms } = data;
      const { lotsForm } = forms;
      const { inputs } = lotsForm;
      const { stockDescriptionTable } = inputs;
      return stockDescriptionTable;
    } catch (error) {
      console.error(error);
    }

  }

}

class Signatures extends React.Component<any, any> {

  render() {

    const { data } = this.props;
    const { forms } = data;
    const { saleNoteForm } = forms;
    const
      { vendorSignatureDate
      , purchaserSignatureDate
      , vendorPresenceOf
      , purchaserPresenceOf
      } = saleNoteForm.inputs;
    const signatureBoxStyle = {
      boxSizing: 'border-box',
      border: '1px dashed lightgray',
      borderRadius: '5px',
      height: '100px',
      width: '100%'
    };
    const
      { vendorSignature
      , purchaserSignature
      , vendorAgentSignature
      , purchaserAgentSignature
      } = this.getSignatures();

    if (!vendorSignature || !purchaserSignature) {
      return (
        <Row>
          <Col xs={6}>
            <strong>Vendor Signature</strong>
            <div style={signatureBoxStyle}></div>
          </Col>
          <Col xs={6}>
            <strong>Purchaser Signature</strong>
            <div style={signatureBoxStyle}></div>
          </Col>
          <Col xs={6}>
            <strong>Vendor Agent Signature</strong>
            <div style={signatureBoxStyle}></div>
          </Col>
          <Col xs={6}>
            <strong>Purchaser Agent Signature</strong>
            <div style={signatureBoxStyle}></div>
          </Col>
        </Row>
      );
    }

    return (
      <div>
        <Row>
          <Col xs={6}>
            <strong>Vendor Signature</strong>
            <img src={vendorSignature}/>
          </Col>
          <Col xs={6}>
            <strong>Purchaser Signature</strong>
            <img src={purchaserSignature}/>
          </Col>
        </Row>
        <Row>
          <Form horizontal>
            {inline(vendorPresenceOf)}
            {inline(purchaserPresenceOf)}
          </Form>
        </Row>
        <Row>
          <Col xs={6}>
            <strong>Vendor Agent Signature</strong>
            <img src={vendorAgentSignature}/>
          </Col>
          <Col xs={6}>
            <strong>Purchaser Agent Signature</strong>
            <img src={purchaserAgentSignature}/>
          </Col>
        </Row>
        <Row>
          <Form horizontal>
            {inline(vendorSignatureDate)}
            {inline(purchaserSignatureDate)}
          </Form>
        </Row>
      </div>
    );
  }

  getSignatures() {

    const { data } = this.props;

    const { forms } = data;
    const { saleNoteForm } = forms;
    const { inputs } = saleNoteForm;
    const
      { vendorSignature
      , purchaserSignature
      , vendorAgentSignature
      , purchaserAgentSignature
      } = inputs;

    return {
      vendorSignature: vendorSignature.data,
      purchaserSignature: purchaserSignature.data,
      vendorAgentSignature: vendorAgentSignature.data,
      purchaserAgentSignature: purchaserAgentSignature.data
    };

  }

}

class SaleNoteForm extends React.Component<any, any> {

  render() {

    const { data } = this.props;
    const { forms } = data;
    const { saleNoteForm, notesForm } = forms;
    const
      { inCalfWarranty
      , deliveryDate
      , commissionRate
      , commissionPayable
      , commissionType
      , vendorSignature
      , purchaserSignature
      } = saleNoteForm.inputs;

    const { administratorNotes } = notesForm.inputs;

    try {
      if (commissionType.data.percent) {
        if (!commissionRate.data.includes('%')) {
          commissionRate.data += '%';
        }
      } else if (commissionType.data['per head']) {
        if (!commissionRate.data.includes('$')) {
          commissionRate.data = '$' + commissionRate.data;
        }
      }
    } catch (error) {
      console.error(error);
    }

    inCalfWarranty.data = selectToString(inCalfWarranty.data);
    commissionPayable.data = selectToString(commissionPayable.data);

    return (
      <div>
        <Row>
          <Form horizontal>
            {inline(inCalfWarranty)}
            {inline(deliveryDate)}
          </Form>
        </Row>
        <Row>
          <Form horizontal>
            {inline(commissionRate)}
            {inline(commissionPayable)}
          </Form>
        </Row>
        <Row>
          <Form horizontal>
            {inline(administratorNotes)}
          </Form>
        </Row>
      </div>
    );
  }

}

interface IInlineForm {
  label: string;
  staticControl?: boolean;
  labelWidth?: number;
  dataWidth?: number;
}

class InlineForm extends React.Component<IInlineForm, any> {

  static propTypes = {
    label: PropTypes.string.isRequired,
    staticControl: PropTypes.bool,
    labelWidth: PropTypes.number,
    dataWidth: PropTypes.number
  };

  static defaultProps = {
    staticControl: true,
    label: '',
    labelWidth: 6,
    dataWidth: 6,
    children: []
  };

  render() {

    const { label, staticControl, labelWidth, dataWidth } = this.props;

    const children = staticControl
      ? (
        <Static>{this.props.children}</Static>
      ) : (
        this.props.children
      );

    return (
      <FormGroup>
        <Col xs={labelWidth}>
          <ControlLabel>{label}</ControlLabel>
        </Col>
        <Col xs={dataWidth}>
          {children}
        </Col>
      </FormGroup>
    );
  }
}

function selectToString(data): string {

  if (typeof data === 'string') {
    return data;
  }

  for (const key in data) {
    const value = data[key];
    if (typeof value === 'boolean' && value) {
      return capitalize(key);
    }
    if (value === '1') {
      return capitalize(key);
    }
  }

  return 'None selected';

}

class Header extends React.Component<any, any> {

  render() {

    const { vendorForm, purchaserForm } = this.props;
    let title: string = 'SALE NOTE';

    if (vendorForm && !purchaserForm) {
      title = 'VENDOR SUMMARY';
    } else if (!vendorForm && purchaserForm) {
      title = 'PURCHASER SUMMARY';
    }

    return (
      <Row>
        <Col xs={6}>
          <Image src='/assets/logo.png' rounded responsive/>
        </Col>
        <Col xs={6}>
          <h1>
            <b>{title}</b>
          </h1>
          <small style={{margin: '0'}}>
            Version: {sessionStorage.getItem('savedVersion') || '0.0.31'}
            , generated at {new Date().toLocaleString()}
          </small>
          <p>
            <strong>Farm Source Livestock</strong>{' '}
            Level 1, 19 Home Straight, Te Rapa, Hamilton, 3200,
            PO Box 9045, Hamilton 3240, New Zealand
          </p>
          <p>
            <strong>T</strong> 07 858 0611
            <strong>F</strong> 07 974 9141
            <br/>
            nzfss.livestock@fonterra.com{' '}
            <strong>nzfarmsource.co.nz/livestock</strong>
          </p>
        </Col>
      </Row>
    );
  }

}

function inline(inputData: any) {

  if (typeof inputData === 'undefined') {
    inputData = { label: '', data: '' };
  }

  return (
    <Col xs={6}>
      <InlineForm label={inputData.label}>
        {inputData.data}
      </InlineForm>
    </Col>
  );
}
