'use strict';

import * as React from 'react';

import Navbar from '../components/navbar';
import Sidebar from '../components/sidebar';
import { Page, Title } from '../layout';

export default class extends React.Component<any, any> {

  render() {

    const { forms } = this.props;

    return (
      <Page>
        <Title>Test Page</Title>
        <forms.TestForm/>
      </Page>
    );

  }

}
