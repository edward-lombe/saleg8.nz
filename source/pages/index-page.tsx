'use strict';

import * as React from 'react';
import { Link } from 'react-router';

import * as actions from '../actions';
import DesignQuote from '../components/design-quote';
import { Page, Title } from '../layout';
import * as Bootstrap from 'react-bootstrap';

const { Row, Col, Grid } = Bootstrap;
const Image = Bootstrap['Image'];

export default class extends React.Component<any, any> {

  render() {
    return (
      <Page>
        <Col xs={12}>
          <Image src='/assets/logo.png' rounded responsive/>
        </Col>
        <Title>Home</Title>
        <Col xs={12} sm={6}>
          <p>
            Go to <Link to='/settings'>settings</Link> to
            set up agent details.
          </p>
        </Col>
        <Col xs={12} sm={6}>
          <DesignQuote/>
        </Col>
      </Page>
    );
  }

  componentWillMount() {
    this.props.dispatch(actions.initial());
  }

}
