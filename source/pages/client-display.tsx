'use strict';

import * as React from 'react';
import { Link } from 'react-router';

import { Page, Title } from '../layout';
import * as Bootstrap from 'react-bootstrap';

const { Row, Col } = Bootstrap;

export default class extends React.Component<any, any> {

  render() {

    const { forms } = this.props;

    return (
      <Page>
        <Row>
          <Title>Clients</Title>
          <Col xs={12}>
            <forms.ClientDisplayForm/>
          </Col>
        </Row>
      </Page>
    );
  }

}
