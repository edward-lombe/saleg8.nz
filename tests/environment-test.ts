'use strict';

import * as assert from 'assert';
import { join } from 'path';
import * as fsep from 'fs-extra-promise';
import 'isomorphic-fetch';

describe('environment', () => {

  const APPLICATION_ENV_FILE = join(__dirname, '../application/env.json');

  it('should have an internet connection', () => {

    return fetch('http://www.google.com.au')
      .then(() => assert(true))
      .catch(() => assert(false));

  });

  it('should have an application env file', () => {

    return fsep.existsAsync(APPLICATION_ENV_FILE)
      .then(exists => {
        assert(exists, 'Application env file not found');
      })
      .catch(error => {
        assert(false, `existsAsync error, ${error}`);
      });

  });

  let RELAY_SERVER: string = null;

  it('should have a RELAY_SERVER env variable', () => {

    return fsep.readJSONAsync(APPLICATION_ENV_FILE)
      .then(envJSON => {

        RELAY_SERVER = envJSON['RELAY_SERVER'];
        const type = typeof RELAY_SERVER;

        if (type !== 'string') {
          assert.fail(type, 'string', 'Expected RELAY_SERVER to be a string');
          return;
        }

        assert(true);

      })
      .catch(error => {
        assert(false, error);
      });

  });

  it('should should have a RELAY_SERVER reachable via http', () => {

    return fetch(RELAY_SERVER)
      .then(response => {
        const message = 'Got invalid response from relay';
        assert(response.status === 200, message);
      })
      .catch(error => {
        assert(false, error);
      });

  });

});
