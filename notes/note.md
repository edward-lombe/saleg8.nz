# Note

- get sale notes is returning some non valid data
- determine triggers for all http calls to cache
- make sure sync is not being called at the wrong time

Agent Number: A013

- loaded sale note
- pressed edit before saving it
- wrong sale note was loaded
- closed sale note
- same (but wrong sale note was loaded)
- only that sale note was in the data
- restarting bring things back to normal

Tips

- Order sale notes from most recent to least recent
- 60 days
- Some sort of notification for sending / status
