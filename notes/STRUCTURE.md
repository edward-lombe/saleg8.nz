# Structure

This file contains an explation of the code structure for this app

## Server side

This side of the app is the one that is running all the electron related events,
and can generally be considered to be running within the `node` javascript
context

For example `application/main.ts` runs the following code

```javascript
// Start HTTP server for the pages to access
server.initialize();

app.on('ready', () => {

  // Adds Command or Control + F12 as shortcut to open devtools
  registerDevToolsShortcut();

  // Opens the main window
  main.initialize();

});
```

The HTTP server is used in the call to `main.initialize()` which contains the
line

```javascript
// Electron api call to create a new window instance
browserWindow = application.createWindow(windowName);

// ... omitted for brevity ...

// Tells it to load our server index
browserWindow.loadURL('http://127.0.0.1:8080');
```

For more detail on this see the code I used to wrap the Electron API in
`application/application.ts` and the relevant Electron documentation
[here][webcontents documentation].

The content is served through an express server with the following code

```javascript
// Serves our bundles, node_modules and a few other folders
app.use(express.static(__dirname));

// Makes it serve the root for all routes
app.get('*', (request, response, next) => {

  response.sendFile(join(__dirname, 'index.html'));

});
```

## Client side

The client side logic for handling these routes is defined in the
`source/main.tsx` file, in which the function `generateRoutes()` takes the
`appData` variable and maps it to a series of react `<Route ... />` elements.
These are the elements that are displayed when a certain link is clicked.

We then get into the client html file `application/index.html` which loads the
CSS, bundled JS and provides a root element for the react app to target

The main file takes the route component, which has a `<Provider/>` component,
which is what Redux hooks its `connect()` calls into, and also a `<Routes/>`
component, which is the component that displays the various pages.

It does this by reading the appropriate `.pages` entry in the `source/db.json`
file, for the following example we will use the setting pages

This is an excerpt of the JSON file, with some relevant fields

```json
{
  "pages" : {
    // ... home page, sale note input page, sale note display page, etc
    "settingsPage": {
      "name": "settingsPage",
      "link": "/settings",
      "type": "pages/settings",
      "forms": {
        "settingsForm": {
          "name": "settingsForm",
          "type": "forms/settings-form",
          "inputs" : {
            "agentName": {
              "name": "agentName",
              "type": "inputs/text",
              "label": "Agent Name",
              "data": ""
            },

            // ... more input data here ...

            "CID": {
              "name": "CID",
              "type": "inputs/text",
              "label": "CID",
              "data": ""
            }
          }
        }
      }
    }
    // ... more page data here ...
  }
}
```

This piece of JSON mainly tell us two things. One, is the link that the page
should be rendered under. This is the `"link": "/settings"` entry. The other is
the type of page to use, namely `"type": "pages/settings"`. This means that
whenever the user navigates to the URL `/settings`, that the component in
`source/pages/settings.tsx` will be rendered and passed the form data it needs.

This is the snippet of code that renders these routes

```javascript
function generateRoute(page) {

  // The unique page identifier
  const componentIdentifier = identifier.generate(page.name);

  // The actual react component
  const component = container.connect(appData, componentIdentifier);

  return (
    <Route

      // Page links are more or less unique, use them as key
      key={page.link}

      // Passing the page URL as a prop to the component
      path={page.link}

      // Passing a component as a prop to the component
      component={component}/>
  );
}
```

The above function is then mapped across all the `.page` entries specified in
the `db.json` file, and then placed as the children to the `<Routes/>` component.

A similar process is then followed for each form on the page, and each input on
the form.

We then use the redux connect function to place all of the inputs or forms into the
props attribute of the React component

```jsx

export class Input extends React.Component<any, any> {

  render() {

    const { data } = this.props

    return (
      <input type='text' value={data}/>
    )

  }

}


// This is an exmaple of a form
export class SomeForm extends React.Component<any, any> {

  render() {

    const { inputs } = this.props

    return (
      <div>
        <inputs.MyInput>
        <inputs.MyOtherInput>
      </div>
    )

  }

}

```

The above is a very simplistic example of what is going on in the sale note app.
Basically you can consider the `MyInput` and `MyOtherInput` as react instances
of the `Input` class. They have been initialized with (potentialy) different data
structures and updated different parts of the state tree.

The way that we interact with the state tree is via the dispatch method.

This is like our 'hole' into the state tree, and allows us to dispatch actions
against it, which in turn mutate the state tree.

The dispatch function is located unders the props attribute of the component
so you can use it like so

```javascript
// imagine you are inside a component

const { dispatch } = this.props
dispatch(action(...))

```

In the case of this app, we have an action called `setInput` that is used to change the
value of various inputs throughout the application. This is done with something called
an `identifier`, which is used to tag all of the inputs in the app. We can then dispatch
an action that sets these identified inputs and update their values.

For example this code sets the agent email on the settings page

```javascript
dispatch(setInput({
  identifier: {
    page: 'settingsPage',
    form: 'settingsForm',
    input: 'agentEmail'
  },
  data: 'billy.bruce@email.com'
}))
```

Which then calls a function in the `actions.ts` file. This function/file are where a lot of the
updates to the app are performed.

```typescript
export function setInput(options: ISetInput) {

  const { identifier, data } = options;
  const type = SET_INPUT;
  const payload = { identifier, data };

  return { type, payload };

}
```

The two important things here are the type, `SET_INPUT` and that the payload is the data and identifier

The object `{ type, payload }` ends up being used as the input to the following function

```javascript
function inputReducer(
    initialState = { data: null }
  , inputIdentifier: IIdentifier) {

  // This is a function that we are returning
  return (state = initialState, action) => {

    const { payload, type } = action;

    if (typeof payload === 'undefined') {
      return state;
    }

    if (!id.compare(payload.identifier, inputIdentifier)) {
      return state;
    }

    // This is where we assign data
    if (type === SET_INPUT) {
      return Object.assign({}, state, { data: payload.data });
    }

    // This is where we set stuff like label, warning etc.
    if (type === CONFIGURE_INPUT) {
      return Object.assign({}, state, payload);
    }

    return state;

  };

}

```

I also highly reccomend reading this if you need to get up to speed <http://redux.js.org>

So the app is divided into a series of pages

The indexPage, which is th first page you see when you load the app
The saleNoteInputPage, which is the page that displays the sale notes
The saleNoteDisplayPage, which is what is used to list and filter the sale notes
The clientDisplayPage which us used for the clients to load and view them
The settingsPage which contains all of the settings

Additionally, there is the concept of forms, which are ways of dividing input on pages

[webcontents documentation]: https://github.com/electron/electron/blob/master/docs/api/web-contents.md