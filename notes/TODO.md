# TODO

---

## the important list

- Fix signatues
- Fix validation

## the old list

* ✓ generate routes function
* ✓ connect pages to store
* ✓ fix email validation
* ✓ remove third party and security from purchaser form
* ✓ add delete to stock description table
* ✓ add rest of inputs to stock description table
* ✓ add commission rate, payment help, delivery date
* ✓ add text to sale note form
* ✓ get working on safari
* ✗ add validation to app data
* ✗ add validation to rest of inputs
* ✗ add validation defined in appData.json
* ✗ client list
  * ✗ create
  * ✗ read
  * ✗ update
  * ✗ delete
* ✗ offline/online syncing
* ✗ add electron
* get plop working for
  * ✓ pages
  * ✓ forms
  * ✓ inputs
  * ✓ components
* ✗ some sort of auto indexing
* ✗ fix url not being found on production builds
* ✗ add reset button to form
* ✗ better naming conventions in the db.json
* ✗ add default actions
* ✗ resetForm messes up radio buttons
* ✗ place client table columns into logical not alphabetical order
  * see saleg8 screen
* ✓ client lookup
* ✗ add date field to signatures
* ✗ workflow statuses
  * sale location
  * statuses
    * delivered
    * nait transfer
    * animal record transfer
    * email
    * auditing workflow
    * each one can either be blank complete or not applicable
  * vendor signed, purchaser signed check box
  * send / email pdf to vendor, purchaser
    * include a date for sending of email
  * send to the saleg8 server status
    * button
    * perform the creation and updating sales, lots, data
* ✗ useful error if client cannot fetch salenotes
* add feedback
* deleted items?
  * if sent to server do not allow for deleting
  * clear everything but sale note
* auto generating of sale note numbers
  * webmethod to fetch next salenote number
  * action to fetch next salenote number
  * load next salenote number into the form
* FIX INDEX ISSUES
* add a last / next sale note number
  * will be persisted
* auto synchronisation of settings
* add pagination for clients
* SYNC ALL SETTINGS AND SALENOTES
