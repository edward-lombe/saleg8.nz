# Notes

terms and conditions

- attach sale note
- searching
- sale note
- sale notes disappearing
- fix right click
- Look into Reactive styling, allow easy re-skin?
- Have an update button
  - Need some central store for distributions

## Design

**PAGE**: A page can be considered a top level wrapper for forms. It should contain
either little or no user input, only links to other pages/navigation

**FORM**: A form is placed can be placed with any other form on a page,
it is made up of a combination of components, each of which will store
its data into the form. The form will then be responsible for amalgamation
and then the sending of data. Any bunch of inputs related to a submit or
some other related action should be combined into a form

**INPUT/COMPONENT**: An input can be considered as any element which has to
take, store and validate some sort of user interaction. Each input will be
attatched to a particular form, and it will update that forms value

## State atom

```

{

  saleNotePage: {
    saleNoteForm: {
      saleNoteNumber: {
        value:
        valid:

}

```

## Page

Have the page as a top level component, rendering mostly layout and
navigation elements. Each page element would be inserted into some
root level node.

```

// The link that the page will be accessible form
export const link = '/home/dashboard';

export class Page extends React.Component {

  render() {
    return (
      <boring layout>
        {/* This will be where the form components go*/}
        {this.props.children}
      <probably more boring layout/>
    );
  }

}

```

## Form

## Inputs

Have the concept of primitive and complex inputs, the latter being
made of some combination of primitive and/or complex inputs.

- Primitive Inputs
  - text (`<input type='text'/>`)
  - numeric (`<input type='number'/>`)
  - note (`<textarea></textarea>`)
  - boolean (`<input type='checkbox'/>`, `<input type='radio'/>`)
  - multiple (
  - enumeration

text: 'some string'
number: 90
textarea: 'Something with\nnewlines'
select: {
  option1: false,
  option2: false,
  option3: true
}
checkbox: {
  checkbox1: true,
  checkbox2: false,
  checkbox3: true
}
radio: {
  radio1: false,
  radio2: true,
  radio3: false
}


```json
{
  pages: {
    saleNote: {
      saleNoteForm: {
        saleNoteNumber: {
          type: 'text',
          label: 'Sale Note Number'
        }
      }
    }
  }
}
```

list of fields for display
salenotenumber, v entity name, v agent code, p entity name, p agent code
first lot line
open to the edit page for more details

include purchaser nait number and minda code in print pdf
terms and conditions
final 3 pdfs are sale note (whole), vendor summary, purchaser summary
