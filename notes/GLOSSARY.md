# Glossary of terms

---

## appData

The top level data structure for defining the app.

It will usually have a shape similar to

```
{
  pages: {
    <page name>: {
      name, type, link,
      forms: {
        <form name>: {
          name, type,
          inputs: {
            <input name>: {
              name, type, label, data
            },
            <input name>: { ... }
          }
        },
        <form name>: { ... }
      }
    },
    <page name>: { ... },
    <page name>: { ... }
  }
}
```

##