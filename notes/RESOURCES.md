# Resources

---

Collection of useful reading material

## Online / Offline

- offline web applications - <https://hacks.mozilla.org/2010/01/offline-web-applications/>
- let's take this offline - <http://diveintohtml5.info/offline.html>
- NavigatorOnLine.onLine - <https://developer.mozilla.org/en-US/docs/Web/API/NavigatorOnLine/onLine>
- Online and offline events - <https://developer.mozilla.org/en-US/docs/Online_and_offline_events>
- Working Off the Grid with HTML5 Offline - <http://www.html5rocks.com/en/mobile/workingoffthegrid/>