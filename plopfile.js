'use strict';

module.exports = function (plop) {
    
    plop.setGenerator('page', {
      description: 'Adds a page to the project',
      prompts: [
        {
          type: 'input',
          name: 'name',
          message: 'What do you want to call the page?',
          validate: name => name.length > 0
        }
      ],
      actions: [
        {
          type: 'add',
          path: 'source/pages/{{dashCase name}}.tsx',
          templateFile: 'plop_templates/page.txt'
        }
      ]
    });
    
    plop.setGenerator('form', {
      description: 'Adds a form to the project',
      prompts: [
        {
          type: 'input',
          name: 'name',
          message: 'What do you want to call the form?',
          validate: name => name.length > 0
        }
      ],
      actions: [
        {
          type: 'add',
          path: 'source/forms/{{dashCase name}}.tsx',
          templateFile: 'plop_templates/form.txt'
        }
      ]
    });
    
    plop.setGenerator('input', {
      description: 'Adds an input to the project',
      prompts: [
        {
          type: 'input',
          name: 'name',
          message: 'What do you want to call the input?',
          validate: name => name.length > 0
        }
      ],
      actions: [
        {
          type: 'add',
          path: 'source/inputs/{{dashCase name}}.tsx',
          templateFile: 'plop_templates/input.txt'
        }
      ]
    });
    
    plop.setGenerator('component', {
      description: 'Add a component to the project',
      prompts: [
        {
          type: 'input',
          name: 'name',
          message: 'What do you want to call the component',
          validate: name => name.length > 0
        }
      ],
      actions: [
        {
          type: 'add',
          path: 'source/components/{{dashCase name}}.tsx',
          templateFile: 'plop_templates/component.txt'
        }
      ]
    })
    
};